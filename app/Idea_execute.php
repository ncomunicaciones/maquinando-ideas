<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea_execute extends Model
{
    protected $guarded = [];

    public function idea()
    {
        return $this->belongsTo('App\Idea');
    }

    public function idea_operators()
    {
        return $this->hasMany('App\Idea_operator');
    }

    public function operator_boss()
    {
        return $this->belongsTo('App\User', 'operator_boss_user_id');
    }
    
}
