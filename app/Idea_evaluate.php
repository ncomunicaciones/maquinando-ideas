<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea_evaluate extends Model
{
    protected $guarded = [];

    public function idea() {
        return $this->belongsTo('App\Idea');
    }
}
