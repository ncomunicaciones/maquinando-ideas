<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    protected $guarded = [];

    public function problems() {
        return $this->hasMany('App\Problem');
    }
    
    public function idea_evaluate() {
        return $this->hasMany('App\Idea_evaluate');
    }

    public function idea_execute() {
        return $this->hasMany('App\Idea_execute');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function convert_datetime($str) {
     
        list($date, $time) = explode(' ', $str);
        list($year, $month, $day) = explode('-', $date);
        list($hour, $minute, $second) = explode(':', $time);
        
        $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
        
        return $timestamp;
    }
}
