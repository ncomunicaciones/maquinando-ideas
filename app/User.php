<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'phone', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function boss_operator_idea_executes()
    {
        return $this->hasMany('App\Idea_execute', 'operator_boss_user_id');
    }

    public function operator_idea_executes()
    {
        return $this->hasMany('App\Idea_operator', 'operator_user_id');
    }
    public function administrator()
    {
        return $this->hasMany('App\Administrator');
    }
    
}
