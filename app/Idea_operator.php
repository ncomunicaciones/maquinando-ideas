<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea_operator extends Model
{
    public function idea_execute()
    {
        return $this->belongsTo('App\Idea_execute');
    }

    public function operator()
    {
        return $this->belongsTo('App\User', 'operator_user_id');
    }
}
