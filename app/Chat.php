<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $guarded = [];

    public function chat_rooms() {
        return $this->belongsToMany('App\Chat_room');
    }
}
