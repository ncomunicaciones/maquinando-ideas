<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat_room extends Model
{
    public function chats() {
        return $this->hasMany('App\Chat');
    }
}
