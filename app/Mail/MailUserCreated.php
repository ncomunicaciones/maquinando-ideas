<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailUserCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $mailuser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailuser)
    {
        $this->mailuser = $mailuser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('layouts.usercreated');
    }
}
