<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function ideas() {
        return $this->hasMany('App\Idea');
    }
}
