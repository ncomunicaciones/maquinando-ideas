<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use \Validator;
use App\User;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest',['only' => 'showLoginForm']);
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $first = User::where('email', request('username'))->first();

        if($first){
            if($first->state != false){
                $credentials = ['email' => request('username'), 'password' => request('password')];
                
                $validatedData = Validator::make($request->all(), [
                    'username' => 'required',
                    'password' => 'required'
                ]);

                if ($validatedData->fails()) {
                    return response()->json(['errors' => $validatedData->errors()]);
                }

                if (Auth::attempt($credentials)) {
                    return response()->json(['data' => Auth::user(), 'result' => 'success', 'message' => 'Autenticación exitosa']);
                }
                
                return response()->json(['data' => null, 'result' => 'error', 'message' => 'Usuario o contraseña incorrectos']);
            } else {
                return response()->json(['data' => null, 'result' => 'error', 'message' => 'El usuario no se encuentra activo en la plataforma']);
            }
        } else {
            return response()->json(['data' => null, 'result' => 'error', 'message' => 'Usuario o contraseña incorrectos']);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/login');
    }
}
