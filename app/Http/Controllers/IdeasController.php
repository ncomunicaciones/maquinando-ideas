<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Idea;
use App\Problem;
use Illuminate\Support\Facades\Auth;
use App\Idea_evaluate;
use App\Administrator;
use App\Chat;
use App\Chat_room;
use Carbon\Carbon;
use \Validator;
use App\User;
use App\Idea_execute;
use App\Idea_operator;
use App\Category;
use App\Notification;
use PHPExcel; 
use PHPExcel_IOFactory;
use Session;
use App\Requests;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Mail\MailUserCreated;

class IdeasController extends Controller
{

    public function checkNotifications(Request $request){
        $notifications_ids = $request['notificaciones'];
        $array_we = [];
        foreach ($notifications_ids as $notification_idea_id) {
            $is_not_chat = DB::select('CALL getNotificationNotChatById("'.$notification_idea_id.'")');
            
            if ($is_not_chat) {
                $array_we['ideas'][] = $this->setIdea($is_not_chat[0]->idea_id);
            }else{
                $chat_rooms_ids = DB::select('CALL getChatRoomsByNotificationId("'.$notification_idea_id.'")');
                foreach ($chat_rooms_ids as &$chat_room_id) {
                    $chat_room_id = $chat_room_id->id;
                }
                unset($chat_room_id);

                $chat_rooms = Chat_room::whereIn('id', $chat_rooms_ids)->get();

                foreach ($chat_rooms as &$chat_room) {
                    $chat_room->chat = $chat_room->chats;
                }
                unset($chat_room);
                
                $array_we['chats'][] = $chat_rooms;
            }
        }
        return $array_we;
    }

    

    private function setIdea($id){
        $idea = Idea::find($id);
        $admin = $idea->admin_id?  Administrator::find($idea->admin_id)->user_id : null;
        $newCategory = Category::find($idea->category_id);
        $idea->category_name = $newCategory->name;
        $idea->problems = $idea->problems;
        $idea->usercreator = User::find($idea->user_id)->name;
        $idea->usercreatorall = User::find($idea->user_id);
        $idea->administratorcreator = User::find($admin);
        if($idea->state == 'guardado'){
            $idea->date_start = $idea->created_at->diffForHumans();
        }
        if($idea->state == 'evaluacion'){
            $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
            $newVal =  $idea->idea_evaluate[0]->created_at? $idea->idea_evaluate[0]->created_at->diffForHumans() : null;
            $idea->date_difference = Carbon::parse($idea->idea_evaluate[0]->end_day)->diffInDays(Carbon::parse(Carbon::now()));
            $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
            $idea->date_start = 'La evaluacion inicio '.$newVal;
        }  
        if($idea->state == 'ejecucion'){
            $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
            $idea->idea_execute_create = $idea->idea_evaluate[0]->created_at;

            $newVal =  $idea->idea_execute[0]->created_at? Carbon::parse($idea->idea_execute[0]->created_at)->diffForHumans() : null;
            $idea->date_difference = Carbon::parse($idea->idea_evaluate[0]->end_day)->diffInDays(Carbon::parse(Carbon::now()));
            $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
            $idea->date_start = 'La ejecución inicio '.$newVal;
            $getOperators = Idea_operator::where('idea_execute_id',$idea->idea_execute[0]->id)->get();
            $idea->idea_execute = $idea->idea_execute[0];
            $newOperators = [];
            foreach($getOperators as $operator){
                array_push($newOperators, User::select('id','name','avatar')->where('id', $operator->operator_user_id)->first());
            }
            $idea->getOperators = $newOperators;
            $idea->getBossOperators = User::select('id','name','avatar')->where('id',$idea->idea_execute->operator_boss_user_id)->first();
        }  
        if($idea->state == 'aprobado'){
            $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
            $newVal =  $idea->idea_execute[0]->created_at? $idea->idea_execute[0]->created_at->diffForHumans() : null;
            $idea->date_start = 'La idea se aprobó '.$newVal;
            $idea->idea_aprobed_create = $idea->idea_execute[0]->created_at;
            if (property_exists($idea, 'idea_execute')) {
                $idea->idea_execute = $idea->idea_execute[0];
            } else {
                $idea->idea_execute = [];
            }
        }
        if($idea->state == 'desaprobado'){
            $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
            $idea->date_start = 'La idea se desaprobó '. $idea->updated_at->diffForHumans();
            $idea->idea_disapproved_create = $idea->updated_at;
            if (property_exists($idea, 'idea_execute')) {
                $idea->idea_execute = $idea->idea_execute[0];
            } else {
                $idea->idea_execute = [];
            }
        }
        if($idea->state == 'abortado'){
            $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
            $idea->date_start = 'La idea se abortó '. $idea->idea_execute[0]->updated_at->diffForHumans();
            $idea->idea_disapproved_create = $idea->updated_at;
            $getOperators = Idea_operator::where('idea_execute_id',$idea->idea_execute[0]->id)->get();
            $newOperators = [];
            foreach($getOperators as $operator){
                array_push($newOperators, User::select('id','name','avatar')->where('id',$operator->operator_user_id)->first());
            }
            $idea->getOperators = $newOperators;
            $idea->getBossOperators = User::select('id','name','avatar')->where('id',$idea->idea_execute[0]->operator_boss_user_id)->first();
        }
        if($idea->state == 'premiada'){
            $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
            $idea->idea_execute_create = $idea->idea_evaluate[0]->created_at;

            $newVal =  $idea->idea_execute[0]->end_day? Carbon::parse($idea->idea_execute[0]->end_day)->diffForHumans() : null;
            $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
            $idea->test = $idea->idea_execute[0];
            $idea->date_start = 'La idea concluyó '.$newVal;
            $idea->finish = $idea->idea_execute[0]->end_day;
            $getOperators = Idea_operator::where('idea_execute_id',$idea->idea_execute[0]->id)->get();
            $newOperators = [];
            foreach($getOperators as $operator){
                array_push($newOperators, User::select('id','name','avatar')->where('id',$operator->operator_user_id)->first());
            }
            $idea->getOperators = $newOperators;
            $idea->getBossOperators = User::select('id','name','avatar')->where('id',$idea->idea_execute[0]->operator_boss_user_id)->first();
        }
        return $idea;
    }




    public function index()
    {
        return view('ideas.index');
    }

    public function show($idea_id)
    {
        $idea = Idea::with('problems', 'idea_evaluate', 'idea_execute', 'category')->find($idea_id);
        return $idea;
    }

    public function store(Request $request)
    {
        $newIdea = Idea::create([
            'title' => request('title'),
            'summary' => request('summary'),
            'conclusions' => request('conclusions'),
            'category_id' => request('category_id'),
            'state' => 'guardado',
            'user_id' => request('user')
        ]);
        $counter = 0;
        foreach(request('problem') as $row){
            Problem::create([
                'idea_id' => $newIdea->id,
                'problem' => request('problem')[$counter],
                'solutions' => serialize(request('solutions')[$counter])
            ]);
            $counter++;
        }
        $newCategory = Category::find($newIdea->category_id);
        $newIdea->category_name = $newCategory->name;
        $newIdea->date_start = 'Creado '.$newIdea->created_at->diffForHumans();
        return $this->setIdea($newIdea->id);
    }

    public function edit(Idea $idea)
    {
        $problems = $idea->problems;

        return view('ideas.edit')->with(['idea' => $idea, 'problems' => $problems]);
    }

    public function update(Request $request, Idea $idea)
    {
        $idea->title = request('title');
        $idea->summary = request('summary');
        $idea->conclusions = request('conclusions');
        $idea->save();
        $idea->touch();
        Problem::where('idea_id', $idea->id)->delete();

        $counter = 0;
        foreach(request('problem') as $row){
            Problem::create([
                'idea_id' => $idea->id,
                'problem' => request('problem')[$counter],
                'solutions' => serialize(request('solutions')[$counter])
            ]);
            $counter++;
        }

        $newCategory = Category::find($idea->category_id);
        $idea->category_name = $newCategory->name;
        $idea->date_start = 'Creado '.$idea->created_at->diffForHumans();

        return $this->setIdea($idea->id);
    }

    public function evaluate(Idea $idea)
    {
            $user_is_admin = Administrator::where('user_id', Auth::id())->first();
            $rand = [];
            foreach(Administrator::all() as $admin) {
                if ($admin->user->state) {
                    if ($user_is_admin) {
                        if ($user_is_admin->id != $admin->id) {
                            array_push($rand, $admin->id);
                        }
                    } else {
                        array_push($rand, $admin->id);
                    }
                }
            }

            if (count($rand) > 0) {
                $random_admin = array_random($rand);

                $idea->state = 'evaluacion';
                $idea->admin_id = $random_admin;
                $idea->save();

                $chatroom = Chat_room::create();

                $idea_valuate = Idea_evaluate::create([
                    'idea_id' => $idea->id,
                    'end_day' => Carbon::now()->addDay(30),
                    'chat_room_id' => $chatroom->id,
                    'evaluate_time' => 30,
                ]);
                
                $get_admin = Administrator::where('id', $idea->admin_id)->first();

                Chat::create([
                    'chat_room_id' => $chatroom->id,
                    'user_id' => $get_admin->user_id,
                    'message' => 'Después del tiempo de evaluación de tu idea te diremos cuales son los siguientes pasos',
                ]);
                
                $getAdmin = Administrator::where('id',$random_admin)->get();

                Notification::create([
                    'read' => false,
                    'title' => 'Se agregó una nueva idea para su evaluación',
                    'type' => 'grey',
                    'user_id' => $getAdmin[0]->user_id,
                    'idea_id' => $idea->id,
                    'provider' => Auth::id(),
                    'link' => $idea->id,
                    'isAdmin' => true
                ]);


                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $newVal =  $idea->idea_evaluate[0]->created_at? $idea->idea_evaluate[0]->created_at->diffForHumans() : null;
                $idea->date_difference = Carbon::parse($idea->idea_evaluate[0]->end_day)->diffInDays(Carbon::parse(Carbon::now()));
                $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
                $idea->date_start = 'La evaluacion inicio '.$newVal;

                $admin = $idea->admin_id?  Administrator::find($idea->admin_id)->user_id : null;
                $newCategory = Category::find($idea->category_id);
                $idea->category_name = $newCategory->name;
                $idea->problems = $idea->problems;
                $idea->usercreator = User::find($idea->user_id)->name;
                $idea->usercreatorall = User::find($idea->user_id);
                $idea->administratorcreator = User::find($admin);

                return $this->setIdea($idea->id);
            } else {
                return response()->json(['result' => 'No hay administradores registrados'], 205);
            }
    }

    public function filter($filtro)
    {
        $ideas = Idea::where('state',$filtro)->get();
        return $ideas;
    }

    public function filter1($filtro)
    {
        if($filtro == 'guardado'){
            $ideas = Idea::where('state', 'guardado')->orWhere('state', 'evaluacion')->take(3)->get();
        }else{
            $ideas = Idea::where('state',$filtro)->take(3)->get();
        }
        return $ideas;
    }

    public function prueba(Idea $idea)
    {
        $evaluate = Idea_evaluate::where('idea_id', $idea->id)->first();
        $chatroom = $evaluate->chat_room_id;
        $messages = Chat::where('chat_room_id', $chatroom)->get();
    }

    public function evaluateDetail(Idea $idea)
    {
        $all = array();
        $idea_evaluate = Idea_evaluate::where('idea_id', $idea->id)->first();

        return array('idea' => $idea, 'idea_evaluate' => $idea_evaluate);
    }

    public function executeteDetail(Idea $idea)
    {
        $all = array();
        $idea_execute = Idea_execute::where('idea_id', $idea->id)->first();
        
        return array('idea' => $idea, 'idea_execute' => $idea_execute);
    }

    public function evaluateEdit(Request $request, Idea $idea_evaluate)
    { 
        $idea = Idea_evaluate::where('idea_id', $idea_evaluate->id)->first();
        // almacenamos el nro de dias transcurridos
        $datetime1 = new \DateTime(date('Y-m-d H:i:s'));
        $datetime2 = new \DateTime($idea->created_at);
        $difference = $datetime1->diff($datetime2);
        $idea->time_elapsed = $difference->days;
        $idea->sustain = $request->sustain;
        $idea->save();

        $idea_evaluate->state = $request->state;
        $idea_evaluate->save();

        if($request->state == 'aprobado'){
            $chatroom = Chat_room::create();
            Idea_execute::create([
                'idea_id' => $idea_evaluate->id,
                'state' => 'inactive',
                'chat_room_id' => $chatroom->id
            ]); 
        }
        
        $validate = $request->state == 'aprobado'? 'green' : 'red';
        $text = 'La idea "'. $idea_evaluate->title .'" enviada fue '.$request->state;
        Notification::create([
            'read' => false,
            'title' => $text,
            'type' => $validate,    
            'user_id' => $idea_evaluate->user_id,
            'idea_id' => $idea_evaluate->id,
            'provider' => Auth::id(),
            'link' => $idea_evaluate->id,
            'isAdmin' => false
        ]);
        
        return $this->setIdea($idea_evaluate->id);

    }

    public function execute_idea($idea_id) {
        $idea = Idea::find($idea_id);
        $idea->state = 'ejecucion';
        $idea->save();

        $text = 'La idea "'. $idea->title .'" enviada fue puesta en ejecución';
        Notification::create([
            'read' => false,
            'title' => $text,
            'type' => 'green',    
            'user_id' => $idea->user_id,
            'idea_id' => $idea->id,
            'provider' => Auth::id(),
            'link' => $idea->id,
            'isAdmin' => false
        ]);

        return $this->setIdea($idea->id);
    }

    public function ideaDetail(Idea $idea)
    {   
        $problems = Problem::where('idea_id', $idea->id)->get();
        $idea->problems = $problems;
        Idea_evaluate::where('idea_id', $idea->id)->first()->sustain !== null? $idea->sustain = true : $idea->sustain = false;
        return $idea;
    }

    public function ejecutarIdeaDetalle($idea)
    {
        $idea_execute = Idea_execute::where('idea_id', $idea)->first();
        return $idea_execute;
    }

    public function categorias(){
        return Category::all();
    }
    public function categoriasShow($categoria){
        $get = Category::where('slug',$categoria)->first();
        return $get;
    }

    public function save_operators(Request $request, $idea_execute_id)
    {
        $operator_boss_id = $request->input('operator_boss');
        $operators_id = $request->input('operators');

        $idea_execute = Idea_execute::find($idea_execute_id);
        $idea_execute->operator_boss_user_id = $operator_boss_id;
        $idea_execute->start_day = date('Y-m-d h:i:s');
        $idea_execute->state = 'active';
        $idea_execute->save();
        
        foreach ($operators_id as $operator_id) {
            $idea_operator = new Idea_operator;
            $idea_operator->idea_execute_id = $idea_execute_id;
            $idea_operator->operator_user_id = $operator_id;
            $idea_operator->save();
        }
        $getIdea = Idea::find($idea_execute->idea_id)->id;
        return $this->setIdea($getIdea);
    }

    public function get_idea_state_data($idea_id) {
        $idea = Idea::with(['idea_evaluate', 'idea_execute'])->get()->find($idea_id);
        if (count($idea->idea_execute) > 0) {
            return array('is_execute' => true, 'idea' => $idea);
        }
        return array('is_execute' => false, 'idea' => $idea);
    }

    public function finish_idea_execute($idea_id) {
        $idea = Idea::find($idea_id);
        $idea->state = 'premiada';
        $resu_idea = $idea->save();

        Notification::create([
            'read' => false,
            'title' => 'La idea '.$idea->title.' fue premiada',
            'type' => 'green',
            'user_id' => $idea->user_id,
            'idea_id' => $idea->id,
            'provider' => Auth::id(),
            'link' => $idea->id,
            'isAdmin' => false
        ]);

        $getExecute = Idea_execute::where('idea_id', $idea->id)->get()[0];

        //Operadores
        $getExecuteOperador = Idea_operator::where('idea_execute_id', $getExecute->id)->get();
        $getExecuteOperators = [];
        foreach($getExecuteOperador as $user){
            array_push($getExecuteOperators, $user->operator_user_id);
        }
        //Jefe Operador
        $getExecuteBoss = $getExecute->operator_boss_user_id;
        array_push($getExecuteOperators, $getExecuteBoss);
        

        foreach($getExecuteOperators as $user){
            Notification::create([
                'read' => false,
                'title' => 'La idea '.$idea->title.' fue premiada',
                'type' => 'green',
                'user_id' => $user,
                'idea_id' => $idea->id,
                'provider' => Auth::id(),
                'link' => $idea->id,
                'isAdmin' => false
            ]);
        }




        $idea_execute = Idea_execute::where('idea_id', $idea_id)->first();
        // almacenamos el nro de dias transcurridos
        $datetime1 = new \DateTime(date('Y-m-d H:i:s'));
        $datetime2 = new \DateTime($idea_execute->created_at);
        $difference = $datetime1->diff($datetime2);
        $idea_execute->time_elapsed = $difference->days;
        $idea_execute->end_day = date('Y-m-d h:i:s');
        $resu_idea_execute = $idea_execute->save();

        $user = User::find($idea->user_id);
        $admin = User::find(Administrator::find($idea->admin_id)->id);
        $boss = User::find(Idea_execute::where('idea_id', $idea_id)->first()->operator_boss_user_id);
        $operators = Idea_operator::where('idea_execute_id', Idea_execute::where('idea_id', $idea_id)->first()->id);

        $allUsers = [];

        /*foreach($operators as $operator){
            
        }

        $transport = (new \Swift_SmtpTransport('smtp.1and1.com', 587,'tls'))->setUsername('msoto@ncomunicaciones.com')->setPassword('Ms0t0#01');
            $view = view('layouts.usercreated', ['user' => $user, 'password' => $idea->title, 'state1' => '1']);
            $body = (string) $view;
            //$body = $view->render();
            $mailer = (new \Swift_Mailer($transport));
            $message = (new \Swift_Message('Asunto'))
            ->setFrom(array('msoto@ncomunicaciones.com' => 'Maquinando Ideas - Administrador'))
            ->setTo(array($user->email => $user->name))
            ->setBcc(array($user->email => $user->name, ))
            ->setBody($body, 'text/html');
            $result = $mailer->send($message);*/

        return $this->setIdea($idea->id);
    }

    /*
    // Chat 

    /*
    // Obtener ideas de operadores
    */
    public function get_ideas_operator($user_id) {
        $ideas_operator = Idea_operator::where('operator_user_id', $user_id)->with('idea_execute')->get();
        $ideas_boss_operator = Idea_execute::where('operator_boss_user_id', $user_id)->get();

        $idea_ids = [];
        foreach ($ideas_operator as $idea_operator) {
            array_push($idea_ids, $idea_operator->idea_execute->idea_id);
        }
        $idea_boss_ids = [];
        foreach ($ideas_boss_operator as $idea_boss_operator) {
            array_push($idea_boss_ids, $idea_boss_operator->idea_id);
        }

        $ideas = [];
        if (count($idea_ids) > 0) {
            $ideas = Idea::where('id', $idea_ids)->get();
        }
        
        $ideas_boss = [];
        if (count($idea_boss_ids) > 0) {
            $ideas_boss = Idea::where('id', $idea_boss_ids)->get();
        }

        return array( 'ideas_operator' => $ideas, 'ideas_boss_operator' => $ideas_boss, 'count' => count($ideas) + count($ideas_boss) );
    }

    /*
    // Editar Tiempo de ideas
    */
    public function evaluarIdeaEditar(Idea_evaluate $idea, Request $request)
    {
        if (isset($request->time)) {
            $idea->evaluate_time = $request->time;
            $idea->end_day = $idea->created_at->addDays($request->time);
            $idea->save();
        }

        $idea_only = Idea::find($idea->idea_id);

        $text = 'Se modifico el tiempo de evaluación a '.$request->time.' dias de la idea "'.$idea_only->title.'"';

        Notification::create([
            'read' => false,
            'title' => $text,
            'type' => 'green',    
            'user_id' => $idea_only->user_id,
            'idea_id' => $idea_only->id,
            'provider' => Auth::id(),
            'link' => $idea_only->id,
            'isAdmin' => false
        ]);
        return $this->setIdea($idea_only->id);
    }

    public function ejecutarIdeaEditar(Idea_execute $idea, Request $request)
    {
        if (isset($request->time)) {
            $idea->execute_time = $request->time;
            $idea->start_day? $idea->end_day = Carbon::parse($idea->start_day)->addDays($request->time) : null;
            $idea->save();
        }

        $idea_only = Idea::find($idea->idea_id);

        $text = 'Se modifico el tiempo de ejecución a '.$request->time.' dias de la idea "'.$idea_only->title.'"';

        if($request->generateNoti){
            Notification::create([
                'read' => false,
                'title' => $text,
                'type' => 'green',    
                'user_id' => $idea_only->user_id,
                'idea_id' => $idea_only->id,
                'provider' => Auth::id(),
                'link' => $idea_only->id,
                'isAdmin' => false
            ]);
        }

        return $this->setIdea($idea_only->id);
    }

    public function excelIdeas()
    {
        return Idea::all();
    }

    public function excelCookies(Request $request)
    {
        Session::put('excel', $request->input('ke'));
        return 'success';
    }

    public function exportExcel()
    {
        $id_ideas = Session::get('excel');
        sort($id_ideas);
        $ideas = [];

        foreach($id_ideas as $idea){
            $onlyIdea = Idea::where('id', $idea)->first();

            $onlyIdea->innovador = User::where('id', $onlyIdea->user_id)->first()->name;

            $onlyIdea->supervisor = $onlyIdea->state != 'guardado'? User::where('id', Administrator::where('id',$onlyIdea->admin_id)->first()->user_id)->first()->name : null;
            array_push($ideas, $onlyIdea);
            
            $onlyIdea->categoria = Category::where('id', $onlyIdea->category_id)->first()->name;

            $onlyIdea->evaluado = Idea_evaluate::where('idea_id', $onlyIdea->id)->first()? Idea_evaluate::where('idea_id', $onlyIdea->id)->first()->created_at : null;

            $onlyIdea->evaluado_final = Idea_execute::where('idea_id', $onlyIdea->id)->first()? Idea_execute::where('idea_id', $onlyIdea->id)->first()->created_at : null;

            $onlyIdea->ejecutado = Idea_execute::where('idea_id', $onlyIdea->id)->first()? Idea_execute::where('idea_id', $onlyIdea->id)->first()->start_day : null;

            $onlyIdea->jefeoperador = Idea_execute::where('idea_id', $onlyIdea->id)->first() && Idea_execute::where('idea_id', $onlyIdea->id)->first()->operator_boss_user_id? User::where('id',Idea_execute::where('idea_id', $onlyIdea->id)->first()->operator_boss_user_id)->first()->name : null;

            $operadores = Idea_execute::where('idea_id', $onlyIdea->id)->first() && Idea_execute::where('idea_id', $onlyIdea->id)->first()->operator_boss_user_id ?
            Idea_operator::where('idea_execute_id', Idea_execute::where('idea_id', $onlyIdea->id)->first()->id)->get() : null;
            
            $newOperadores = [];
            if($operadores){
                foreach($operadores as $operador){
                    $newVal = User::find($operador->operator_user_id)->name;
                    array_push($newOperadores, $newVal);
                }
            }

            $onlyIdea->operadores = $newOperadores? implode(",",$newOperadores) : null;

            $onlyIdea->premiacion = Idea_execute::where('idea_id', $onlyIdea->id)->first() && Idea_execute::where('idea_id', $onlyIdea->id)->first()->end_day && $onlyIdea->state == 'premiada'? Idea_execute::where('idea_id', $onlyIdea->id)->first()->end_day : null;
        }

        $name = 'Estadísticas de maquinando ideas';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Estadísticas');
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', $name);
        $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Titulo');
        $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Id');
        $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Innovador');
        $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Supervisor');
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Categoría');
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Estado');
        $objPHPExcel->getActiveSheet()->setCellValue('G3', 'Fecha de creación de idea');
        $objPHPExcel->getActiveSheet()->setCellValue('H3', 'Fecha de envío de idea');
        $objPHPExcel->getActiveSheet()->setCellValue('I3', 'Fecha de evaluación');
        $objPHPExcel->getActiveSheet()->setCellValue('J3', 'Fecha de ejecución');
        $objPHPExcel->getActiveSheet()->setCellValue('K3', 'Jefe ejecutor');
        $objPHPExcel->getActiveSheet()->setCellValue('L3', 'Ejecutores');
        $objPHPExcel->getActiveSheet()->setCellValue('M3', 'Fecha de premiación');
        

        $count = 4;

        foreach($ideas as $idea) {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$count, $idea->title);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$count, $idea->id);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$count, $idea->innovador);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$count, $idea->supervisor);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$count, $idea->categoria);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$count, $idea->state);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$count, $idea->created_at);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$count, $idea->evaluado);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$count, $idea->evaluado_final);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$count, $idea->ejecutado);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$count, $idea->jefeoperador);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$count, $idea->operadores);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$count, $idea->premiacion);
            
            $count++;
        }

        foreach(range('A','M') as $columnID) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        //Esto solo funciona con archivos exportados que no sean CSV ya que el csv es un texto y no se puede agregar estilos
		//cambiar el font size
		$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setSize(16);
		//hacer que la fuente sea bold
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFont()->setBold(true);

        $filename = $name . date('Y_m_d_H_i_s') . '.xlsx'; //guardar el libro con el nombre
		
		header("Content-type:application;charset=UTF-8");
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		//guardarlo en el formato
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
    }

    public function update_request(Request $request){
        return $request->email;
        $request = Requests::find($id);
        $request->state = 1;
        $request->save();

        return $request;
    }

    public function getAllChatsRooms(){
        $getId = DB::select('CALL getIdeasAllByUserId("'. Auth::id() .'")');
        foreach($getId as &$id){
            $id = $id->id;
        }
        $chat_evaluate = Idea_evaluate::whereIn('idea_id', $getId)->get();
        $chat_execute = Idea_execute::whereIn('idea_id', $getId)->get();
        $getAllChatRooms = [];
        foreach($chat_evaluate as $chatroom){
            $data = Chat_room::find($chatroom->chat_room_id);
            $data->type = 'evaluate';
            $data->idea = $chatroom->idea_id;
            $data->chat = Chat::where('chat_room_id', $data->id)->get();
            array_push($getAllChatRooms, $data);
        }

        foreach($chat_execute as $chatroom){
            $data = Chat_room::find($chatroom->chat_room_id);
            $data->type = 'execute';
            $data->idea = $chatroom->idea_id;
            $data->chat = Chat::where('chat_room_id', $data->id)->get();
            array_push($getAllChatRooms, $data);
        }

        return $getAllChatRooms;
    }
    public function getAllChatsRoomsSupervisor(){
        $admin_id = DB::select("CALL administradorById(".Auth::id().")");
        //$ideas = DB::select("CALL ideasGetAll(".$admin_id[0]->id.")");
        $newgetId = Idea::where('admin_id', $admin_id[0]->id)->get();
        
        $getId = [];
        foreach($newgetId as $id){
            array_push($getId, $id->id);
        }

        $chat_evaluate = Idea_evaluate::whereIn('idea_id', $getId)->get();
        $chat_execute = Idea_execute::whereIn('idea_id', $getId)->get();
        $getAllChatRooms = [];
        foreach($chat_evaluate as $chatroom){
            $data = Chat_room::find($chatroom->chat_room_id);
            $data->type = 'evaluate';
            $data->idea = $chatroom->idea_id;
            $data->chat = Chat::where('chat_room_id', $data->id)->get();
            array_push($getAllChatRooms, $data);
        }
        foreach($chat_execute as $chatroom){
            $data = Chat_room::find($chatroom->chat_room_id);
            $data->type = 'execute';
            $data->idea = $chatroom->idea_id;
            $data->chat = Chat::where('chat_room_id', $data->id)->get();
            array_push($getAllChatRooms, $data);
        }
        return $getAllChatRooms;
    }
}
