<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categorias = Category::all();
        return $categorias;
    }

    public function get_category_by_id($category_id)
    {
        return Category::find($category_id);
    }
}
