<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Idea;
use App\Administrator;
use App\Idea_execute;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Notification;
use App\Idea_operator;
use App\Mail\MailUserCreated;
use App\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Contracts\Encryption\DecryptException;


class UserController extends Controller
{

    public $successStatus = 200;


    private function setIdeas($ideas){
        foreach($ideas as $idea){
            $admin = $idea->admin_id?  Administrator::find($idea->admin_id)->user_id : null;
            $idea->category_name = Category::find($idea->category_id)->name;
            $idea->problems = $idea->problems;
            $idea->usercreator = User::find($idea->user_id)->name;
            $idea->usercreatorall = User::find($idea->user_id);
            $idea->administratorcreator = User::find($admin);
            if($idea->state == 'guardado'){
                $idea->date_start = $idea->created_at->diffForHumans();
            }
            if($idea->state == 'evaluacion'){
                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $newVal =  $idea->idea_evaluate[0]->created_at? $idea->idea_evaluate[0]->created_at->diffForHumans() : null;
                $idea->date_difference = Carbon::parse($idea->idea_evaluate[0]->end_day)->diffInDays(Carbon::parse(Carbon::now()));
                $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
                $idea->date_start = 'La evaluacion inicio '.$newVal;
            }
            if($idea->state == 'ejecucion'){
                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $idea->idea_execute_create = $idea->idea_evaluate[0]->created_at;

                $newVal =  $idea->idea_execute[0]->created_at? Carbon::parse($idea->idea_execute[0]->created_at)->diffForHumans() : null;
                $idea->date_difference = Carbon::parse($idea->idea_evaluate[0]->end_day)->diffInDays(Carbon::parse(Carbon::now()));
                $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
                $idea->date_start = 'La ejecución inicio '.$newVal;
                $getOperators = Idea_operator::where('idea_execute_id',$idea->idea_execute[0]->id)->get();
                $idea->idea_execute = $idea->idea_execute[0];
                $newOperators = [];
                foreach($getOperators as $operator){
                    array_push($newOperators, User::select('id','name','avatar')->where('id', $operator->operator_user_id)->first());
                }
                $idea->getOperators = $newOperators;
                $idea->getBossOperators = User::select('id','name','avatar')->where('id',$idea->idea_execute->operator_boss_user_id)->first();
            }
            if($idea->state == 'aprobado'){
                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $newVal =  $idea->idea_execute[0]->created_at? $idea->idea_execute[0]->created_at->diffForHumans() : null;
                $idea->date_start = 'La idea se aprobó '.$newVal;
                $idea->idea_aprobed_create = $idea->idea_execute[0]->created_at;
                if (property_exists($idea, 'idea_execute')) {
                    $idea->idea_execute = $idea->idea_execute[0];
                } else {
                    $idea->idea_execute = [];
                }
            }
            if($idea->state == 'desaprobado'){
                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $idea->date_start = 'La idea se desaprobó '. $idea->updated_at->diffForHumans();
                $idea->idea_disapproved_create = $idea->updated_at;
                if (property_exists($idea, 'idea_execute')) {
                    $idea->idea_execute = $idea->idea_execute[0];
                } else {
                    $idea->idea_execute = [];
                }
            }
            if($idea->state == 'abortado'){
                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $idea->date_start = 'La idea se abortó '. $idea->idea_execute[0]->updated_at->diffForHumans();
                $idea->idea_disapproved_create = $idea->updated_at;
                $getOperators = Idea_operator::where('idea_execute_id',$idea->idea_execute[0]->id)->get();
                $newOperators = [];
                foreach($getOperators as $operator){
                    array_push($newOperators, User::select('id','name','avatar')->where('id',$operator->operator_user_id)->first());
                }
                $idea->getOperators = $newOperators;
                $idea->getBossOperators = User::select('id','name','avatar')->where('id',$idea->idea_execute[0]->operator_boss_user_id)->first();
            }
            if($idea->state == 'premiada'){
                $idea->idea_evaluate_create = $idea->idea_evaluate[0]->created_at;
                $idea->idea_execute_create = $idea->idea_evaluate[0]->created_at;

                $newVal =  $idea->idea_execute[0]->end_day? Carbon::parse($idea->idea_execute[0]->end_day)->diffForHumans() : null;
                $idea->state_time = ($idea->date_difference * 100)/$idea->idea_evaluate[0]->evaluate_time;
                $idea->test = $idea->idea_execute[0];
                $idea->date_start = 'La idea concluyó '.$newVal;
                $idea->finish = $idea->idea_execute[0]->end_day;
                $getOperators = Idea_operator::where('idea_execute_id',$idea->idea_execute[0]->id)->get();
                $newOperators = [];
                foreach($getOperators as $operator){
                    array_push($newOperators, User::select('id','name','avatar')->where('id',$operator->operator_user_id)->first());
                }
                $idea->getOperators = $newOperators;
                $idea->getBossOperators = User::select('id','name','avatar')->where('id',$idea->idea_execute[0]->operator_boss_user_id)->first();
            }
        }

        $allIdeas = [];

        foreach($ideas as $idea){
            array_push($allIdeas, $idea);
        }

        usort($allIdeas, array($this, 'custom_sort'));

        return $allIdeas;
    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $first = User::where( 'username', request('username') )->first();
        if($first){
            if($first->state != false){
                if (Auth::attempt(['email' => request('email'), 'password' => request('password')]))
                    {
                        $user = Auth::user();
                        $success['token'] =  $user->createToken('MyApp')->accessToken;
                        return 'success';
                    }
                    else
                    {
                        return 'Error en el servidor';
                    }
            }else{
                return 'El usuario no se encuentra activo en la plataforma';
            }
        }else{
            return 'Nombre de usuario no registrado';
        }


    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function check_login() {
      if (Auth::check()) {
        return 'success';
      } else {
        return 'error';
      }
    }

    public function logout()
    {
        Auth::logout();
    }

    public function auth()
    {
        $user = Auth::user();
        $select_user = User::find($user->id);
        $ideas = sizeof(Idea::where('user_id', Auth::id())->get());
        $premios = sizeof(Idea::where('state', 'finalizado')->get());
        $select_user->ideasNumber = $ideas;
        $select_user->premios = $premios;
        $select_user->administrator = sizeof($select_user->administrator) > 0? $select_user->administrator[0] : null;
        $select_user->allUsers = User::all();
        return $select_user;
    }

    public function getNotifications(){
        $result = Notification::where('user_id', Auth::id())->where('type', '!=', 'chat')->get();

        //Obtener id de ideas
        $ideas = Idea::where('user_id',Auth::id())->get();
        $admin = Administrator::where('user_id', Auth::id())->first();

        $ideas_admin = [];
        if ($admin !== null) {
            $ideas_admin = Idea::where('admin_id',$admin->id)->get();
        }
        $ideasId = [];
        foreach($ideas as $idea){
            array_push($ideasId, $idea->id);
        }
        $ideasAdminId = [];
        foreach($ideas_admin as $idea){
            array_push($ideasAdminId, $idea->id);
        }


        //Operadores
        $_ideasEjecutor = Idea_operator::where('operator_user_id', Auth::id())->get();
        $_getExecuteIdeas = [];
        foreach($_ideasEjecutor as $operator){
            array_push($_getExecuteIdeas, $operator->idea_execute_id);
        }
        $_ideasExecute = Idea_execute::whereIn('id', $_getExecuteIdeas)->get();
        $_getIdeas = [];
        foreach($_ideasExecute as $operator){
            array_push($_getIdeas, $operator->idea_id);
        }

        //Jefe ejecutor
        $_ideasExecute1 = Idea_execute::where('operator_boss_user_id', Auth::id())->get();
        $_getIdeas1 = [];
        foreach($_ideasExecute1 as $operator){
            array_push($_getIdeas1, $operator->idea_id);
        }
        foreach($_getIdeas1 as $idea){
            array_push($_getIdeas, $idea);
        }


        //Obtener las notificaciones que pertenecen a esas ideas y sean CHAT
        $newResult = Notification::whereIn('idea_id', $ideasId)->where('type','chat')->where('user_id', Auth::id())->get();
        $newResultEjecutor = Notification::whereIn('idea_id', $_getIdeas)->where('type','chat')->where('user_id', Auth::id())->get();
        $newResultAdmin = Notification::whereIn('idea_id', $ideasAdminId)->where('type','chat')->where('user_id', Auth::id())->get();


        $allNotifications = [];
        $allAdminNotifications = [];

        foreach($result as $notification){
            if($notification->isadmin == 0){
                array_push($allNotifications, $notification);
            }
        }

        foreach($result as $notification){
            if($notification->isadmin == 1){
                array_push($allAdminNotifications, $notification);
            }
        }

        foreach($newResult as $notification){
            array_push($allNotifications, $notification);
        }

        foreach($newResultEjecutor as $notification){
            array_push($allNotifications, $notification);
        }



        /*foreach($ideasEjecutorEvaluateIdeasGet as $idea){
            array_push($allNotifications, $idea);
        }
        foreach($ideasBoss2 as $idea){
            array_push($allNotifications, $idea);
        }*/


        foreach($newResultAdmin as $notification){
            array_push($allAdminNotifications, $notification);
        }



        foreach($allNotifications as $notification){
            //$idea = Idea::where('id', $notification->idea_id)->get()[0]->state;
            $getLink = '';

            if($notification->type != 'chat'){
                // Filtrar por estado de la idea
                if(strpos($notification->title, "aprobado") !== false || strpos($notification->title, "evaluación") !== false){$getLink = 'idea/evaluacion/'.$notification->link;}
                if(strpos($notification->title, "desaprobado") !== false){$getLink = 'idea/guardado/'.$notification->link;}
                if(strpos($notification->title, "ejecución") !== false){$getLink = 'idea/ejecucion/'.$notification->link;}
                if(strpos($notification->title, "premiada") !== false){$getLink = 'idea/premiada/'.$notification->link;}
            }else{
                // Filtrar por estado de la idea
                if($notification->link == "aprobado" || $notification->link == "evaluacion"){$getLink = 'idea/evaluacion/'.$notification->idea_id;}
                if($notification->link == "desaprobado"){$getLink = 'idea/guardado/'.$notification->idea_id;}
                if($notification->link == "ejecucion"){$getLink = 'idea/ejecucion/'.$notification->idea_id;}
                if($notification->link == "premiada"){$getLink = 'idea/premiada/'.$notification->idea_id;}
                $notification->role = 'user';
            }

            $notification->time = Carbon::parse($notification->created_at)->diffForHumans();
            $notification->userProvider = [User::find($notification->provider)->name, User::find($notification->provider)->avatar];
            $notification->getLink = $getLink;
        }

        foreach($allAdminNotifications as $notification){
            //$idea = Idea::where('id', $notification->idea_id)->get()[0]->state;
            $getLink = '';

            if($notification->type != 'chat'){
                // Filtrar por estado de la idea
                if(strpos($notification->title, "aprobado") !== false || strpos($notification->title, "evaluación") !== false){$getLink = 'idea/evaluacion/'.$notification->link;}
                if(strpos($notification->title, "desaprobado") !== false){$getLink = 'idea/guardado/'.$notification->link;}
                if(strpos($notification->title, "ejecución") !== false){$getLink = 'idea/ejecucion/'.$notification->link;}
                if(strpos($notification->title, "premiada") !== false){$getLink = 'idea/premiada/'.$notification->link;}
            }else{
                // Filtrar por estado de la idea
                if($notification->link == "aprobado" || $notification->link == "evaluacion"){$getLink = 'idea/evaluacion/'.$notification->idea_id;}
                if($notification->link == "desaprobado"){$getLink = 'idea/guardado/'.$notification->idea_id;}
                if($notification->link == "ejecucion"){$getLink = 'idea/ejecucion/'.$notification->idea_id;}
                if($notification->link == "premiada"){$getLink = 'idea/premiada/'.$notification->idea_id;}
                $notification->role = 'admin';
            }


            $notification->time = Carbon::parse($notification->created_at)->diffForHumans();
            $notification->userProvider = [User::find($notification->provider)->name, User::find($notification->provider)->avatar];
            $notification->getLink = $getLink;
        }

        usort($allAdminNotifications, array($this, 'custom_sort2'));
        usort($allNotifications, array($this, 'custom_sort2'));

        return $admin && $admin->state? $allAdminNotifications : $allNotifications ;
    }

    public function authIdeas()
    {
        /*$ideas_all = DB::select('CALL getIdeaById("'.Auth::id().'")');
        $ideas_ejecutadas_all = DB::select('CALL getIdeasExecutesById("'.Auth::id().'")');
        $ideas_operador_all = DB::select('CALL getIdeasOperatorsById("'.Auth::id().'")');
        $ideas = [];

        foreach($ideas_all as $idea){
            array_push($ideas, $idea);
        }

        foreach($ideas_ejecutadas_all as $idea){
            $idea->operator = true;
            array_push($ideas, $idea);
        }

        foreach($ideas_operador_all as $idea){
            $idea->operator = true;
            array_push($ideas, $idea);
        }

        return $this->setIdeas($ideas);

        //return $this->setIdeas($ideas);*/
        $operator_ideas = Idea_operator::where('operator_user_id', Auth::id())->get();

        $getideas_operator_ejecucion = [];
        foreach($operator_ideas as $idea){
            $new = Idea_execute::find($idea->idea_execute_id)->idea_id;
            array_push($getideas_operator_ejecucion, $new);
        }

        $getideas_operator_idea = [];
        foreach($getideas_operator_ejecucion as $idea){
            $new = Idea::find($idea);
            $new->operator = true;
            array_push($getideas_operator_idea, $new);
        }

        $boss = Idea_execute::where('operator_boss_user_id', Auth::id())->get();
        $boss_idea = [];
        foreach($boss as $idea){
            $new = Idea::find($idea->idea_id);
            $new->operator = true;
            array_push( $boss_idea, $new );
        }
        $ideas_only = Idea::where('user_id', Auth::id())->get();
        $ideas = [];
        foreach($boss_idea as $idea){
            array_push( $ideas, $idea );
        }
        foreach($ideas_only as $idea){
            $new = Idea::find($idea->id);
            array_push( $ideas, $new );
        }
        foreach($getideas_operator_idea as $idea){
            array_push( $ideas, $idea );
        }

        return $this->setIdeas($ideas);

    }

    private function custom_sort($a, $b)
    {
        if (strtotime($a->updated_at) == strtotime($b->updated_at)) {
            return 0;
        }
        return (strtotime($a->updated_at) > strtotime($b->updated_at)) ? -1 : 1;
    }

    private function custom_sort2($a, $b)
    {
        if (strtotime($a->created_at) == strtotime($b->created_at)) {
            return 0;
        }
        return (strtotime($a->created_at) > strtotime($b->created_at)) ? -1 : 1;
    }

    public function supervisorIdeas()
    {
        $admin_id = DB::select("CALL administradorById(".Auth::id().")");
        //$ideas = DB::select("CALL ideasGetAll(".$admin_id[0]->id.")");
        $ideas = Idea::where('admin_id', $admin_id[0]->id)->get();

        return $this->setIdeas($ideas);
    }



    public function AdminIdeas($type)
    {
        if($type == 'por-evaluar'){ $val = 'evaluacion'; }
        else if($type == 'desaprobados') {$val = 'desaprobado';}
        else if($type == 'en-ejecucion') {$val = 'ejecucion';}
        else{$val = $type;}
        $admin = Administrator::where('user_id', Auth::id())->first();
        $ideas = Idea::where('admin_id', $admin->id)->where('state', $val)->get();
        return $ideas;
    }

    public function allIdeasIsAdministrator($admin, $type)
    {
        $administrator = Idea::where('admin_id', $admin)->where('state', $type)->take(3)->get();
        return $administrator;
    }

    public function isAdministrator($user)
    {
        $validate = Administrator::where('user_id', $user)->get();
        return $validate;
    }

    public function get_administrator($admin_id) {
        $admin =  Administrator::with('user')->where('id', $admin_id)->first();

        return $admin;
    }
    public function allUsers()
    {
        $users = User::all();
        return $users;
    }

    public function update_user(Request $request) {
        $user = User::find(auth::id());
        if($request->phone !== null){
            $user->phone = request('phone');
        }
        if($request->sex !== null){
            $user->gender = request('sex');
        }
        if($request->file('avatar') !== null){
            $path_avatar = Storage::disk('avatars')->put('', $request->file('avatar'));
            $user->avatar = $path_avatar;
        }
        $result = $user->save();
        $select_user = User::find($user->id);
        $ideas = sizeof(Idea::where('user_id', Auth::id())->get());
        $premios = sizeof(Idea::where('state', 'finalizado')->get());
        $select_user->ideasNumber = $ideas;
        $select_user->premios = $premios;
        $select_user->administrator = sizeof($select_user->administrator) > 0? $select_user->administrator[0] : null;
        $select_user->allUsers = User::all();
        return $select_user;
    }

    public function create_user(Request $request) {
        $validator = Validator::make($request->all(), [
            'names' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'repeat_password' => 'required|same:password',
            'sex' => 'required',
            'number' => 'required',
            'position' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $user = new User;
        $user->username = $request['username'];
        $user->name = $request['names'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->password1 = 'ss';
        $user->phone = $request['number'];
        $user->gender = $request['sex'];
        $user->state = 1;



        if ($user->save()) {

            if ($request['position'] == 1) {
                $admin = new Administrator;
                $admin->user_id = $user->id;

                if (!$admin->save()) {
                    return response()->json(['data' => $user, 'message' => 'No se ha podido colocar al usuario como administrador'], 201);
                }
            }
        } else {
            return response()->json(['error' => 'No se ha podido crear el usuario'], 500);
        }

        // enviar correo
        $transport = (new \Swift_SmtpTransport('smtp.1and1.com', 587,'tls'))->setUsername('msoto@ncomunicaciones.com')->setPassword('Ms0t0#01');
        $view = view('layouts.usercreated', ['user' => $user, 'password' => $request['password'] ]);
        $body = (string) $view;
        //$body = $view->render();
        $mailer = (new \Swift_Mailer($transport));
        $message = (new \Swift_Message('Asunto'))
        ->setFrom(array('procesos@unimaq.com.pe' => 'Maquinando Ideas - Administrador'))
        ->setTo(array($user->email => $user->name))
        ->setBody($body, 'text/html');
        $result = $mailer->send($message);

        $allRequests = Requests::where('user_id',Auth::id())->get();
        foreach($allRequests as $request1){
            if(sizeof(unserialize($request1->data)) > 1){
                if($request['email'] == unserialize($request1->data)[1]){
                    $request1->state = 1;
                    $request1->save();
                }
            }
        }

        return $user;
    }

    public function edit_user(Request $request){

        $validator = Validator::make($request->all(), [
            'names' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'number' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $user = User::find($request->id);
        $user->username = $request['username'];
        $user->name = $request['names'];
        $user->email = $request['email'];
        $user->password = $request['password']? bcrypt($request['password']) : $user->password;
        $user->phone = $request['number']? $request['number'] : $user->number;
        $user->gender = $request['sex']? $request['sex'] : $user->gender;
        $user->save();

        if(isset($request['password'])){
            $allRequests = Requests::where('user_id',Auth::id())->get();
            foreach($allRequests as $request1){
                if($request1->type == 'update'){
                    if($request['username'] == unserialize($request1->data)[0]){
                        $request1->state = 1;
                        $request1->save();
                    }
                }
            }
        }

        if($request['password']){
            $transport = (new \Swift_SmtpTransport('smtp.1and1.com', 587,'tls'))->setUsername('msoto@ncomunicaciones.com')->setPassword('Ms0t0#01');
            $view = view('layouts.usercreated', ['user' => $user, 'password' => $request['password'], 'state' => '1']);
            $body = (string) $view;
            //$body = $view->render();
            $mailer = (new \Swift_Mailer($transport));
            $message = (new \Swift_Message('Asunto'))
            ->setFrom(array('procesos@unimaq.com.pe' => 'Maquinando Ideas - Administrador'))
            ->setTo(array($user->email => $user->name))
            ->setBody($body, 'text/html');
            $result = $mailer->send($message);
        }

        return $user;
    }

    public function recoverPassword(Request $request)
    {
        $user = User::where('username', $request->username)->first();

        if(!$user){
            $user = User::where('email', $request->username)->first();
        }

        if($user){
            $validator = Validator::make($request->all(), [
                'username' => 'required'
            ]);
            // enviar correo
            $transport = (new \Swift_SmtpTransport('smtp.1and1.com', 587,'tls'))->setUsername('msoto@ncomunicaciones.com')->setPassword('Ms0t0#01');
            $new_password = 'user_' . strtotime(date('Y-m-d H:i:s'));
            $view = view('layouts.usercreated', ['user' => $user, 'password' => $new_password]);
            $body = (string) $view;
            //$body = $view->render();
            $mailer = (new \Swift_Mailer($transport));
            $message = (new \Swift_Message('Reestablecimiento de Contraseña'))
            ->setFrom(array('procesos@unimaq.com.pe' => 'Maquinando Ideas - Administrador'))
            ->setTo(array($user->email => $user->name))
            ->setBody($body, 'text/html');
            $result = $mailer->send($message);

            $user->password = bcrypt($new_password);
            $user->save();

            return 'success';
        } else {
            return 'error';
        }
    }
    public function newUser(Request $request)
    {
        $users = User::where('email', $request->email)->get();
        $request1 = null;
        $requests = Requests::all();
        foreach($requests as $request2){
            if($request2->type == 'create' && $request2->state == 0){
                $allvals = unserialize($request2->data);
                if($allvals[1] == $request->email){
                    $request1 = $request2;
                }
            }
        }

       if(sizeof($users)){
            return 'error1';
       }

       if($request1){
            return 'error2';
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $newVal = [$request->name, $request->email, $request->phone];

        $rand = [];
        foreach(Administrator::all() as $admin){
            array_push($rand, $admin->id);
        }
        $random_admin = array_random($rand);
        $getAdmin = Administrator::find($rand)[0]->user_id;

        Requests::create([
            'type' => 'create',
            'data' => serialize($newVal),
            'user_id' => $getAdmin
        ]);

        $user = new \stdClass();
        $user->name = $request->name;

        $transport = (new \Swift_SmtpTransport('smtp.1and1.com', 587,'tls'))->setUsername('msoto@ncomunicaciones.com')->setPassword('Ms0t0#01');
        $view = view('layouts.usercreated', ['user' => $user, 'account' => true ]);
        $body = (string) $view;
        //$body = $view->render();
        $mailer = (new \Swift_Mailer($transport));
        $message = (new \Swift_Message('Asunto'))
        ->setFrom(array('procesos@unimaq.com.pe' => 'Maquinando Ideas - Administrador'))
        ->setTo(array($request->email => $request->name))
        ->setBody($body, 'text/html');
        $result = $mailer->send($message);

        return 'success';
    }
    public function requests()
    {
        $allRequests = Requests::where('user_id',Auth::id())->get();
        foreach($allRequests as $request){
            $allvals = unserialize($request->data);
            $request->name = $allvals[0];
            if(sizeof($allvals) > 1){
                $request->email = $allvals[1];
                $request->phone = $allvals[2];
            }
        }
        return $allRequests;
    }

    public function readNotifications($id){
        $getNotification = Notification::find($id);
        $getNotification->read = true;
        $getNotification->save();

        return $getNotification;
    }

    public function getUsers(){
        $allUsers = User::all();
        foreach($allUsers as $user){
            $get = Administrator::where('user_id', $user->id)->get();
            $user->admin = sizeof($get);
        }
        return $allUsers;
    }

    public function create_category(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        $result = Category::create([
            'name' => request('name'),
            'slug' => str_slug(request('name')),
            'description' => request('description'),
            'state' => true,
            'image'=> 'image'
        ]);
        if($request->file('image')){
            $path_image = Storage::disk('avatars')->put('', $request->file('image'));
            $result->fill(['image' => $path_image])->save();
        }
        return $result;
    }

    public function edit_category(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'description' => 'required'
        ]);
        $categoria = Category::find(request('id'));
        $categoria->name = request('name');
        $categoria->slug = str_slug(request('name'));
        $categoria->description = request('description');

        if($request->file('image')){
            $path_image = Storage::disk('avatars')->put('', $request->file('image'));
            $categoria->fill(['image' => $path_image])->save();
        }

        $categoria->save();

        return $categoria;
    }
    public function change_category(Request $request){
        $getVal = Category::find($request->id);
        $getVal->state = $request->value;
        $getVal->save();
        return $getVal;
    }
    public function change_user(Request $request){
        $getVal = User::find($request->id);
        $getVal->state = $request->value;
        $getVal->save();
        return $getVal;
    }

    public function changeRole($value){
        $get = Administrator::where('user_id', Auth::id())->first();
        $get->state = $value;
        $get->save();
        return User::find(Auth::id());
    }

    public function seeChanges()
    {
        $result = DB::select('CALL selectNotificationsNotChat('. Auth::id() . ', 0' .')');
        $newResponse = [];
        foreach($result as $resu){
            array_push($newResponse, $this->getNotificationUniq($resu, 'user'));
        }
        return $newResponse;
    }
    public function seeChangesSup()
    {
        $adminrow = DB::select('CALL administradorById('.Auth::id().')');
        $result = DB::select('CALL selectNotificationsNotChatAdmin('. Auth::id() . ',' . $adminrow[0]->id .')');
        $newResponse = [];
        foreach($result as $resu){
            array_push($newResponse, $this->getNotificationUniq($resu, 'admin'));
        }
        return $newResponse;
    }

    public function getNotificationUniq($notification, $user){
        /*$idea = DB::select('CALL getIdeaByCurrentId("'.$notification->idea_id.'")');

        if ($idea && isset($idea[0])) {
            $idea = $idea[0]->state;
        }*/

        $user1 = DB::select('CALL getUserById("'.$notification->provider.'")')[0];
        $getLink = '';

        if($notification->type != 'chat'){
            // Filtrar por estado de la idea
            if(strpos($notification->title, "aprobado") !== false || strpos($notification->title, "evaluación") !== false){$getLink = 'idea/evaluacion/'.$notification->link;}
            if(strpos($notification->title, "desaprobado") !== false){$getLink = 'idea/guardado/'.$notification->link;}
            if(strpos($notification->title, "ejecución") !== false){$getLink = 'idea/ejecucion/'.$notification->link;}
            if(strpos($notification->title, "premiada") !== false){$getLink = 'idea/premiada/'.$notification->link;}
        } else {
            // Filtrar por estado de la idea
            if($notification->link == "aprobado" || $notification->link == "evaluacion"){$getLink = 'idea/evaluacion/'.$notification->idea_id;}
            if($notification->link == "desaprobado"){$getLink = 'idea/guardado/'.$notification->idea_id;}
            if($notification->link == "ejecucion"){$getLink = 'idea/ejecucion/'.$notification->idea_id;}
            if($notification->link == "premiada"){$getLink = 'idea/premiada/'.$notification->idea_id;}
            $notification->role = $user == 'user' ? 'user' : 'admin';
        }

        $notification->time = Carbon::parse($notification->created_at)->diffForHumans();
        $notification->userProvider = [$user1->name, $user1->avatar];
        $notification->getLink = $getLink;

        return $notification;
    }
}
