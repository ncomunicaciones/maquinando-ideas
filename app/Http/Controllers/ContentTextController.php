<?php

namespace App\Http\Controllers;

use App\ContentText;
use Illuminate\Http\Request;
use Validator;

class ContentTextController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'slug' => 'required',
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors(), 'result' => null], 400);
        }

        $content_text = new ContentText;
        $content_text->description = $request['description'];
        $content_text->slug = $request['slug'];
        $content_text->text = $request['text'];
        
        if ($content_text->save()) {
            return response()->json(['error' => null, 'result' => $content_text], 200);
        } else {
            return response()->json(['error' => $validator->errors(), 'result' => null], 500);
        }
    }

    public function show()
    {
        return ContentText::all();
    }

    public function update(Request $request, $content_text_id)
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors(), 'result' => null], 400);
        }

        $content_text = ContentText::find($content_text_id);
        $content_text->text = $request['text'];
        $content_text->save();

        return $content_text;
    }
}
