<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Idea;
use App\Idea_evaluate;
use App\Idea_execute;
use App\Chat;
use App\User;
use Carbon\Carbon;
use App\Chat_room;
use App\Administrator;
use App\Notification;
use App\Idea_operator;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index($idea, $state)
    {
        $chat = Chat::where('idea_id', $idea)->where('state', $state)->get();
    }
    public function message(Idea $idea, Request $request)
    {
        $set = Idea_execute::where('idea_id', $idea->id)->first();
        if($set){
            $var = Chat_room::find($set->chat_room_id);
            $send = Chat::create([
                'chat_room_id' => $var->id,
                'user_id' => Auth::id(),
                'message' => request('message'),
            ]);
        }else{
            $set = Idea_evaluate::where('idea_id', $idea->id)->first();
            $var = Chat_room::find($set->chat_room_id);
            $send = Chat::create([
                'chat_room_id' => $var->id,
                'user_id' => Auth::id(),
                'message' => request('message'),
            ]);
        }

        return 'success';
        
    }

    public function get_messages($chat_room) {
        return Chat::where('chat_room_id', $chat_room)->get();
    }




    public function getChat($idea, $state){
        $idea_select = Idea::where('id', $idea)->first();
        $idea_select_user = Idea::where('id', $idea)->first()->user_id;
        $idea_select_supervisor = Administrator::find(Idea::where('id', $idea)->first()->admin_id)->user_id;
        $state_select = '';
        if($state == 'evaluate'){
            $state_select = Idea_evaluate::where('idea_id', $idea_select->id)->first();
            $chatRoom = $state_select->chat_room_id;
        }else{
            $state_select = Idea_execute::where('idea_id', $idea_select->id)->first();
            $chatRoom = $state_select->chat_room_id;
        }

        $AllMessages = Chat::where('chat_room_id', $chatRoom)->orderBy('id', 'desc')->take(5)->get();
        foreach($AllMessages as $message){
            $message->getuser = User::where('id', $message->user_id)->first();
            $message->getTime = Carbon::parse($message->created_at)->diffForHumans();
            if($message->user_id == $idea_select_user){
                $message->identify = 'innovador';
            }else if($message->user_id == $idea_select_supervisor){
                $message->identify = 'supervisor';
            }else{
                $message->identify = 'ejecutor';
            }
        }
        return $AllMessages;
    }

    
    public function sendChat(Request $request, $chatroom, $state){

        $getChatroom = Chat_room::find($chatroom);
        $send = Chat::create([
            'chat_room_id' => $getChatroom->id,
            'user_id' => Auth::id(),
            'message' => $request->message,
        ]);
        if($state == 'evaluate'){
            $idea_select = Idea::find(Idea_evaluate::where('chat_room_id', $getChatroom->id)->first()->idea_id);
        }else{
            $idea_select = Idea::find(Idea_execute::where('chat_room_id', $getChatroom->id)->first()->idea_id);
        }

        if($state != 'evaluate'){
            $getExecute = Idea_execute::where('chat_room_id', $getChatroom->id)->first();
            
            //Operadores
            $getExecuteOperador = Idea_operator::where('idea_execute_id', $getExecute->id)->get();
            $getExecuteOperators = [];
            foreach($getExecuteOperador as $user){
                array_push($getExecuteOperators, $user->operator_user_id);
            }
            //Jefe Operador
            $getExecuteBoss = $getExecute->operator_boss_user_id;
            array_push($getExecuteOperators, $getExecuteBoss);
            $setRowsValidate = in_array(Auth::id(), $getExecuteOperators);
            $setRows = array_search(Auth::id(), $getExecuteOperators);
            if($setRowsValidate){
                unset($getExecuteOperators[$setRows]);
            }
            
        }

        if($idea_select->user_id == Auth::id()){
            Notification::create([
                'read' => false,
                'title' => $request->message ,
                'type' => 'chat',
                'user_id' => Administrator::find($idea_select->admin_id)->user_id,
                'idea_id' => $idea_select->id,
                'provider' => Auth::id(),
                'link' => $idea_select->state,
                'isAdmin' => true
            ]);
        }else if(Administrator::find($idea_select->admin_id)->user_id == Auth::id()){
            Notification::create([
                'read' => false,
                'title' => $request->message ,
                'type' => 'chat',
                'user_id' => $idea_select->user_id,
                'idea_id' => $idea_select->id,
                'provider' => Auth::id(),
                'link' => $idea_select->state,
                'isAdmin' => false
            ]);
        }else{
            Notification::create([
                'read' => false,
                'title' => $request->message ,
                'type' => 'chat',
                'user_id' => $idea_select->user_id,
                'idea_id' => $idea_select->id,
                'provider' => Auth::id(),
                'link' => $idea_select->state,
                'isAdmin' => false
            ]);
            Notification::create([
                'read' => false,
                'title' => $request->message ,
                'type' => 'chat',
                'user_id' => Administrator::find($idea_select->admin_id)->user_id,
                'idea_id' => $idea_select->id,
                'provider' => Auth::id(),
                'link' => $idea_select->state,
                'isAdmin' => true
            ]);
        }
        if($state != 'evaluate'){
            foreach($getExecuteOperators as $user){
                Notification::create([
                    'read' => false,
                    'title' => $request->message ,
                    'type' => 'chat',
                    'user_id' => $user,
                    'idea_id' => $idea_select->id,
                    'provider' => Auth::id(),
                    'link' => $idea_select->state,
                    'isAdmin' => $user == Administrator::find($idea_select->admin_id)->user_id? true : false
                ]);
            }
        }
        

        $data = $getChatroom;
        $data->chat = Chat::where('chat_room_id', $data->id)->get();
        return $data;
    }
}
