<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
    protected $guarded = [];

    public function idea() {
        return $this->belongsTo('App\Idea');
    }

    public function getSolutionsAttribute($value)
	{
	    return unserialize($value);
	}
}
