import React,{Component} from 'react'
import {connect} from 'react-redux'
import {isEmpty} from 'lodash'
import { HashRouter as Router, Link, Redirect, NavLink, withRouter } from 'react-router-dom'
import { changeStateAdminAsync } from '../redux/asyncActions'
import { bindActionCreators } from 'redux'
import NavbarPerfil from '../components/NavbarPerfil'
import NavbarNotifications from '../components/NavbarNotifications'
import NavbarIdeas from '../components/NavbarIdeas'
import NavbarRequests from '../components/NavbarRequests';
import NavbarSocial from '../components/NavbarSocial';
import NavbarMobile from '../components/NavbarMobile';

class Navbar extends Component {
    state = {
        auth: true,
        ideas: []
    }
    _isMounted = false
    componentDidMount(){
        this._isMounted = true
        setTimeout(() => {
           this._isMounted?  this.setState({ideas: this.props.ideasinnovador}) : null;
        },3000)

       /* $(document).on('click','.close-menu-button', function() {
            $('.menu-mobile, .close-mobile').addClass('active')
        })
        $(document).on('click','.close-mobile', function(){
            $('.menu-mobile, .close-mobile').removeClass('active')
        })*/
    }
    componentDidUpdate(){
        if(this.props.ideasinnovador.length > this.state.ideas.length){
            this.callBubble();
            this._isMounted? this.setState({ideas: this.props.ideasinnovador}) : null;
        }
    }
    openMenu(){
        $('.menu-mobile, .close-mobile').addClass('active')
    }
    callBubble(){
        setTimeout( () => {
        $('.bubble-ideas').css('display','block').removeClass('exit').addClass('enter')
        setTimeout(() => $('.bubble-ideas').removeClass('enter').addClass('exit'), 3000)
        setTimeout(() => $('.bubble-ideas').css('display','none'), 4000)}, 500
        )
    }
    logout = async () => {
        this._isMounted? this.setState({ auth: false }) : null
        
        this.props.exit();
    }

    componentWillUnmount(){
        this._isMounted = false
    }

    render() {
        if (!this.state.auth) {
            return <Redirect to="/login" />
        }
        return(
            <div>
            <div className="container">
            <div className="nav-bar">
                <div className={this.props.type == 'administrator'? 'row h-100 m-0 supervisor' : 'row h-100 m-0'}>
                    <div className="col-md-2 d-flex align-items-center pl-0">
                        <Link className="logo" to="/"> <img className="w-100" src={assets + "/logo.png"} /> </Link>
                    </div>
                    <div className={this.props.type == 'administrator'? "col-md-10 navbar-color navbar-color-sup" : "col-md-10 navbar-color"}>
                        <div className="row h-100">
                            <NavbarSocial />
                             <div className="col-md-9 h-100">
                                <NavbarPerfil
                                    user={this.props.user}
                                    type={this.props.type}
                                    logout={() => this.logout()}
                                    changeStateUser={() => this.props.changeStateFun()}
                                    userstate={!isEmpty(this.props.auth.administrator)? this.props.auth.administrator[0].state : null}
                                />
                                <NavbarNotifications
                                    user={this.props.user}
                                    type={this.props.type}
                                    logout={() => this.logout()}
                                    notifAlert={false}
                                />
                                <div className="navbar-options-container float-right h-100">
                                    <NavbarIdeas ideas={this.props.type == 'administrator' && this.props.auth.administrator[0].state ? 
                                        this.props.ideassupervisor.filter(idea => idea.operator != true) : 
                                        this.props.ideasinnovador.filter(idea => idea.operator != true)} type={this.props.type} />
                                </div>
                                { this.props.type == 'user' &&
                                    <div className="navbar-options-container float-right h-100">
                                        <div className="d-flex align-items-center navbar-options-container-opt float-left h-100 pr-2 pl-2 position-relative">
                                            <Router>
                                                <NavLink className="navbar-options-container-opt-link" exact to="/mis-ideas-ejecutor">
                                                    <div><strong>Ejecutor</strong> <span>({this.props.ideasinnovador.filter(idea => idea.operator == true).length})</span></div>
                                                </NavLink>
                                            </Router>
                                        </div>
                                    </div>
                                }
                                {this.props.type == 'administrator'? 
                                <div className="navbar-options-container float-right h-100">
                                    <NavbarRequests ideas={this.props.type == 'administrator'? this.props.ideassupervisor : this.props.ideasinnovador} type={this.props.type} />
                                </div>
                                 :
                                 null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
                <NavbarMobile/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { userstate, ideasinnovador, ideassupervisor, alertnotification, idea, auth } = state
    return {
        userstate, ideasinnovador, ideassupervisor, alertnotification, idea, auth
    }
}

const mapDispatchToProps = dispatch => {
    return (
        bindActionCreators({
            changeStateAdminAsync
        }, dispatch)
    )
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar))