import React, {Component} from 'react';
import Textbox from '../components/Textbox';
import Button from '../components/Button';
import {connect} from 'react-redux'
import Problem from '../components/Problem';
import { addInnovadorIdeaAsync, editInnovadorIdeaAsync, evaluateInnovadorIdeaAsync, addAllChatsAsync  } from '../redux/asyncActions'
import { Link } from "react-router-dom";
import { Redirect } from 'react-router-dom'
import { bindActionCreators } from 'redux';
import { isEmpty } from 'lodash'

class IdeaForm extends Component{
    constructor(props) {
        super(props);

        this.appendProblem = this.appendProblem.bind(this);
        this.evaluateForm = this.evaluateForm.bind(this);
        this.editForm = this.editForm.bind(this);
        this.deleteProblem = this.deleteProblem.bind(this);
        this.ifIdeaExist = this.ifIdeaExist.bind(this);

        this.state = {
            admin: this.props.admin,
            formState : true,
            redirectToIdea: false,
            formSend : false,
            problems: [0],
            redirect: false,
            ideaID : props.ideaId,
            ideaSelect: '',
            validate: false,
            form: [],
            errors: {},
            disableSubmit: false,
            loading: true,
            ideaState: null,
            problemData: null,
            load: false,
            loadIdea: true
        }

        this.inputProblems = []
        this.inputSolutions = []
    }

    componentDidMount = async () => {
        if (this.state.ideaID) {
            this.setState({ disableSubmit: true, formState: false, loadIdea: false })
            try {
                const resp = this.props.ideas.find((idea) => idea.id == this.state.ideaID)
                this.title.value = resp.title
                this.summary.value = resp.summary
                this.conclusions.value = resp.conclusions

                if (resp.state == 'evaluacion' || resp.state === 'aprobado' || resp.state === 'desaprobado'|| resp.state === 'ejecucion'|| resp.state === 'premiada') {
                    this.setState({ formSend: true })
                }

                let newArr = resp.problems[0].solutions.filter((value) => !isEmpty(value))
                this.setState({problemData: newArr},
                    () => {
                        !isEmpty(newArr) && newArr.map((solution, j) => {
                            return this.inputSolutions[0][j].value = solution
                        })
                    }
                )
                this.inputProblems[0].value = resp.problems[0].problem

                resp.problems.map((data, i) => {
                    if (i > 0) {
                        this.appendProblem(data, i, resp)
                    }
                    //this.inputProblems[i].value = data.problem

                    return null
                })
                this.setState({ loading: false, ideaState: resp.state })
            } catch (err) {
                console.error(err)
            }

            $('.idea-form').find('input').attr('disabled', true);
        } else {
            this.setState({ loading: false })
        }
    }

    ifIdeaExist(id) {
        axios.get(axios_url+'/ideas/'+id+'/show')
            .then((response) =>{
                this.setState({
                    ideaSelect: response,
                    disableSubmit: true
                })
            }).catch((errors) => {
            })
    }

    validateFields = () => {
        let validate = true
        let errors = {}

        if (!this.title.value.length) {
            validate = false
            errors.title = ['Este campo debe ser llenado']
        }
        if (!this.summary.value.length) {
            validate = false
            errors.summary = ['Este campo debe ser llenado']
        }

        errors.problems = this.inputProblems.map(iProblem => {
            if (iProblem) {
                if (!iProblem.value.length) {
                    validate = false
                    return ['Este campo debe ser llenado']
                } else {
                    return null
                }
            }
            return null
        })

        errors.solutions = this.inputSolutions.map(arrSolution => {
            let isArrSolution = arrSolution.filter(iSol => iSol != null)

            const arrSolLength = isArrSolution.length
            let anySol = false

            return isArrSolution.map((iSolution, i) => {
                if (iSolution.value.length) {
                    anySol = true
                }

                if (arrSolLength === i + 1) {
                    if (!anySol) {
                        validate = false
                        return ['Debe ingresar al menos una solución']
                    } else {
                        return null
                    }
                }
                return null
            })
        })

        this.setState({ errors, validate })

        return validate
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        if (this.validateFields()) {
            this.setState({ disableSubmit: true, formState: false })
            $('.idea-form').find('input').attr('disabled', true);
            let problems = [];
            $('input[name^="problem"]').each(function() {
                problems.push($(this).val());
            });
            let solutions = []
            let preSolutions = []
            for (let x = 0; x < $('input[name^="problem"]').length; x++){
                for (let i = 0; i <= 2; i++) {
                    preSolutions.push($('input[name^="solution['+ x +']['+ i +']"]').val());
                }
                solutions.push(preSolutions);
                preSolutions = [];
            }
            let val = {
                'title': this.title.value,
                'summary': this.summary.value,
                'problem': problems,
                'category_id': this.props.category.id,
                'solutions': solutions,
                'conclusions': this.conclusions.value,
                'user': this.props.auth.id
            };
            if (this.state.ideaID) {
                await this.props.editInnovadorIdeaAsync(val, this.state.ideaID)
                this.setState({loadIdea: false})
            } else {
                const { payload } = await this.props.addInnovadorIdeaAsync(val)
                this.setState({ ideaID: payload.id, redirectToIdea: true })
                this.setState({loadIdea: false})
            }
        }
    }

    deleteProblem(id) {
        let arr = this.state.problems;
        let index = arr.indexOf(id);
        this.state.problems.length > 1 ? arr.splice(index, 1) : '';
        this.setState({
            problems : arr
        });
    }

    async appendProblem(data, i, resp) {
        let arr = this.state.problems;
        let val = this.state.problems.length - 1;
        arr.indexOf(val) == -1? '' : val = Math.max.apply(Math, this.state.problems) + 1;
        this.state.problems.length >= 3? '' : arr.push(val);
        await this.setState({
            problems : arr
        }, () => {
            if(!isEmpty(resp)){
            this.inputProblems[i].value = resp.problems[0].problem
            let newArr = data.solutions.filter((value) => !isEmpty(value))
            this.setState({problemData: newArr},
                    () => {
                        !isEmpty(newArr) && newArr.map((solution, j) => {
                            return this.inputSolutions[i][j].value = solution
                        })
                    }
                )
            }
        })
    }

    editForm() {
        $('.idea-form').find('input').attr('disabled', false);

        this.setState({
            formState: true,
            disableSubmit: false
        });
    }

    async evaluateForm() {
        this.setState({load: true})
        await this.props.evaluateInnovadorIdeaAsync(this.state.ideaID)
        await this.props.addAllChatsAsync()
        this.setState({ formSend: true, load: false })
    }

    render() {
        return (
            <div className="col-md-8 form-category-create">
                <div className={this.state.formState? 'idea-form': 'idea-form inactive'}>
                    {this.state.redirectToIdea &&
                        <Redirect to={'/idea/guardado/'+this.state.ideaID} />
                    }
                    <form onSubmit={this.handleSubmit} noValidate>
                        <Textbox
                            name="title"
                            title="Título"
                            refInput={component => this.title = component}
                            errors={this.state.errors.title}
                        />
                        <Textbox
                            name="summary"
                            title="Resumen"
                            refInput={component => this.summary = component}
                            errors={this.state.errors.summary}
                        />

                        {
                            this.state.problems.map(problem =>
                                <Problem
                                    key={problem}
                                    set={problem}
                                    ideaExist={this.state.ideaID}
                                    problemData = {this.state.problemData}
                                    problems={this.inputProblems}
                                    solutions={this.inputSolutions}
                                    errors={this.state.errors}
                                    numberProblems={this.state.problems.length}
                                    event={this.appendProblem}
                                    deleteProblem={() => this.deleteProblem(problem)}
                                />
                            )
                        }

                        <Textbox
                            hiddeme="true"
                            name="conclusions"
                            title="Conclusiones"
                            incluseVal="none"
                            refInput={component => this.conclusions = component}
                            errors={this.state.errors.conclusions}
                        />

                        {(this.state.admin != true || this.props.userstate)?
                            !this.state.loading && this.state.ideaState != 'desaprobado' &&
                                <div>
                                    {this.state.formSend ?
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="float-left">
                                                    <Link to={'/idea/evaluacion/'+this.state.ideaID}>
                                                        <Button
                                                            state="false"
                                                            name="Estado"
                                                            color="red lg"
                                                            type="button"
                                                        />
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                        :
                                        <div className="row button-editar-create-category">
                                            <div className="col-md-5">
                                                <div className="float-left">
                                                    <Button
                                                        type="submit"
                                                        name="Guardar"
                                                        color={!this.state.disableSubmit ? 'black' : 'lightgrey lg no-hover'}
                                                        disabledButton={this.state.disableSubmit}
                                                    />
                                                </div>
                                                <div className="float-left ml-3">
                                                    { this.state.disableSubmit &&
                                                        <Button
                                                            event={this.editForm}
                                                            state="true"
                                                            type="button"
                                                            name="Editar"
                                                            color="red lg"
                                                        />
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-md-7">
                                                <div className="float-right">
                                                    <Button
                                                        state={this.state.loadIdea ? 'false' : 'true'}
                                                        event={this.evaluateForm} name="Solicitar evaluación"
                                                        disabled={this.state.load}
                                                        color={this.state.loadIdea ? 'lightgrey lg no-hover' : 'red lg'}
                                                        type="button"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                            :
                            <div className="row">
                                <div className="col-12">
                                    <div className="float-right">
                                        <Link to={"/idea/evaluacion/"+this.state.ideaID}>
                                            <Button
                                                state={this.state.formState ? 'false' : 'true'}
                                                name="Evaluar idea"
                                                color={this.state.formState ? 'lightgrey lg no-hover' : 'red lg'}
                                                type="button"
                                            />
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        }
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { auth } = state
    const  ideas  = state.auth.administrator.length > 0 && state.auth.administrator[0].state ? state.ideassupervisor : state.ideasinnovador
    const userstate = state.auth.administrator.length > 0 && state.auth.administrator[0].state ? true : false;
    return { auth, userstate, ideas }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        addInnovadorIdeaAsync,
        editInnovadorIdeaAsync,
        evaluateInnovadorIdeaAsync,
        addAllChatsAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(IdeaForm)
