import React from 'react';
import FontAwsome from 'react-fontawesome';
import { withRouter, HashRouter as Router, Route,Switch, Link } from 'react-router-dom';
import LoaderHOC from '../HOC/LoaderHOC'

const IdeaProgress = ({stage, ideaId, ideastate, history}) => {

    /* Las 4 variables son las que indican el estado de la idea, estas variables se replican en todo el siguiente codigo */
    const guardado = "/idea/guardado/" + ideaId.id
    const evaluacion = "/idea/evaluacion/"+  + ideaId.id
    const ejecucion = "/idea/ejecucion/" + ideaId.id
    const premiacion = "/idea/premiada/" + ideaId.id

    return( 
        <div className="idea-progress mb-4 pb-4">
            <div onClick={() => history.goBack()} className="idea-progress-return mb-1">
                <FontAwsome name="chevron-left" /> Regresar
            </div>
            <div className="idea-progress-container-im">
                <div className="idea-progress-image d-flex align-items-center justify-content-center">
                    <svg className={"svg-idea-state stage" + stage.index} id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 627.38 64.46">
                            {/* Estado premiacion */}
                                <g className={ideastate >= 6 ? "" : "svg-no-click"}>
                                    <rect className="rect" x="362.26" y="18.69" width="166.1" height="3.18"/>
                                    <Link to={premiacion}><text className="svg-text-word svg-text-word-4" transform="translate(492.96 58.71)">PREMIACIÓN</text></Link>
                                    <Link to={premiacion}><circle className="circle circle-4" cx="533.33" cy="19.66" r="19.66"/></Link>
                                    <Link to={premiacion}><text className="svg-text svg-text-2" transform="translate(528.95 25.66)">4</text></Link>
                                </g>
                            

                            {/* Estado ejecucion */}
                                <g className={ideastate >= 4 || ideastate >= 5 ? "" : "svg-no-click"}>
                                    <rect className="rect" x="221.43" y="19.18" width="142.47" height="2.68"/>
                                    <Link to={ejecucion}><text className="svg-text-word svg-text-word-3" transform="translate(336.68 58.71)">EJECUCIÓN</text></Link>
                                    <Link to={ejecucion}><circle className="circle circle-3" cx="371.13" cy="19.66" r="19.66"/></Link>
                                    <Link to={ejecucion}><text className="svg-text svg-text-3" transform="translate(366.75 25.66)">3</text></Link>
                                </g>

                            {/* Estado evaluacion */}
                                <g className={ideastate >= 1 || ideastate >= 2 || ideastate >= 3? "" : "svg-no-click"}>
                                    <rect className="rect" x="53.43" y="19.18" width="144.47" height="2.68"/>
                                    <Link to={evaluacion}><text className="svg-text-word svg-text-word-2" transform="translate(169.24 58.71)">EVALUACIÓN</text></Link>
                                    <Link to={evaluacion}><circle className="circle circle-2" cx="210.17" cy="19.66" r="19.66"/></Link>
                                    <Link to={evaluacion}><text className="svg-text svg-text-4" transform="translate(205.78 25.66)">2</text></Link>
                                </g>
                            

                            {/* Estado guardado */}
                                <g className={ideastate >= 1? "" : "svg-no-click"}>
                                    <Link to={guardado}><text className="svg-text-word svg-text-word-1" transform="translate(34 58.71)">IDEA</text></Link>
                                    <Link to={guardado}><circle className="circle circle-1" cx="47.97" cy="19.66" r="19.66"/></Link>
                                    <Link to={guardado}><text className="svg-text svg-text-1" transform="translate(43.06 25.66)">1</text></Link>
                                </g>
                    </svg>
                </div>
            </div>
        </div>
    )}
export default withRouter( IdeaProgress )