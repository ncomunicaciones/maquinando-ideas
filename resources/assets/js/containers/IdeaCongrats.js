import React, {Component} from 'react'
import { formatDate } from '../utils'

export default class IdeaCongrats extends Component {
    state = {
        idea: this.props.ideaselect
    }

    render() {
        return this.state.idea && (
            <div className="col-md-8">
                <div className="premiacion">
                    <div className="clearfix">
                        <div className="float-right clearfix">
                            <div className="evaluate-idea-container">Idea enviada el {formatDate(this.state.idea.idea_execute[0].end_day)}</div>
                        </div>
                    </div> 
                    <div className="premiacion-img mt-4">
                        {this.state.idea.usercreatorall.gender == 'm'?
                            <img src={assets + "/award.png"} />
                            :
                            <img src={assets + "/award1.png"} />
                        }
                        
                        <div className="title">Premio al Innovador</div>
                        <span>{this.state.idea.usercreator} <br/> {formatDate(this.state.idea.idea_execute[0].end_day)}</span>
                    </div>
                    <div className="clearfix mt-3 mb-3">
                        <div className=" float-right premiacion-supervisor-container">
                            <div>Administrador(a)</div>
                            <div>{this.state.idea.administratorcreator.name}</div>
                        </div>
                    </div>
                    <div className="subtitle">
                        Agradecemos al lider innovador {this.state.idea.usercreator} y los miembros del equipo que han logrado poner en marcha “{ this.state.idea.title}”
                    </div>
                    <div className="premiacion-idea-description mt-2 mb-4">
                        Es muy importante que podamos hacerle seguimiento a la idea.
                    </div>
                </div>
            </div>
        )
    }
}