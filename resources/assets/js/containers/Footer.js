import React, {Component} from 'react';

export default class Footer extends Component{
    render(){
        return(
            <div className="all-width clearfix mt-3">
                <div className="row mt-5">
                    <div className="col-md-6">
                        <img className="image-down" src={assets + "/logo-down.png"} />

                    </div>
                    <div className="col-md-6 align-text-to-right">
                        <img className="image-down-2" src={assets + "/unimaq-logo2.png"} />
                    </div>
                </div>
            </div>
        )
    }
}
