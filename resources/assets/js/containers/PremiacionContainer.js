import React from 'react'
import Button from '../components/Button'
import { formatDate } from '../utils'
import { setContentText } from '../utils'
import { connect } from 'react-redux'

function setTextAwardUser(ideaTitle, ideaUser) {
    let textAward = setContentText(this.props.contenttext.contentTexts, `title-award-collaborator`)
    textAward.replace('{innovador}', ideaUser)
    textAward.replace('{idea}', ideaTitle)
}

const PremiacionContainer = ({ user, idea, admin, finishDate }) => 
    <div className="premiacion">
        <div className="clearfix">
            <div className="float-right clearfix">
                <div className="evaluate-idea-container">Idea enviada el {formatDate(idea.created_at)}</div>
            </div>
        </div> 
        <div className="premiacion-img mt-4">
            <img src={assets + "/award.png"} />
            <div className="title">{ setContentText(this.props.contenttext.contentTexts, `title-award-collaborator`) }</div>
            <span>{user.name} <br/> {formatDate(finishDate)}</span>
        </div>
        <div className="clearfix mt-3 mb-3">
            <div className=" float-right premiacion-supervisor-container">
                <div>Administrador(a)</div>
                <div>{ admin.user.name }</div>
            </div>
        </div>
        <div className="subtitle">
            { setTextAwardUser(idea.title, user.name) }
        </div>
        <div className="premiacion-idea-description mt-2 mb-4">
            Es muy importante que podamos hacerle seguimiento a la idea.
        </div>
        <div>
            <div className="float-right">
                <Button color="yellow" name="Enviar correo" />
            </div>
            <div className="float-right mr-2">
                <Button color="black" name="Seguimiento del proyecto" />
            </div>
        </div>
    </div>

const mapStateToProps = state => state

export default connect(mapStateToProps, null)(PremiacionContainer)
