import React from 'react'
import { formatDate } from './../utils'
import { Link } from 'react-router-dom'
import { getTimeElapsedFromDate, getTimeElapsedFromDate2 } from '../utils'
import BarIdea from '../components/BarIdea'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'

function checkIdeaState(stateText, ideaState) {
    if (stateText == 'evaluación') {
        return ideaState == 'evaluacion'
    } else if (stateText == 'ejecución') {
        return ideaState == 'ejecucion'
    }
    return false
}

class EvaluateProgress extends React.Component{
    state = {
        idea: this.props.idea,
        time: this.props.type == 'evaluate'? this.props.idea.idea_evaluate[0].evaluate_time : this.props.idea.idea_execute[0].execute_time,
        load: true
    }
    componentDidUpdate(){
        const validate = this.props.ideas.find((idea) => idea.id == this.state.idea.id);
        if(this.props.type == 'evaluate'){
            if(!isEmpty(validate)){
                if(this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_evaluate[0].evaluate_time != this.state.time){
                    this.setState({idea: this.props.ideas.find((idea) => idea.id == this.state.idea.id), load: false, time: this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_evaluate[0].evaluate_time}, () => this.setState({load: true}))
                }
            }
        }else{
            if(!isEmpty(validate)){
                if(this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_execute[0].execute_time != this.state.time){
                    this.setState({idea: this.props.ideas.find((idea) => idea.id == this.state.idea.id), load: false, time: this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_execute[0].execute_time}, () => this.setState({load: true}))
                }
            }
        }
        
        
    }
    render(){
        return this.state.load && (
            <div>
            <div>
                <div className="clearfix">
                    { this.props.isAdmin && checkIdeaState(this.props.stateText, this.props.ideaState) ?
                        <div className="float-left clearfix">
                            <Link to={`/cambiar-tiempo/${this.props.ideaId}`} className="evaluate-idea-container change-date">Cambiar tiempo</Link>
                        </div> : null
                    }
                    <div className="float-right clearfix">
                        <div className="evaluate-idea-container">Idea enviada el {formatDate(this.props.day)}</div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="evaluate-progress-container col-md-12">
                    <div className="mt-3 mb-1">
                        Progreso de { this.props.stateText } <span>transcurridos <i style={{ width: '3px', display: 'inline-block'}}></i>
                         { this.props.type == 'evaluate'?
                            isEmpty(this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_evaluate[0].time_elapsed) ?
                                getTimeElapsedFromDate(this.props.startDate)
                                :
                                this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_evaluate[0].time_elapsed
                            :
                            isEmpty(this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_execute[0].time_elapsed) ?
                                getTimeElapsedFromDate(this.props.startDate)
                                :
                                this.props.ideas.find((idea) => idea.id == this.state.idea.id).idea_execute[0].time_elapsed
                        } de { this.state.time }</span>
                    </div>
                    <BarIdea alltime={ this.state.time } time={ getTimeElapsedFromDate2(this.props.startDate) } />
                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = state => {
    const ideas = state.auth.administrator.length > 0 && state.auth.administrator[0].state ? state.ideassupervisor : state.ideasinnovador
    return { ideas }
}

export default connect(mapStateToProps, null)(EvaluateProgress)
