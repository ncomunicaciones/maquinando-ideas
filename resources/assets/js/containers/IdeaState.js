import React, { Component } from 'react'
import { setContentText } from '../utils'
import {isEmpty} from 'lodash'
import { connect } from 'react-redux'

class IdeaState extends Component {
    userTextSlugs = []
    typeUser = (!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state)? 'admin' : 'user'

    render() {
        return (
            <div className="ideastate-container mt-4 mb-5 pl-5 pr-5">
                <div className="ideastate-container-1 mr-5 h-100">
                    { this.props.type == "aprobado" ? <img src={assets + "/idea-aprobada.png"} /> : '' }
                    { this.props.type == "desaprobado" ? <img src={assets + "/idea-desaprobada.png"} /> : '' }
                    { this.props.type == "ejecucion" ? <img src={assets + "/idea-ejecucion.png"} /> : '' }
                </div>
                <div className="ideastate-container-2 h-100">
                    <div className="h-100">
                        { this.props.type == "aprobado" ? 
                            <div>
                           
                                <div className="subtitle mb-3">{ setContentText(this.props.contenttext, `title-${this.typeUser}-idea-approved`) }</div>
                                <p>{ setContentText(this.props.contenttext, `text-${this.typeUser}-idea-approved`) }</p>
                            </div>
                            :
                            ''
                        }
                        { this.props.type == "desaprobado" ? 
                            <div>
                                <div className="subtitle mb-3">{ setContentText(this.props.contenttext, `title-${this.typeUser}-idea-rejected`) }</div>
                                <p>{ setContentText(this.props.contenttext, `text-${this.typeUser}-idea-rejected`) }</p>
                            </div>
                            :
                            ''
                        }
                        { this.props.type == "ejecucion" ? 
                            <div>
                                <div className="subtitle mb-3">{ setContentText(this.props.contenttext, `title-${this.typeUser}-idea-execute`) }</div>
                                <p>{ setContentText(this.props.contenttext, `text-${this.typeUser}-idea-execute`) }</p>
                            </div>
                            :
                            ''
                        }
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { auth } = state
    const { contenttext } = state
    return { auth, contenttext } 
}

export default connect(mapStateToProps, null)(IdeaState)