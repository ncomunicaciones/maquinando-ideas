import React, {Component} from 'react'
import Button from '../components/Button'
import EvaluateProgress from '../containers/EvaluateProgress'
import {Link} from 'react-router-dom'
import IdeaState from './../containers/IdeaState'
import ChatBox from './../containers/ChatBox'
import { isEmpty } from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {passEvaluateInnovadorIdeaAsync } from '../redux/asyncActions'

class IdeaEvaluate extends Component{
    state = {
        category: null,
        idea: this.props.ideaselect? this.props.ideaselect : this.props.ideas.find((idea) => idea.id == this.props.match.params.id) ,
        ideaEvaluate: true,
        messages: [],
        loading: true,
        loadSend: false,
        send: false,
        sendAnswer: false,
        AnswerData: '',
        finish: this.props.ideaselect.state == 'evaluacion'? false : true,
        interval: '',
    }

    componentDidMount(){
        this.getMessages()
    }

    getMessages(){
        /*axios.get(axios_url+'/getchat/'+this.state.idea.id+'/evaluate')
            .then((response) => 
                {this.setState({messages: response.data.reverse()}); this.setState({loading: true})}
            )*/
    }

    

    componentDidUpdate(){
        if(this.state.idea.state != this.props.ideaselect.state){
                this.setState({idea: this.props.ideaselect, finish: this.props.ideaselect.state == 'evaluacion'? false : true})
        }
    }

    handleSubmit = async () => {
        if (this.sustain.value.length > 0) {
            this.setState({send: true})
            const data = {
                sustain: this.sustain.value,
                state: this.state.AnswerData
            }
            await this.props.passEvaluateInnovadorIdeaAsync(data, this.state.idea.id)
            this.setState({sendAnswer: false })
            this.setState({send: true})
        } else {
            this.setState({ sustainRequired: true })
        }
    }

    resolveIdeaEvaluate = ideaEvaluate => ideaEvaluate ? ideaEvaluate[0] : {}
    changeStateIdea = (answer) => this.setState({ sendAnswer: true, AnswerData: answer })

    render() {
        return this.state.idea && (
            <div className="col-md-8">
                <EvaluateProgress
                    day={this.state.idea.created_at}
                    isAdmin={this.props.userstate}
                    ideaId={this.state.idea.id}
                    stateText='evaluación'
                    idea={this.state.idea}
                    ideaState={this.state.idea.state}
                    startDate={this.state.idea.idea_evaluate[0].created_at}
                    type='evaluate'
                />
                { this.state.idea.state == 'aprobado' || this.state.idea.state == 'ejecucion' || this.state.idea.state == 'premiada' ? <IdeaState typeUser="user" type="aprobado" /> : '' }
                { this.state.idea.state == 'desaprobado'? <IdeaState type="desaprobado" /> : '' }
                {!isEmpty(this.props.chatsrooms) &&
                    <ChatBox
                        finish={this.state.finish}
                        admin={true}
                        type='evaluate'
                        idea={this.state.idea}
                        messages={this.state.messages.slice(-5)}
                        loading={this.state.loading}
                        refInputChat={(e) => this.messageInput = e}
                    />
                }
                {this.state.idea.state != 'evaluacion' && this.state.idea.idea_evaluate[0].sustain != 'isEmpty' &&
                    <div>
                        <div className="subtitle mb-2">Sustento</div>
                        {this.state.idea.idea_evaluate[0].sustain}
                    </div>
                }
                {(this.props.admin == true && this.props.userstate)?
                    <div className="clearfix mt-5">
                        { !this.resolveIdeaEvaluate(this.state.idea.idea_evaluate).sustain &&
                            <div className="mt-5 clearfix">
                                <div className="float-left">
                                    <div className="float-left mr-2">
                                        <Button
                                            state="true"
                                            event={() => this.changeStateIdea('aprobado')}
                                            color="red" name="aprobar"
                                        />
                                    </div>
                                    <div className="float-left">
                                        <Button
                                            state="true"
                                            event={() => this.changeStateIdea('desaprobado')}
                                            color="black"
                                            name="desaprobar"
                                        />
                                    </div>
                                </div>
                            </div>
                        }
                        { this.resolveIdeaEvaluate(this.state.idea.idea_evaluate).sustain && this.state.idea.state != 'desaprobado' &&
                            <div className="float-right">
                            <Link to={'/idea/ejecucion/'+this.state.idea.id}>
                                    <Button
                                        name="Continuar con ejecución"
                                        color="red"
                                        type="button"
                                    />
                                </Link>
                            </div>
                        }
                        { this.state.sendAnswer &&
                            <form>
                                <div className="clearfix mt-4">
                                    <div className="form-group mt-3">
                                        <label className="evaluacion-sustento"><span> {this.state.AnswerData} </span>- Sustentar evaluación</label>
                                        <textarea
                                            ref={component => this.sustain = component}
                                            className="form-control"
                                            id="exampleFormControlTextarea1"
                                            rows="3"
                                        >
                                        </textarea>
                                    </div>
                                </div>
                                { this.state.sustainRequired && <p className="invalid-input">Debe ingresar el sustento de su evaluación</p> }
                                    <div className="clearfix">
                                    <div className="float-right">
                                        <div className="float-left mr-2">
                                            <Button
                                                type="button"
                                                state="true"
                                                event={() => this.handleSubmit()}
                                                color="red"
                                                name="Enviar"
                                                disabled={this.state.send}
                                            />
                                        </div>
                                        <div className="float-left">
                                            <Button
                                                type="button"
                                                state="true"
                                                event={() => this.setState({ sendAnswer: false })}
                                                color="black"
                                                name="Cancelar"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        }
                    </div>
                    :
                    <div className="row">
                        <div className="col-12">
                            <div className="float-right">
                            {this.state.idea.state == 'ejecucion' &&
                                <div>
                                    <Link to={'/idea/ejecucion/'+this.state.idea.id}>
                                        <Button
                                            name="Continuar a la ejecución"
                                            color="red"
                                            type="button"
                                        />
                                    </Link>
                                </div>
                            }
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    const ideas = state.auth.administrator.length > 0 && state.auth.administrator[0].state ? state.ideassupervisor : state.ideasinnovador
    const userstate = !isEmpty(state.auth.administrator)? state.auth.administrator[0].state == 1?  1: 0 : 0
    const {chatsrooms} = state
    return { ideas, userstate, chatsrooms }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        passEvaluateInnovadorIdeaAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(IdeaEvaluate)