import React, {Component} from 'react'
import Chat from '../components/Chat'
import Textbox from '../components/Textbox'
import FontAwesome from 'react-fontawesome'
import Button from '../components/Button'
import { sendChatAsync } from '../redux/asyncActions'
import { connect } from 'react-redux'
import {isEmpty, isEqual, isNull} from 'lodash'
import { bindActionCreators } from 'redux';

class ChatBox extends Component{
    state = {
        chatsrooms: null,
        load: false,
        type: this.props.type,
        loading: false
    }
    componentDidMount(){
        const chatroom =  this.props.chatsrooms.find((chat) => chat.idea == this.props.idea.id && chat.type == this.props.type);
        !isEmpty(chatroom)? this.setState({chatsrooms: chatroom, load: true}) : null
    }
    componentDidUpdate(){
        const chatSelect = this.props.chatsrooms.find((chat) => chat.idea == this.props.idea.id && chat.type == this.props.type)
        if(!isEmpty(chatSelect)){
            if((isEmpty(chatSelect.chat) && isNull(this.state.chatsrooms))){
                this.setState({chatsrooms: chatSelect, load: !isEmpty(chatSelect)? true : false})
            }else if(!isEmpty(chatSelect.chat) && !isEqual(chatSelect.chat.sort(), this.state.chatsrooms.chat.sort())){
                this.setState({chatsrooms: chatSelect, load: !isEmpty(chatSelect)? true : false})
            }
            if(this.props.type != this.state.type){
                this.setState({chatsrooms: chatSelect, load: !isEmpty(chatSelect)? true : false})
            }
        }
    }
    async sendMessage(e){
        e.preventDefault();
        this.setState({loading: true})
        const message = this.messageInput
        const form = {message: message.value}
        await this.props.sendChatAsync(this.state.chatsrooms.id, form, this.props.type)
        this.messageInput.value = '';
        this.setState({loading: false})
    }
    render(){
        return this.state.load && !isEmpty(this.state.chatsrooms) && (
            <div className="mt-4">
            <div className="chatBox-title mb-4">
                Estados de Evaluación
            </div>
            {!isEmpty(this.state.chatsrooms) ? <Chat messages={(this.state.chatsrooms.chat.slice(-5))} users={this.props.users} idea={this.props.idea} type={this.state.type} /> : null}            
            {!this.props.finish &&
            <div className="mt-5 chatBox-message clearfix">
                <form onSubmit={(e) => this.sendMessage(e)}>
                    <div className="chatBox-message-part1">
                        <Textbox
                            name="message"
                            title="Mensaje"
                            refInput={(e) => this.messageInput = e}
                        />
                    </div>
                    <button className="chatBox-message-part2 ">
                        <div className=" d-flex align-items-baseline h-100 justify-content-center clearfix">
                            {!this.state.loading?
                                <div>
                                    <FontAwesome size="3x" name="paper-plane" />
                                </div>:
                                <div>
                                    <i className="fa fa-circle-o-notch spinner-animation" aria-hidden="true"></i>
                                </div>
                            }
                        </div>
                    </button>
                </form>
            </div>
            }
        </div>
        )
    }
}

const mapStateToProps = state => {
    const {chatsrooms} = state
    const users = state.auth.allUsers
    return { chatsrooms, users }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        sendChatAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatBox)
