import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'
import {Link, Redirect} from 'react-router-dom'
import { connect } from 'react-redux'

class MenuMobile extends Component{

    state = {
        gender : this.props.auth.gender,
        auth: true,
        validate : true,
        loading: false
    }
    componentDidUpdate(){
        if(this.props.auth.gender != this.state.gender){
            this.setState({validate: false, gender: this.props.user.gender}, () => this.setState({validate: true}))
        }
    }
    logout = async () => {
        const resp = await axios.post(axios_url+'/user/logout')
        this.setState({ auth: false })
    }
    closemenu(){
        $('.menu-mobile, .close-mobile').removeClass('active')
    }

    openSubmenu(e){
        $(e.target).toggleClass('active')
        $(e.target).siblings('.menu-mobile-sub-options').stop(true,true).slideToggle();
    }
    
    render(){
        if (!this.state.auth){
            return <Redirect to="/login" />
        }
        return(
            <div className="menu-mobile">
                <div className="menu-mobile-user">
                    <div className="menu-mobile-user-image">
                        <div className="perfil-image">
                            {this.state.validate &&
                                <div>   
                                    {this.state.gender == 'm'?
                                        <img src={assets+'/avatars/'+this.props.auth.avatar} data='M' onError={(e) => e.target.src = assets + '/avatar.png'} /> 
                                        : 
                                        <img src={assets+'/avatars/'+this.props.auth.avatar} data='F' onError={(e) => e.target.src = assets + '/avatar-f.png'} />
                                    }
                                </div>
                            }
                        </div>
                    </div>
                    <div className="menu-mobile-user-type">innovador</div>
                    <div className="menu-mobile-user-name">{this.props.user.name}</div>
                    <div className="menu-mobile-user-container-notification">
                        <div className="menu-mobile-user-notification">
                            <Link to="/notificaciones" onClick={this.closemenu}>
                                <FontAwesome name="bell" />
                            </Link>
                        </div>
                    </div>
                    
                </div>
                <div className="menu-mobile-options">
                    <Link to="/mis-ideas"><div onClick={this.closemenu} className="menu-mobile-option">Mis ideas</div></Link>
                    <Link to="/mis-ideas-ejecutor"><div onClick={this.closemenu} className="menu-mobile-option">Ejecutor</div></Link>
                    <div className="menu-mobile-option" onClick={(e) => this.openSubmenu(e)}>Configuración <div className="float-right"><FontAwesome name="chevron-down" /></div></div>
                    <div className="menu-mobile-sub-options">
                        <Link onClick={this.closemenu} to="/perfil"><div className="menu-mobile-sub-option">EDITAR PERFIL</div></Link>
                        <div onClick={this.closemenu} onClick={this.logout} className="menu-mobile-sub-option">SALIR</div>
                    </div>
                </div>
                <div className="navbar-redes">
                    <div className="navbar-redes-box">
                        <span aria-hidden="true" className="fa fa-envelope"></span>
                    </div>
                    <div className="navbar-redes-box">
                        <span aria-hidden="true" className="fa fa-phone"></span> &nbsp; (01) 202-1300
                    </div>
                    <div className="navbar-redes-box">
                    <span aria-hidden="true" className="fa fa-facebook"></span>
                    </div>
                    <div className="navbar-redes-box">
                        <span aria-hidden="true" className="fa fa-twitter"></span>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { auth } = state
    return {auth}
}

export default connect(mapStateToProps, null)(MenuMobile)