import React, {Component} from 'react'
import EvaluateProgress from '../containers/EvaluateProgress'
import RadioTime from './../components/RadioTime'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import fuzzysearch from 'fuzzysearch'
import IdeaState from './../containers/IdeaState'
import {connect} from 'react-redux'
import Button from '../components/Button'
import { saveOperatorsAsync, passInnovadorIdeaToExecuteAsync, setExecuteTimeAsync, finishIdeaExecuteAsync, addAllChatsSupAsync } from '../redux/asyncActions'
import ChatBox from './../containers/ChatBox'
import { isEmpty, isNaN, isNull} from 'lodash'

class IdeaExecute extends Component{
    state = {
        admin: this.props.admin,
        category: null,
        seeDay : false,
        time: false,
        selectedTime: null,
        finish: false,
        filterUsers: [],
        filterUsers1: [],
        filterUsers2: [],
        filterUsers3: [],
        ejecutorBoss: null,
        ejecutor1: null,
        ejecutor2: null,
        ejecutor3: null,
        ejecutores: '',
        idea: {},
        ideaEvaluate: true,
        loading: true,
        ideaId: this.props.ideaselect,
        messages: [],
        interval: '',
    }

    timeOptions = [30, 60, 90, 120, 150, 180, 210, 240]
    nOperators = 3

    async componentDidMount(){
        this.setState({idea: this.props.ideaselect, finish: this.props.ideaselect.state == 'ejecucion'? false : true})
        try {
            this.setState({
                time: this.state.ideaId.idea_execute[0].execute_time,
                ejecutores: this.state.ideaId.idea_execute[0].operator_boss_user_id,
                loadingPage: false
            })

        } catch (err) {
            console.error(err)
        }

    }

    validateClassSelected = time => {
       return this.state.selectedTime == time ? 'selected' : ''
    }


    setTimeOption = ({ target }) => {
        document.getElementById('messageTime').textContent = ''
        this.setState({ selectedTime: target.value })
    }

    onFocus = i => {
        setTimeout(() => {
            const autocompleteBox = document.getElementById(`autocomplete-${i}`)
            if (autocompleteBox) {
                autocompleteBox.style.visibility = 'visible'
            }
        }, 100)
    }

    sendOperators = async () => {
        const { msg, resu } = this.validateOperators()
        const $message = document.getElementById('messageOperators')
        $message.className = 'text-info'
        $message.textContent = 'Guardando...'

        if (resu) {
            try {
                const { ejecutorBoss: operator_boss, ejecutor1, ejecutor2, ejecutor3 } = this.state
                var operators = {
                    operator_boss,
                    operators: []
                }
                var operators1 = [ejecutor1, ejecutor2, ejecutor3]
                var opp2 = operators1.filter((operator) => !isNull(operator))
                operators.operators = opp2

                await this.props.saveOperatorsAsync(operators, this.state.ideaId.idea_execute[0].id)
                await this.props.passInnovadorIdeaToExecuteAsync({}, this.state.ideaId.id)
                await this.props.addAllChatsSupAsync()
                this.setState({ ejecutores: operator_boss })

            } catch (err) {
                console.error(err)
                $message.className = 'text-danger'
                $message.textContent = 'Servicio temporalmente no disponible'
            }
        } else {
            $message.className = 'text-danger'
            $message.textContent = msg
        }
    }

    validateOperators = () => {
        const { ejecutorBoss, ejecutor1, ejecutor2, ejecutor3 } = this.state
        const ejecutores = [ejecutorBoss, ejecutor1, ejecutor2, ejecutor3]
        let inputs = [document.getElementById('inputjefe').value,
                      document.getElementById('inputoperador1').value,
                      document.getElementById('inputoperador2').value,
                      document.getElementById('inputoperador3').value]

        if(isNull(ejecutorBoss) && !isEmpty(inputs[0])){
            return { msg: 'El nombre del jefe ejecutor no es valido.', resu: false }
        }
        if(isNull(ejecutor1) && !isEmpty(inputs[1])){
            return { msg: 'El nombre del primer ejecutor no es valido. Si solo desea mantener un ejecutor, deje los demas campos vacios.', resu: false }
        }
        if(isNull(ejecutor2) && !isEmpty(inputs[2])){
            return { msg: 'El nombre del segundo ejecutor no es valido. Si solo desea mantener un ejecutor, deje los demas campos vacios', resu: false }
        }
        if(isNull(ejecutor3) && !isEmpty(inputs[3])){
            return { msg: 'El nombre del tercer ejecutor no es valido. Si solo desea mantener un ejecutor, deje los demas campos vacios', resu: false }
        }

        const validateEjecutores = ejecutores.filter(
            (ejecutor) => !isEmpty(ejecutor)
        )
        if (!ejecutorBoss) {
            return { msg: 'Debe ingresar todos los ejecutores', resu: false }
        }

        if (new Set(validateEjecutores).size !== validateEjecutores.length) {
            return { msg: 'Cada ejecutor debe ser diferente', resu: false }
        }

        return { msg: 'Ok', resu: true }
    }

    filter = (filterName) => {
        document.getElementById('messageOperators').textContent = ''
        let getVal = this[`users${filterName ? filterName : ''}`].value.toLowerCase()
        let inputs = [document.getElementById('inputjefe').value,
                      document.getElementById('inputoperador1').value,
                      document.getElementById('inputoperador2').value,
                      document.getElementById('inputoperador3').value]

        if (getVal.length > 2) {
            const filterUsers = this.props.users.filter((user) => user.id != this.props.user.id && user.id != this.state.idea.user_id && user.state && inputs.indexOf(user.name) == '-1').filter(user => {
                return (
                    this[`users${filterName ? filterName : ''}`].value.length > 0 ?
                    fuzzysearch(getVal, user.name.toLowerCase(), {caseSensitive: true, sort: true}) : null
                )
            })

            this.setState({
                [`filterUsers${filterName ? filterName : ''}`]: filterUsers,
                [`ejecutor${filterName ? filterName : 'Boss'}`]: null
            })

        } else {
            this.setState({
                [`filterUsers${filterName ? filterName : ''}`]: [],
                [`ejecutor${filterName ? filterName : 'Boss'}`]: null
            })
        }
    }

    setVal = (e, i, userId) => {
        this[`users${i ? i : ''}`].value = e.currentTarget.innerText
        this.setState({
            [`ejecutor${i ? i : 'Boss'}`]: userId,
            [`filterUsers${i ? i : ''}`]: []
        })
    }

    onBlur = i => {
        setTimeout(() => {
            const autocompleteBox = document.getElementById(`autocomplete-${i}`)
            if (autocompleteBox) {
                autocompleteBox.style.visibility = 'hidden'
            }
        }, 200)
    }

    sendFormTime = async () => {
        const { selectedTime: time } = this.state
        const $message = document.getElementById('messageTime')
        $message.className = 'text-info'
        $message.textContent = 'Guardando...'
        const res = {}
        res.time = time;

        if (time) {
            await this.props.setExecuteTimeAsync(res, this.state.ideaId.idea_execute[0].id)
            this.setState({time: res.time})
        } else {
            $message.className = 'text-danger'
            $message.textContent = 'Debe ingresar el tiempo de ejecución'
        }
    }

    finishIdeaExecute = async () => {
        const $message = document.getElementById('message-execute')
        $message.className = 'text-info'
        $message.textContent = 'Espere...'

        await this.props.finishIdeaExecuteAsync(this.state.ideaId.id)
    }

    componentDidUpdate(){
        if(this.state.idea.state != this.props.ideaselect.state){
            this.setState({idea: this.props.ideaselect, finish: this.props.ideaselect.state == 'ejecucion'? false : true})
        }
    }

    render() {
        return this.state.idea && this.state.ideaEvaluate && (
            <div className="col-md-8">
                { !this.state.time &&
                    <div className="mt-5">
                        <div className='problem-container-header mb-3'>
                            ¿Cuanto tiempo de ejecución tendrá esta idea?
                        </div>
                        <div className="clearfix">
                            {
                                this.timeOptions.map(time =>
                                    <RadioTime
                                        key={`time-${time}`}
                                        time={time}
                                        setTimeOption={this.setTimeOption}
                                        isSelectedClass={`form-check-label square ${this.validateClassSelected(time)}`}
                                    />
                                )
                            }
                        </div>
                        <p id="messageTime"></p>
                        <div className="clearfix mt-3">
                            <div className="float-left mr-2">
                                <Button
                                    event={this.sendFormTime}
                                    state="true"
                                    color="red"
                                    name="Aceptar"
                                />
                            </div>
                            <div className="float-left">
                                <Button
                                    state="true"
                                    color="black"
                                    name="Cancelar"
                                />
                            </div>
                        </div>
                    </div>
                }




                { !this.state.ejecutores && this.state.time &&
                    <div>
                        <div className='problem-container-header mb-3'>
                            jefe ejecutor
                        </div>
                        <div className="form-group">
                            <div className="form-setStatic">
                                <input
                                    onChange={() => this.filter(0)}
                                    onFocus={() => this.onFocus(0)}
                                    onBlur={() => this.onBlur(0)}
                                    ref={(e) => this.users = e}
                                    type="text"
                                    className="form-control"
                                    aria-describedby="message"
                                    placeholder="Nombre"
                                    id="inputjefe"
                                />

                                <div className="autocomplete" id="autocomplete-0">
                                    {
                                        this.state.filterUsers.map(
                                            (user, i) =>
                                                <div
                                                    className="autocomplete-item"
                                                    onClick={(e) => this.setVal(e, 0, user.id)}
                                                    key={i}
                                                >
                                                    {user.name}
                                                </div>
                                        )
                                    }
                                </div>
                            </div>
                        </div>

                        <div className='problem-container-header mb-3'>
                            ejecutores
                        </div>

                        { [...Array(this.nOperators)].map((e, i) =>

                            <div className="form-group" key={`ejecutor-${i}`}>
                                <div className="form-setStatic">
                                    <input
                                        onChange={() => this.filter(i+1)}
                                        onFocus={() => this.onFocus(i+1)}
                                        onBlur={() => this.onBlur(i+1)}
                                        ref={(e) => this[`users${i+1}`] = e}
                                        type="text"
                                        className="form-control"
                                        aria-describedby={`message-${i+1}`}
                                        placeholder="Nombre"
                                        id={`inputoperador${i+1}`}
                                    />

                                    <div className="autocomplete" id={`autocomplete-${i+1}`}>
                                        {
                                            this.state[`filterUsers${i+1}`].map(
                                                (user, j) =>
                                                    <div
                                                        className="autocomplete-item"
                                                        onClick={(e) => this.setVal(e, i+1, user.id)}
                                                        key={j}
                                                    >
                                                        {user.name}
                                                    </div>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                        )}

                        <p id="messageOperators"></p>

                        <div className="clearfix mt-5">
                            <div className="float-left mr-2">
                                <Button
                                    event={this.sendOperators}
                                    state={this.state.ejecutorBoss ? 'true' : 'false'}
                                    color={this.state.ejecutorBoss ? 'red' : 'black'}
                                    name="Aceptar"
                                />
                            </div>
                        </div>
                    </div>
                }
                { this.state.ejecutores &&
                    <div>
                    <EvaluateProgress
                        day={this.state.idea.created_at}
                        isAdmin={this.props.userstate}
                        ideaId={this.state.idea.id}
                        stateText='ejecución'
                        idea={this.state.idea}
                        ideaState={this.state.idea.state}
                        startDate={this.state.idea.idea_execute[0].start_day}
                        type='execute'
                    />
                    <IdeaState
                        type="ejecucion"
                        typeUser="user"
                    />
                    <ChatBox
                        finish={this.state.finish}
                        admin={true}
                        type='execute'
                        messages={this.state.messages.slice(-5)}
                        loading={this.state.loading}
                        refInputChat={(e) => this.messageInput = e}
                        idea={this.state.ideaId}
                    />

                    {this.state.finish == false?
                        (this.state.admin == true && !this.props.userstate.data) &&
                        <div className="row mt-5">
                            <div className="col-md-12">
                                <div className="float-left">
                                    <Button
                                    event={this.finishIdeaExecute}
                                    type="button"
                                    className="w-25"
                                    state="true"
                                    color="red"
                                    name="Terminar ejecución"
                                    />
                                </div>
                            </div>
                            <p id="message-execute"></p>
                        </div>

                        :
                        <div className="row mt-5">
                        <div className="col-md-12">
                            <div className="float-left">
                            <Link to={"/idea/premiada/"+this.state.ideaId.id}>
                                <Button
                                type="button"
                                className="w-25"
                                state="true"
                                color="red"
                                name="Ir a la premiacion"
                                />
                            </Link>
                            </div>
                        </div>
                    </div>
                    }


                    </div>
                }
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
            passInnovadorIdeaToExecuteAsync,
            finishIdeaExecuteAsync,
            setExecuteTimeAsync,
            saveOperatorsAsync,
            addAllChatsSupAsync
        }, dispatch)
}
function mapStateToProps(state, props) {
    return {
        idea: state.idea,
        isLoading: state.ideaIsLoading,
        users: state.auth.allUsers,
        chat: state.chat,
        operators: state.operators,
        ideas: state.ideasinnovador,
        userstate: !isEmpty(state.auth.administrator)? state.auth.administrator[0].state == 1?  1: 0 : 0,
        user: state.auth
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(IdeaExecute)
