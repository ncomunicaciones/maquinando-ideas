const api = {
  user: {
    async getUser() {
      const { data: user } = await axios.get(axios_url+'/auth-user')
      let { data: administrator } = await axios.get(axios_url+`/${user.id}/is-administrator`)
      administrator = administrator.length ? administrator : null
      return { user, administrator }
    },
    async getAdministrator(adminId) {
      const { data: admin } = await axios.get(axios_url+`/admin/view/${adminId}`)
      return admin
    },
    async getAllUsers() {
      const { data: users } = await axios.get(`${axios_url}/all-users`)
      return users
    }
  },
  request: {
    async getRequests(){
      const {data} = await axios.get(axios_url + '/requests')
      return data
    }
  },
  idea: {
    async getIdea(ideaId) {
      const { data: idea } = await axios.get(`${axios_url}/ideas/${ideaId}/view`)
      return idea
    },
    async evaluateIdea(ideaId, data) {
      const { data: idea } = await axios.post(`${axios_url}/ideas/${ideaId}/evaluateedit`, data)
      return idea
    },
    async passIdeaToExecute(ideaId) {
      const urlExecute = `${axios_url}/ideas/execute_idea/${ideaId}`
      const { data: idea } = await axios.post(urlExecute, null)

      return idea
    },
    async setExecuteTime(ideaExecuteId, time) {
      const { data: ideaExecute } = axios.post(`${axios_url}/ejecutar_idea/${ideaExecuteId}/edit`, { time })
      return ideaExecute
    },
    async finishIdeaExecute(ideaId) {
      const { data } = await axios.post(`${axios_url}/ideas/finish_execute_idea/${ideaId}`)
      return data
    },
    async setStateTime(ideaId, path, time) {
      const url = `${axios_url}/${path}/${ideaId}/edit`
      const { data: idea } = await axios.post(url, { time })
      return idea
    },
    async getSupervisorIdeas() {
      const { data } = await axios.get(axios_url+'/supervisor-ideas')
      return data
    },
    async getIdeasOperator(userId) {
      const { data } = await axios.get(`${axios_url}/ideas/get_ideas_operator/${userId}`)
      return data
    }
  },
  category: {
    async getCategory(categoryId) {
      const { data: category } = await axios.get(`${axios_url}/ideas/categories/${categoryId}`)
      return category
    }
  },
  operator: {
    async saveOperators(ideaExecuteId, operators) {
      const urlOperators = `${axios_url}/ideas/save_operators/${ideaExecuteId}`
      const ideaExecute = await axios.post(urlOperators, operators)

      return ideaExecute
    }
  },
  contentText: {
    async getContentTexts() {
      const { data: contentTexts } = await axios.get(`${axios_url}/content_text/retrieve`)
      return contentTexts
    },
    async updateContentText(textContentId, textContentData) {
      const { data: response } = await axios.post(`${axios_url}/content_text/update/${textContentId}`, textContentData)
      return response.result
    }
  },
  
}

export default api