import React, { Component } from 'react'
import { connect } from 'react-redux'
import IdeasBlock from '../components/IdeasBlock'

class IdeasEjecutor extends Component {
    render() {
        const { ideasinnovador } = this.props

        return (
            <div className="mis-ideas-container mt-5">
                <div className="title" >Ejecutor</div>
                    <div>
                        <IdeasBlock
                            title="Mis ideas como ejecutor"
                            categories={false}
                            ideastate="true"
                            rows={ideasinnovador.filter(idea => idea.operator == true)}
                            link={`/mis-ideas-ejecutor/operador`}
                            isOpen={true}
                            size={ideasinnovador.filter(idea => idea.operator == true).length}
                            link2="/idea-ejecutar/"
                        />
                    </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { ideasinnovador } = state
    return { ideasinnovador }
}

export default connect(mapStateToProps, null)(IdeasEjecutor)