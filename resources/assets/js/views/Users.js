import React, {Component} from 'react'
import { HashRouter as Router, Route, Switch,  NavLink } from 'react-router-dom';
import UserList from '../components/UserList'
import AdminCrearUsuario from './../views/AdminCrearUsuario'

class Users extends Component{
    
    render(){
        return(
            <div className="mt-5">
            <div className="title">Usuarios</div>
            <div className="row">
                <div className="col-md-3 usuarios-sidebar">
                <div className="subtitle">Opciones de usuarios</div>
                    <NavLink exact to="/usuarios" className="usuarios-sidebar-opt mt-3">Lista de usuarios</NavLink>
                    <NavLink exact to="/usuarios/crear" className="usuarios-sidebar-opt">Crear usuario</NavLink>
                </div>
                <Router>
                    <Switch>
                        <Route exact path="/usuarios" component={UserList} />
                        <Route exact path="/usuarios/crear" component={AdminCrearUsuario} />
                        <Route exact path="/usuarios/:type/:id" component={AdminCrearUsuario} />
                    </Switch>
                </Router>
            </div>
            </div>
        )
    }
}

export default Users