import React, { Component } from 'react'
import Textbox from '../components/Textbox'
import {connect} from 'react-redux'
import ComboBox from './../components/ComboBox'
import Button from '../components/Button'
import {editAuthAsync} from '../redux/asyncActions'
import { bindActionCreators } from 'redux'
import {isEmpty} from 'lodash'
import { validatePhone } from '../utils'
import Img from 'react-image'

class Perfil extends Component {
    state = {
        loading: false,
        imgError: '',
        phoneError: '',
        gender: true
    }
    onSubmit = async (e) => {
        e.preventDefault();
        this.setState({gender: false})
        if (validatePhone(this.iPhone.value)) {
            this.setState({loading:true, phoneError: ''})
            const fd = new FormData();
            fd.append('avatar', this.iAvatar.files[0]);
            fd.append('phone', this.iPhone.value);
            fd.append('sex', this.sex.value);
            await this.props.editAuthAsync(fd)
            this.setState({loading:false, gender: true})
        } else {
            this.setState({phoneError: 'Debe ingresar un número de teléfono válido'})
        }
    }  

    optionSex = [
        {
            value: 'm',
            name: 'Hombre'
        },
        {
            value: 'f',
            name: 'Mujer'
        }
    ]

    setAvatar = event => {
        const file = this.iAvatar.files[0]
        const reader = new FileReader()
        if((file.name.split('.').pop() == 'jpg') || (file.name.split('.').pop() == 'png')){
            if(file.size < 300000){
                reader.onloadend = () => {
                    Array.from(document.getElementsByClassName('find-me'))[0].src = reader.result;
                }
                if (file) {
                    reader.readAsDataURL(file)
                }
                this.setState({imgError: ''})
            }else{
                this.setState({imgError: 'La imagen sobrepasa el límite de peso. El límite de peso es 300kb'})
                this.iAvatar.value = "";
            }
        } else{
            this.setState({imgError: 'El formato de la imagen no es permitida'})
            this.iAvatar.value = "";
        }
    } 
    
    getError = (e) => {
        if(this.props.user.gender == 'm') e.target.src = assets + '/avatar.png'
        if(this.props.user.gender == 'f') e.target.src = assets + '/avatar-f.png'
    }
    
    render() {
        return (
            <form noValidate onSubmit={this.onSubmit}>
                <div className="perfil-container">
                    {this.props.user.image}
                    <div id="perfil-image" className="perfil-image">
                        {this.state.gender? <img className="find-me" src={assets+'/avatars/'+this.props.user.avatar} onError={(e) => this.getError(e)} /> : null}
                        <div className="perfil-image-hover"></div>
                        <input
                            onChange={() => this.setAvatar()}
                            id="avatar"
                            type="file"
                            accept="image/x-png,image/gif,image/jpeg"
                            className="input-avatar"
                            ref={node => this.iAvatar = node}
                        />
                        
                    </div>
                    <div className="error-message">{this.state.imgError}</div>
                    <div className="idea-form inactive">
                        <div className="mb-4 clearfix">
                            <Textbox
                                disabled="disabled"
                                disabled="true"
                                title="Cargo"
                                value={isEmpty(this.props.user.administrator)? 'Innovador': 'Facilitador'}
                            />
                        </div>
                        <div className="mb-4">
                            <Textbox 
                                disabled="disabled"
                                title="Nombre"
                                value={this.props.user.name}
                            />
                        </div>
                        <div className="mb-4">
                            <Textbox 
                                disabled="disabled"
                                title="Nombre"
                                value={this.props.user.username}
                            />
                        </div>
                        <div className="mb-4">
                            <Textbox 
                                disabled = "disabled"
                                title="Correo electrónico" 
                                refInput={node => this.iEmail = node}
                                value={this.props.user.email}
                            />
                        </div>
                    </div>
                    <div className="mb-4">
                        <Textbox
                            title="Número telefónico"
                            type="tel"
                            refInput={node => this.iPhone = node}
                            defaultValue={this.props.user.phone}
                        />
                    </div>
                    <div className="mb-4">
                        <ComboBox
                            title="Sexo"
                            select={this.props.user.gender}
                            options={this.optionSex}
                            refInput={node => this.sex = node}
                        />
                    </div>
                    <div className="error-message">{this.state.phoneError}</div>

                    <div className="row text-center m-1">
                        <p id="msg-profile"></p>
                    </div>
                    
                    <div className="row">
                        <div className="col-12 pr-1">
                            <Button
                                name="Realizar cambios"
                                color="red"
                                type="submit"
                                disabled={this.state.loading}
                            />
                        </div>
                    </div>
                </div>
            </form>
        )
    }

}

const mapStateToProps = state => {
    const user = state.auth
    return {user}
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        editAuthAsync
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Perfil)