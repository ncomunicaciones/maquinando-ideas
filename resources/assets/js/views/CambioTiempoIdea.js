import React, { Component } from 'react'
import Button from '../components/Button'
import loader from '../../../../public/img/load.png'
import RadioTime from '../components/RadioTime'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setStateTimeAsync } from '../redux/asyncActions'
import { isEmpty } from 'lodash'
import Alert from '../components/Alert';


class CambioTiempoIdea extends Component {
    state = {
        idea: null,
        ideaCurrent: null,
        time: null,
        loadingPage: true,
        send: false,
        message: false,
        messageText: null,
    }
    timeOptions = [30, 60, 90, 120, 150, 180, 210, 240]
    
    componentDidMount() {
        const idea = this.props.ideassupervisor.find((idea) => idea.id == this.props.match.params.id)
        const ideaCurrent = idea.state == 'ejecucion' ? idea.idea_execute[0] : idea.idea_evaluate[0];
        this.setState({idea, ideaCurrent, time: idea.state == 'ejecucion'? ideaCurrent.execute_time : ideaCurrent.evaluate_time}, () => this.setState({loadingPage: false}))
    }

    changeTime = async () => {
        this.setState({send: true})
        const res = {}
        res.time = this.state.time
        res.generateNoti = 'generate'
        const { ideaCurrent } = this.state
        const path = this.state.idea.state == 'ejecucion' ? 'ejecutar_idea' : 'evaluar_idea';
        await this.props.setStateTimeAsync(ideaCurrent.id, path, res)
        this.setState({send: false, message: true, messageText: 'El tiempo cambio exitosamente a '+ this.state.time +' días'})
    }
    changeMessage = async () => {
        this.setState({message: false})
    }

    setTimeOption = ({ target }) => this.setState({ time: target.value })

    validateClassSelected = time => this.state.time == time ? 'selected' : ''

    getStateText = () =>  this.state.idea.state == 'ejecucion' ? 'ejecución' : 'evaluación'

    render() {
        return this.state.loadingPage ? 
            <div className="mis-ideas-container-carga mt-5"><img src={loader} /></div>
            :
            (
                <div className="mt-5">
                    <div className="row">
                        <div className="col-md-8" >
                            <h5 className="subtitle mb-3">Tiempo de { this.getStateText() } de idea</h5>
                            <p>Cambiar fecha limite de { this.getStateText() }</p>
                            <p>{ this.state.time } días aproximadamente</p>
                        </div>
                        <div className="col-md-12">
                            <div className="clearfix">
                                {
                                    this.timeOptions.map(time => 
                                        <RadioTime
                                            key={`time-${time}`}
                                            time={time}
                                            setTimeOption={this.setTimeOption}
                                            isSelectedClass={`form-check-label square ${this.validateClassSelected(time)}`}
                                        />
                                    )
                                }

                                <p id="message-time"></p>

                                <div className="clearfix mt-3">
                                    <div className="mr-2 float-left">
                                        <Button
                                            event={this.changeTime}
                                            state="true"
                                            color="red"
                                            name="Aceptar"
                                            disabled={this.state.send}
                                        />
                                    </div>
                                    <div className="mr-2 float-left">
                                        <Button
                                            event={() => window.history.go(-1)}
                                            state="true"
                                            color="black"
                                            name="Cancelar"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="succes-message" ref={(e) => this.message = e}></div>
                     <Alert deletethis={() => this.changeMessage()} onTransitionEnd={this.transitionEnd} mounted={this.state.message} type="success" message={this.state.messageText} />
                </div>
            )
    }
}

const mapStateToProps = (state, props) => {
    const { ideassupervisor } = state;
    return { ideassupervisor }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setStateTimeAsync
    },dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CambioTiempoIdea)