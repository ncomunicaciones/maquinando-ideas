import React, {Component} from 'react'
import { HashRouter as Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import CategoriesList from '../components/CategoriesList';
import AdminCreateCategory from '../components/AdminCreateCategory';


class AdminCategorias extends Component{
    

    render(){
        return(
            <div className="mt-5">
                <div className="title">Categorías</div>
                <div className="row">
                    <div className="col-md-3 usuarios-sidebar">
                    <div className="subtitle">Opciones de categorías</div>
                        <NavLink to="/categorias/administrar" className="usuarios-sidebar-opt mt-3">Lista de categorías</NavLink>
                        <NavLink to="/categoria/administrar/crear" className="usuarios-sidebar-opt">Crear categoría</NavLink>
                    </div>
                    <Router>
                        <Switch>
                            <Route exact path="/categorias/administrar" component={CategoriesList} />
                            <Route exact path="/categoria/administrar/crear" component={AdminCreateCategory} />
                            <Route exact path="/categoria/administrar/:id" component={AdminCreateCategory} />
                        </Switch>
                    </Router>
                </div>
            </div>
        )
    }

}

export default AdminCategorias