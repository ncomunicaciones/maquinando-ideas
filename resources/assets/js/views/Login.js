import React, {Component} from 'react'
import Textbox from '../components/Textbox'
import Button from '../components/Button'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import FontAwesome from 'react-fontawesome'

export default class Login extends Component {
    state = {
        auth: false,
        loading: false,
        errors: {},
        login: true,
        recoverPassword: false,
        requestAccess: false,
        validateButtonRequest: true,
        validateButtonRecover: true
    }

    onSubmitLogin = async (event) => {
        var eventForm = event.target; 
        event.preventDefault()
        this.setState({ loading: true, errors: {} })
        const $message = document.getElementById('msg-auth')
        axios.post(axios_url+'/login', {
            username: eventForm.elements.username.value,
            password: eventForm.elements.password.value
        }).then((res) => {
            this.validateForm(res.data)
            this.setState({loading: false})
        })
    }
    onSubmitRequest = async event => {
        event.preventDefault()
        this.setState({ loading: true, errors: {} })
        const $message = document.getElementById('msg-auth')
        $message.className = 'text-info'
        $message.textContent = 'Enviando petición...'
        const {data} = await axios.post(axios_url+'/newuser', {
            name: event.target.elements.name.value,
            email: event.target.elements.email.value,
            phone: event.target.elements.phone.value
        })

        if(data == 'error1'){
            $message.textContent = 'El correo ya fue registrado.'
            this.setState({loading: false})
        }else if(data == 'error2'){
            $message.textContent = 'La solicitud se encuentra en proceso.'
            this.setState({loading: false})
        }else{
            $message.textContent = 'La solicitud se envió. Estaremos en contacto cuando su solicitud sea aprobada.'
            this.setState({validateButtonRequest: false, loading: false})
        }

    }

    onSubmitRecover = async event => {
        event.preventDefault()
        this.setState({ loading: true, errors: {} })
        const $message = document.getElementById('msg-auth')
        $message.className = 'text-info'
        $message.textContent = 'Autenticando...'
        const { data } = await axios.post(axios_url+'/recoverpassword', {
            username: event.target.elements.username1.value,
        })
        if(data != 'error'){
            $message.textContent = 'Se envio un correo con la nueva contraseña a su email registrado con esta cuenta.'
            this.setState({validateButtonRecover: false , loading: false})
        }else{
            $message.textContent = 'El usuario no existe en nuestra plataforma.';
            this.setState({loading: false})
        }
    }

    validateForm(response){
        const $message = document.getElementById('msg-auth')
        if (response.errors) {
            $message.textContent = ''
            const {errors} = response
            this.setState({errors, loading: false})
        } else {
            switch (response.result) {
                case 'success': {
                    this.setState({ auth: true, loading: false })
                    break
                }
                case 'error': {
                    $message.className = 'text-danger'
                    $message.textContent = response.message

                    this.setState({ auth: false, loading: false })
                    break
                }
            }
        }
    }

    componentDidMount(){
        const element = this.iform
        setTimeout(() => {element.elements.password.focus()}, 5000)  
        setTimeout(() => {element.elements.username.focus()}, 5000)        
    }

    recoverPasswordAction = () => this.setState({login:false, recoverPassword: true,requestAccess: false})

    requestAccessAction = () => this.setState({login:false, recoverPassword: false,requestAccess: true})

    backLogin = () => this.setState({login:true, recoverPassword: false,requestAccess: false})

    render() {
        if (this.state.auth) {
            return <Redirect to="/" />
        }

        return (
            <div className="login-view" style={{backgroundImage: `url(${assets + "/background.png"})`}} >
                {this.state.login && 
                    <div className="login-container">
                        <div className="login-responsive">
                            <img src={assets + "/logo-load.png"} />
                            <div>La plataforma no se encuentra disponible en versión móvil.</div>
                        </div>
                        <a target="_blank" href="http://unimaq.com.pe/"><img className="unimaq-logo" src={assets + "/unimaq-logo.png"} /></a>
                        <img className="login-logo" src={assets + "/login-logo.png"} />
                        <form ref={(e) => this.iform = e} onSubmit={this.onSubmitLogin} noValidate>
                            <Textbox
                                autocomplete="off"
                                name="username"
                                classNameOP="active"
                                title="Usuario"
                                type="text"
                                defaultValue=""
                                ref={(e) => this.loginVal = e}
                                errors={this.state.errors.username}
                            />
                            <Textbox
                                autocomplete="off"
                                defaultValue=""
                                classNameOP="active"
                                name="password"
                                title="Password"
                                type="password"
                                errors={this.state.errors.password}
                            />
                            <div id="msg-auth"></div>
                            <div className="row m-0 mt-5">
                                <div className="col-7 pr-1 pl-0">
                                    <Button event={() => this.requestAccessAction()} state="true" color="grey" name="Solicitar acceso" type="button"/>
                                </div>
                                <div className="col-5 pl-1 pr-0">
                                    <Button color="red" name="Ingresar" type="submit" disabled={this.state.loading} />
                                </div>
                            </div>
                        </form>
                        <div className="login-forget-password">
                            <a onClick={() => this.recoverPasswordAction()}>¿Haz olvidado tu contraseña?</a>
                        </div>
                    </div>
                }
                {this.state.recoverPassword && 
                    <div className="login-container recover-password">
                        <div onClick={() => this.backLogin()} className="back-login"><FontAwesome name="chevron-left" /> Login</div>
                        <a target="_blank" href="http://unimaq.com.pe/"><img className="unimaq-logo" src={assets + "/unimaq-logo.png"} /></a>
                        <div className="title">Recuperar Contraseña</div>
                        <div className="paragraph mb-3">Recuperara la contraseña colocando tu nombre de usuario o email:</div>
                        <form onSubmit={this.onSubmitRecover} noValidate>
                            <Textbox
                                name="username1"
                                title="Credencial"
                                type="text"
                                errors={this.state.errors.username}
                            />
                            <div className="mt-4">
                                <div className="" id="msg-auth">
                                </div>
                            </div>
                            {this.state.validateButtonRecover? 
                            <div className="row m-0 mt-4">
                                <div className="col-12 pr-1 pl-0">
                                    <Button color="red" name="Solicitar" type="submit" disabled={this.state.loading} />
                                </div>
                            </div>
                            :
                            null}
                        </form>
                    </div>
                }
                {this.state.requestAccess && 
                    <div className="login-container request-access">
                        <div onClick={() => this.backLogin()} className="back-login"><FontAwesome name="chevron-left" /> Login</div>
                        <a target="_blank" href="http://unimaq.com.pe/"><img className="unimaq-logo" src={assets + "/unimaq-logo.png"} /></a>
                        <div className="title mb-5">Solicitar acceso</div>
                        <form onSubmit={this.onSubmitRequest} noValidate>
                            <Textbox
                                name="name"
                                title="Nombre y apellido"
                                type="text"
                                errors={this.state.errors.name}
                            />
                            <Textbox
                                name="email"
                                title="Correo electronico"
                                type="text"
                                errors={this.state.errors.username}
                            />
                            <Textbox
                                name="phone"
                                title="Telefono/Celular"
                                type="text"
                                errors={this.state.errors.password}
                            />
                            <div className="mt-4">
                                <div className="" id="msg-auth"></div>
                            </div>
                            {this.state.validateButtonRequest? 
                            <div className="row m-0 mt-4">
                                <div className="col-12 pr-1 pl-0">
                                    <Button color="red" name="Solicitar" type="submit" disabled={this.state.loading} />
                                </div>
                            </div>
                            :
                            null}
                        </form>
                    </div>
                }
            </div>
        )
    }
}