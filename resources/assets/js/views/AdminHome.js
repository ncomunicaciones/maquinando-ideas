import React from 'react';
import {connect} from 'react-redux';
import IdeasBlock from '../components/IdeasBlock'
import { setContentText } from '../utils'


class AdminHome extends React.Component{
    value = false
    tryMe = (val) =>{
        if(val > 0 && !this.value) {
            this.value = true
            return true
        }else{
            return false
        }
    }
    render(){
        return(
            <div className="mis-ideas-container pt-5">
                <div className="row">
                    <div className="col-md-7 paragraph">
                        { setContentText(this.props.contenttext, 'text-admin-home') }
                    </div>
                </div>
                {this.props.adminideas.filter((idea) => idea.state == 'evaluacion').length > 0 && 
                    <IdeasBlock isOpen={this.tryMe(this.props.adminideas.filter((idea) => idea.state == 'evaluacion').length)} title="Ideas por evaluar" categories={false} rows={this.props.adminideas.filter((idea) => idea.state == 'evaluacion').slice(0,3)} size={this.props.adminideas.filter((idea) => idea.state == 'evaluacion').length} link="/ideas/evaluacion"  />
                }
                {this.props.adminideas.filter((idea) => idea.state == 'aprobado').length > 0 && 
                    <IdeasBlock isOpen={this.tryMe(this.props.adminideas.filter((idea) => idea.state == 'aprobado').length)} title="Ideas aprobadas" categories={false} rows={this.props.adminideas.filter((idea) => idea.state == 'aprobado').slice(0,3)} size={this.props.adminideas.filter((idea) => idea.state == 'aprobado').length} link="/ideas/aprobado" link2="/idea-evaluar/"  />
                }
                {this.props.adminideas.filter((idea) => idea.state == 'ejecucion').length > 0 && 
                    <IdeasBlock isOpen={this.tryMe(this.props.adminideas.filter((idea) => idea.state == 'ejecucion').length)} title="Ideas en ejecucion" categories={false} rows={this.props.adminideas.filter((idea) => idea.state == 'ejecucion').slice(0,3)} size={this.props.adminideas.filter((idea) => idea.state == 'ejecucion').length} link="/ideas/ejecucion" link2="/idea-ejecutar/" />
                }
                {this.props.adminideas.filter((idea) => idea.state == 'premiada').length > 0 && 
                    <IdeasBlock isOpen={this.tryMe(this.props.adminideas.filter((idea) => idea.state == 'premiada').length)} title="Ideas premiadas" categories={false} rows={this.props.adminideas.filter((idea) => idea.state == 'premiada').slice(0,3)} size={this.props.adminideas.filter((idea) => idea.state == 'premiada').length} link="/ideas/premiada"  />
                }
                {this.props.adminideas.filter((idea) => idea.state == 'desaprobado').length > 0 && 
                    <IdeasBlock isOpen={this.tryMe(this.props.adminideas.filter((idea) => idea.state == 'desaprobado').length)} title="Ideas desaprobadas" categories={false} rows={this.props.adminideas.filter((idea) => idea.state == 'desaprobado').slice(0,3)} size={this.props.adminideas.filter((idea) => idea.state == 'desaprobado').length} link="/ideas/desaprobado" link2="/idea-evaluar/" />
                }
                {this.props.adminideas.filter((idea) => idea.state == 'abortadas').length > 0 && 
                    <IdeasBlock isOpen={this.tryMe(this.props.adminideas.filter((idea) => idea.state == 'abortadas').length)} title="Ideas abortadas" categories={false} rows={this.props.adminideas.filter((idea) => idea.state == 'abortadas').slice(0,3)} size={this.props.adminideas.filter((idea) => idea.state == 'abortadas').length} link="/ideas/abortadas" link2="/idea-ejecutar/" />
                }
                {
                    this.props.adminideas.length == 0?
                    <div className="admin-home-rest">No tienes ideas para evaluar.</div>
                    : null
                }
            </div>
        )
    }
}
    

const mapStateToProps = state => {
    const adminideas = state.ideassupervisor
    const { contenttext } = state
    return {adminideas, contenttext}
}

export default connect(mapStateToProps, null)(AdminHome)