import React from 'react';
import CategoryList from '../components/CategoryList';
import { connect } from 'react-redux'
import { setContentText } from '../utils'

const SelectCategory = ({ contenttext }) => 
    <div className="mt-4">
        <div className="row">
            <div className="col-md-7 paragraph">
                { setContentText(contenttext, 'text-user-home') }
            </div>
        </div>
        <div className="title">{ setContentText(contenttext, 'title-home-categories') }</div>
        <CategoryList />
    </div>

const mapStateToProps = state => {
    const { contenttext } = state
    return { contenttext }
}

export default connect(mapStateToProps, null)(SelectCategory)