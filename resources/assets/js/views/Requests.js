import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
import FontAwesome from 'react-fontawesome'
import {isEmpty} from 'lodash'

const Requests = ({requests, users}) => {
        return(
            <div className="mis-ideas-container mt-5">
                <div className="title" >Solicitudes</div>
                {isEmpty(requests) && 
                    <div className="mis-ideas-container-no-register">No hay solicitudes recibidas.</div>
                }
                {requests.filter((request) => request.type == 'create').length > 0 && 
                    <div>
                        <div className="subtitle subtitle-mis-ideas clearfix">
                            <div className="float-left">Crear nuevo usuario</div>
                            <div className="subtitle-icon" onClick={(e) => toggleItems(e)}><FontAwesome name="chevron-up" /></div>
                        </div>
                        <div className="clearfix w-100">
                            <div>
                                <table className="table table-hover ">
                                    <thead>
                                        <tr>
                                        <th className="pl-0" scope="col" width="100%">Solicitante</th>
                                        <th className="pl-0" scope="col"><span style={{'paddingRight': '45px'}}>Opciones</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {requests.filter((request) => request.type == 'create').map(
                                        (request, i) => 
                                            <tr key={i}>
                                                <td scope="row">{request.name}</td>
                                                <td>
                                                    <Link to={"/usuarios/crear/"+request.id}><FontAwesome style={{'color': '#333'}} name="search" /></Link>
                                                </td>
                                            </tr>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                }
                {requests.filter((request) => request.type == 'update').length > 0 && 
                    <div>
                        <div className="subtitle subtitle-mis-ideas clearfix">
                        <div className="float-left">Actualizar contraseña</div>
                            <div className="subtitle-icon" onClick={(e) => toggleItems(e)}><FontAwesome name="chevron-up" /></div>
                        </div>
                        <div className="clearfix w-100">
                                <div>
                                    <table className="table table-hover ">
                                        <thead>
                                            <tr>
                                            <th className="pl-0" scope="col" width="100%">Solicitante</th>
                                            <th className="pl-0" scope="col"><span style={{'paddingRight': '45px'}}>Opciones</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {requests.filter((request) => request.type == 'update').map(
                                            (request, i) => 
                                                <tr key={i}>
                                                    <td scope="row">{request.name}</td>
                                                    <td>
                                                        {!isEmpty(users.find(user => user.username == request.name))?
                                                        <Link to={"/usuarios/detalle/"+ users.find(user => user.username == request.name).id}><FontAwesome style={{'color': '#333'}} name="search" /></Link>: null
                                                        }
                                                    </td>
                                                </tr>
                                        )}
                                        </tbody>
                                    </table>
                                </div>
                            
                        </div>
                    </div>
                }
            </div>

        )
}
const mapStateToProps = state => {
    const requests = state.requests.filter((request) => request.state == 0)
    const {users} = state
    return {requests, users}
}

export default connect(mapStateToProps, null)(Requests)