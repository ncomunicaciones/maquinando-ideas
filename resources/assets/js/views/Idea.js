import React, {Component} from 'react';
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import IdeaProgress from '../containers/IdeaProgress';
import IdeaEvaluate from '../containers/IdeaEvaluate'
import IdeaExecute from '../containers/IdeaExecute'
import IdeaCongrats from '../containers/IdeaCongrats'
import CategoryDetail from '../components/CategoryDetail';
import {Redirect} from 'react-router-dom'
import IdeaForm from '../containers/IdeaForm';
import {isEmpty, isNaN} from 'lodash'

class Idea extends Component {
    state = {
        linkprops: [this.props.match.params.id, this.props.match.params.state],
        idea: null,
        state: null,
        idea_category: null,
        idea_state: null,
        validateIdea: false,
        redirect: false,
        wait: false
    }

    componentDidMount(){
        this.validateProps();
    }

    validateProps(){
        if(this.props.match.params.state == 'crear'){
            this.setState({idea: 'crear', idea_category: this.props.categories.find((category) => category.slug == this.props.match.params.id), state: {index : 0, name: 'guardado'}})
            this.setState({wait: true})
        }else{
            //Validacion de la idea ( si existe y se encuentra en el estado que es indicado en la URL )
            const allStates = ['guardado','evaluacion','aprobado','desaprobado','abortado','ejecucion','premiada']
            !isEmpty(this.props.ideas.find((val) => val.id == this.state.linkprops[0])) && allStates.findIndex((state) => state == this.state.linkprops[1]) != -1?
            this.setValues() :
            this.setState({redirect: true})
        }
    }


    setValues(){
        const allStates = ['guardado','evaluacion','aprobado','desaprobado','abortado','ejecucion','premiada']
        // Guardar el estado en el que se encuentra la idea => state
        this.setState({state: {index : parseInt((allStates.findIndex((state) => state == this.state.linkprops[1]) + 1)), name: this.state.linkprops[1]} })

        // Buscar por parametros y poner la idea en el state => idea
        this.setState({idea: this.props.ideas.find((val) => val.id == this.state.linkprops[0])}, () => {
            
            // Buscar Estado de la idea => idea_category
            this.setState({idea_state: parseInt((allStates.findIndex((state) => state == this.state.idea.state)))})
            
            // Buscar Categoria a la que pertenece la idea => idea_category
            this.setState({idea_category: this.props.categories.find((category) => category.id == this.state.idea.category_id)})

            this.setState({wait: true})
        })
    }

    componentDidUpdate(){
        // Validar que los parametros no tengan ningun cambio
        if(this.props.match.params.id != this.state.linkprops[0] || this.props.match.params.state != this.state.linkprops[1]){
            this.setState({wait: false, linkprops: [this.props.match.params.id, this.props.match.params.state]}, () => this.validateProps())
        }

        // Validar que el estado de la idea no tenga ningun cambio
        if(this.state.idea  && this.state.idea != 'crear'){
            if((this.props.ideas.find((val) => val.id == this.state.idea.id).state) != this.state.idea.state || 
               (this.props.ideas.find((val) => val.id == this.state.idea.id).title) != this.state.idea.title)
            {this.setState({wait: false}); this.validateProps()}
        }
    }

    render(){
        return (
            <div className="mt-4">
                {this.state.redirect && <Redirect to="/error-page" />}
                {this.state.wait &&
                    <div>
                        <IdeaProgress stage={this.state.state} ideaId={this.state.idea} ideastate={this.state.idea_state} />
                        <div className="row">
                            <div className="col-md-4">
                                <CategoryDetail idea={this.state.idea} innovador={this.state.idea.usercreator} category={this.state.idea_category} ideaName={this.state.idea? this.state.idea.title : null} auth={this.props.auth} />
                            </div>
                            {this.state.idea && (this.state.idea == 'crear' || this.state.linkprops[1] == 'guardado')  &&
                                <IdeaForm admin={this.props.admin} category={this.state.idea_category} ideaId={!isNaN(this.state.idea.id)? this.state.idea.id : null} />
                            }
                            {this.state.idea && this.state.linkprops[1] == 'evaluacion' &&
                                <IdeaEvaluate admin={this.props.admin} ideaselect={this.state.idea} />
                            }
                            {this.state.idea && this.state.linkprops[1] == 'ejecucion' &&
                                <IdeaExecute admin={this.props.admin} ideaselect={this.state.idea} />
                            }
                            {this.state.idea && this.state.linkprops[1] == 'premiada' &&
                                <IdeaCongrats ideaselect={this.state.idea} />
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    const ideas = state.auth.administrator.length > 0 && state.auth.administrator[0].state ? state.ideassupervisor : state.ideasinnovador
    const admin = state.auth.administrator.length > 0 && state.auth.administrator[0].state ? true : false
    const { categories } = state
    const { auth } = state
    return { ideas, admin, categories, auth }
}


export default connect(mapStateToProps, null)(Idea)