import React, { Component } from 'react'
import Textbox from './../components/Textbox'
import Button from './../components/Button'
import ComboBox from './../components/ComboBox'
import { validateEmail } from '../utils'
import {isEmpty} from 'lodash'
import {addUserAsync, editUserAsync, getRequestsAsync} from '../redux/asyncActions'
import { connect } from 'react-redux'
import { updateRequestsAsync } from '../redux/asyncActions'
import { bindActionCreators } from 'redux';
import Alert from './../components/Alert'

class AdminCrearUsuario extends Component {
    state = {
        type: null,
        user: null,
        errors: {},
        send: false,
        message: false,
        messageText: null,
        loading: false
    }
    _isMounted = false
    componentDidMount(){
        this._isMounted = true
        this._isMounted? this.setState({type: this.props.match.params.type}, () => {
            if(!isEmpty(this.state.type)){
                if(this.state.type == 'crear'){
                    this.setState({type: this.props.match.params.type, user: this.props.requests.find(request => request.id == this.props.match.params.id)}, () => console.log(this.state.user))
                }else{
                    this.setState({type: this.props.match.params.type, user: this.props.users.find((user) => user.id == this.props.match.params.id)})
                }
            }
        }) : null
    }

    componentDidUpdate(){
        if(this.state.type != null){
            if(isEmpty(this.props.match.params.type)){
                this.setState({type:null, user:null})
            }
        }
    }


    inputs = {}
    optionPositions = [
        {
            value: 1,
            name: 'Facilitador'
        },
        {
            value: 0,
            name: 'Innovador'
        }
    ]

    optionSex = [
        {
            value: 'm',
            name: 'Hombre'
        },
        {
            value: 'f',
            name: 'Mujer'
        }
    ]
    editUser = async (event) => {
        event.preventDefault()
        if (this.validateFields(true, 'edit')) {
            this.setState({ errors: {} })
            try {
                this.setState({send: true})
                const dataUser = {
                    id: this.state.user.id,
                    names: this.inputs.names.value,
                    username: this.inputs.username.value,
                    email: this.inputs.email.value,
                    password: this.inputs.password.value,
                    number: this.inputs.number.value,
                    sex: this.inputs.sex.value
                }
                await this.props.editUserAsync(dataUser)
                await this.props.getRequestsAsync();
                this.setState({message: true, messageText: 'Se edito al usuario exitosamente', send: false})

            } catch (err) {
                console.error(err)
                $message.textContent = 'Hubo un error en la petición'
            }
        } 
    }

    changeMessage = async () => {
        this._isMounted? this.setState({message: false}) : null
    }

    createUser = async (event) => {
        event.preventDefault()
        if (this.validateFields(false, null)) {
            this._isMounted? this.setState({ errors: {} }) : null;
            try {
                this._isMounted? this.setState({send: true}) : null
                const dataUser = {
                    names: this.inputs.names.value,
                    username: this.inputs.username.value,
                    email: this.inputs.email.value,
                    password: this.inputs.password.value,
                    repeat_password: this.inputs.repeat_password.value,
                    position: this.inputs.position.value,
                    number: this.inputs.number.value,
                    sex: this.inputs.sex.value
                }

                await this.props.addUserAsync(dataUser)
                await this.props.getRequestsAsync();
                document.getElementById('myform').reset();
                this._isMounted? this.setState({message: true, messageText: 'Se creo al usuario exitosamente', send: false}) : null

            } catch (err) {
                console.error(err)
                $message.textContent = 'Hubo un error en la petición'
            }
        }
    }

    validateFields = (edit, edit1) => {
        let result = true 
        let errors = {}
        for (let key in this.inputs) {
            if (!(key == 'position' && edit)) { 
                if (!this.inputs[key].value) {
                    if (!(key == 'password' || key == 'repeat_password') && !edit) {
                        errors[key] = ['Este campo es requerido']
                    }
                } else {
                    if (key === 'email') {
                        if(isEmpty(edit1)){
                            if (!validateEmail(this.inputs.email.value)) {
                                errors.email = ['El email es invalido']
                            }

                            if(!isEmpty(this.props.users.find((user) => user.email.trim() == this.inputs.email.value.trim()))){
                                errors.email = ['El email ya existe']
                            }

                            if(!isEmpty(this.props.users.find((user) => user.username.trim() == this.inputs.username.value.trim()))){
                                errors.username = ['El usuario ya existe']
                            }
                        }else{
                            if(!isEmpty(this.props.users.filter((user) => user.id != this.state.user.id).find((user) => user.username.trim() == this.inputs.username.value.trim()))){
                                errors.username = ['El usuario ya existe']
                            }
                            if(!isEmpty(this.props.users.filter((user) => user.id != this.state.user.id).find((user) => user.email.trim() == this.inputs.email.value.trim()))){
                                errors.email = ['El email ya existe']
                            }
                        }
                    }
                }
            }
        }

        const { password, repeat_password } = this.inputs
        if (password.value !== repeat_password.value) {
            errors.repeat_password = ['La contraseña debe coincidir']
        }

        if (Object.keys(errors).length) {
            this.setState({ errors })
            result = false
        }
        return result
    }

    render() {
        return (
            <div className="col-md-9 usuarios-content">
            <div className="subtitle mt-3"> {isEmpty(this.state.user) || this.state.type == 'crear'? 'Crear usuario' : 'Editar usuario' }</div>
            <div className="mt-2 d-flex flex-row-reverse">
                <div className="col-md-12">
                    {isEmpty(this.state.user)?
                    <form id="myform" onSubmit={this.createUser} noValidate autoComplete="nope">
                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="names"
                                    title="Nombres"
                                    refInput={node => this.inputs.names = node}
                                    errors={this.state.errors.names}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="username"
                                    title="Usuario"
                                    refInput={node => this.inputs.username = node}
                                    errors={this.state.errors.username}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="email"
                                    title="Correo Electrónico"
                                    autocomplete="nope"
                                    refInput={node => this.inputs.email = node}
                                    errors={this.state.errors.email}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="number"
                                    title="Número telefonico"
                                    autocomplete="nope"
                                    type="tel"
                                    refInput={node => this.inputs.number = node}
                                    errors={this.state.errors.number}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-6">
                                <Textbox
                                    name="password"
                                    title="Contraseña"
                                    type="password"
                                    refInput={node => this.inputs.password = node}
                                    errors={this.state.errors.password}
                                />
                            </div>
                            <div className="col-md-6">
                                <Textbox
                                    name="repeat_password"
                                    title="Repetir contraseña"
                                    type="password"
                                    refInput={node => this.inputs.repeat_password = node}
                                    errors={this.state.errors.repeat_password}
                                />
                            </div>
                        </div>

                        <div className="row mt-5">
                            <div className="col-md-12">
                                <ComboBox
                                    name="position"
                                    title="Perfil"
                                    options={this.optionPositions}
                                    refInput={node => this.inputs.position = node}
                                    errors={this.state.errors.position}
                                />
                            </div>
                        </div>

                        <div className="row mt-5">
                            <div className="col-md-12">
                                <ComboBox
                                    name="sex"
                                    title="Sexo"
                                    options={this.optionSex}
                                    refInput={node => this.inputs.sex = node}
                                    errors={this.state.errors.sex}
                                />
                            </div>
                        </div>

                        <div className="clearfix mt-5">
                            <div className="mr-2 float-right">
                                <Button
                                    event={() => window.history.go(-1)}
                                    type="button"
                                    state="true"
                                    color="black"
                                    name="Cancelar"
                                />
                                
                            </div>
                            <div className="mr-2 float-right">
                                <Button
                                    type="submit"
                                    color="red"
                                    name="Crear"
                                    disabled={this.state.send}
                                />
                            </div>
                        </div>
                    </form>
                    :
                    this.props.match.params.type == 'crear'?
                    <form id="myform" onSubmit={this.createUser} noValidate autoComplete="nope">
                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="names"
                                    title="Nombres"
                                    defaultValue={this.state.user.name}
                                    refInput={node => this.inputs.names = node}
                                    errors={this.state.errors.names}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="username"
                                    title="Usuario"
                                    refInput={node => this.inputs.username = node}
                                    errors={this.state.errors.username}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="email"
                                    title="Correo Electrónico"
                                    autocomplete="nope"
                                    defaultValue={this.state.user.email}
                                    refInput={node => this.inputs.email = node}
                                    errors={this.state.errors.email}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <Textbox
                                    name="number"
                                    title="Número telefónico"
                                    autocomplete="nope"
                                    defaultValue={this.state.user.phone}
                                    refInput={node => this.inputs.number = node}
                                    errors={this.state.errors.number}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-6">
                                <Textbox
                                    name="password"
                                    title="Contraseña"
                                    type="password"
                                    refInput={node => this.inputs.password = node}
                                    errors={this.state.errors.password}
                                />
                            </div>
                            <div className="col-md-6">
                                <Textbox
                                    name="repeat_password"
                                    title="Repetir contraseña"
                                    type="password"
                                    refInput={node => this.inputs.repeat_password = node}
                                    errors={this.state.errors.repeat_password}
                                />
                            </div>
                        </div>

                        <div className="row mt-5">
                            <div className="col-md-12">
                                <ComboBox
                                    name="position"
                                    title="Seleccione Cargo o Función"
                                    options={this.optionPositions}
                                    refInput={node => this.inputs.position = node}
                                    errors={this.state.errors.position}
                                />
                            </div>
                        </div>

                        <div className="row mt-5">
                            <div className="col-md-12">
                                <ComboBox
                                    name="sex"
                                    title="Sexo"
                                    options={this.optionSex}
                                    
                                    refInput={node => this.inputs.sex = node}
                                    errors={this.state.errors.sex}
                                />
                            </div>
                        </div>

                        <div className="clearfix mt-5">
                            <div className="mr-2 float-right">
                                <Button
                                    event={() => window.history.go(-1)}
                                    type="button"
                                    state="true"
                                    color="black"
                                    name="Cancelar"
                                />
                                
                            </div>
                            <div className="mr-2 float-right">
                                <Button
                                    type="submit"
                                    color="red"
                                    name="Crear"
                                    disabled={this.state.send}
                                />
                            </div>
                        </div>
                    </form>
                        :
                        <form id="myform" onSubmit={this.editUser} noValidate autoComplete="off">
                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="names"
                                        title="Nombres y apellidos"
                                        defaultValue={this.state.user.name}
                                        refInput={node => this.inputs.names = node}
                                        errors={this.state.errors.names}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="username"
                                        defaultValue={this.state.user.username}
                                        title="Usuario"
                                        refInput={node => this.inputs.username = node}
                                        errors={this.state.errors.username}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="email"
                                        defaultValue={this.state.user.email}
                                        title="Correo Electrónico"
                                        refInput={node => this.inputs.email = node}
                                        errors={this.state.errors.email}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="number"
                                        title="Número telefónico"
                                        refInput={node => this.inputs.number = node}
                                        defaultValue={this.state.user.phone}
                                        errors={this.state.errors.number}
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-6">
                                    <Textbox
                                        name="password"
                                        title="Contraseña"
                                        type="password"
                                        refInput={node => this.inputs.password = node}
                                        errors={this.state.errors.password}
                                    />
                                </div>
                                <div className="col-md-6">
                                    <Textbox
                                        name="repeat_password"
                                        title="Repetir contraseña"
                                        type="password"
                                        refInput={node => this.inputs.repeat_password = node}
                                        errors={this.state.errors.repeat_password}
                                    />
                                </div>
                            </div>

                            <div className="row mt-5">
                                <div className="col-md-12">
                                    <ComboBox
                                        name="position"
                                        title="Sexo"
                                        options={this.optionSex}
                                        select={this.state.user.gender}
                                        refInput={node => this.inputs.sex = node}
                                        errors={this.state.errors.sex}
                                    />
                                </div>
                            </div>

                            <div className="clearfix mt-5">
                                <div className="mr-2 float-right">
                                    <Button
                                        event={() => window.history.go(-1)}
                                        type="button"
                                        state="true"
                                        color="black"
                                        name="Cancelar"
                                    />
                                    
                                </div>
                                <div className="mr-2 float-right">
                                    <Button
                                        type="submit"
                                        color="red"
                                        name="Actualizar"
                                        disabled={this.state.send}
                                    />
                                </div>
                            </div>
                        </form>

                    }
                    <Alert deletethis={() => this.changeMessage()} onTransitionEnd={this.transitionEnd} mounted={this.state.message} type="success" message={this.state.messageText} />
                </div>
            </div>
            </div>
        )
    }

}


const mapStateToProps = state => {
    const users = state.auth.allUsers
    const {requests} = state
    return { users,requests }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        addUserAsync,
        editUserAsync,
        updateRequestsAsync,
        getRequestsAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminCrearUsuario)