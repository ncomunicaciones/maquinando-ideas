import React, { Component } from 'react';
import {connect} from 'react-redux';
import { HashRouter as Router, Route,Switch, Link, Redirect, NavLink } from 'react-router-dom';
import IdeasBlock from '../components/IdeasBlock'
import { isEmpty, isBoolean } from 'lodash'

class MisIdeas extends Component {
    state = {
        set : false
    }
    value = false
    tryMe = (val) =>{
        if(val > 0 && !this.value) {
            this.value = true
            return true
        }else{
            return false
        }
    }
    render() {
        const {ideas} = this.props
        return <div className="mis-ideas-container mt-5">
            <div className="title" >Mis ideas</div>
            {isEmpty(ideas) &&
                <div className="mis-ideas-container-no-register" style={{ color: '#333' }}>
                No has creado ninguna idea.
                <Link to={"/"}> Agrega una nueva idea.</Link>
                </div>
            }
            {ideas.filter((idea) => idea.state == 'guardado').length > 0 &&
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'guardado').length)} title="Mis ideas guardadas" size={ideas.filter((idea) => idea.state == 'guardado').length} categories={false} rows={ideas.filter((idea) => idea.state == 'guardado').slice(0,3)} link="/mis-ideas/guardado" link2="/idea/"  />
            }
            {ideas.filter((idea) => idea.state == 'evaluacion').length > 0 && 
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'evaluacion').length)} title="Mis ideas en evaluación" size={ideas.filter((idea) => idea.state == 'evaluacion').length} categories={false} rows={ideas.filter((idea) => idea.state == 'evaluacion').slice(0,3)} link="/mis-ideas/evaluacion" link2="/idea/evaluacion/"  />
            }
            {ideas.filter((idea) => idea.state == 'aprobado').length > 0 && 
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'aprobado').length)} title="Mis ideas aprobadas" size={ideas.filter((idea) => idea.state == 'aprobado').length} categories={false} rows={ideas.filter((idea) => idea.state == 'aprobado').slice(0,3)} link="/misideas/aprobado" link2="/idea/evaluar/"  />
            }
            {ideas.filter((idea) => idea.state == 'ejecucion').length > 0 && 
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'ejecucion').length)} title="Mis ideas aprobadas en ejecución" size={ideas.filter((idea) => idea.state == 'ejecucion').length} categories={false} rows={ideas.filter((idea) => idea.state == 'ejecucion').slice(0,3)} link="/mis-ideas/ejecucion" link2="/idea-ejecutar/"  />
            }
            {ideas.filter((idea) => idea.state == 'desaprobado').length > 0 && 
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'desaprobado').length)} title="Mis ideas desaprobadas" size={ideas.filter((idea) => idea.state == 'desaprobado').length} categories={false} rows={ideas.filter((idea) => idea.state == 'desaprobado').slice(0,3)} link="/mis-ideas/desaprobado" link2="/idea-evaluar/" />
            }
            {ideas.filter((idea) => idea.state == 'abortadas').length > 0 && 
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'abortadas').length)} title="Mis ideas abortadas" size={ideas.filter((idea) => idea.state == 'abortadas').length} categories={false} rows={ideas.filter((idea) => idea.state == 'abortadas').slice(0,3)} link="/mis-ideas/abortadas" link2="/idea-ejecutar/" />
            }
            {ideas.filter((idea) => idea.state == 'premiada').length > 0 && 
                <IdeasBlock isOpen={this.tryMe(ideas.filter((idea) => idea.state == 'premiada').length)} title="Mis ideas premiadas" size={ideas.filter((idea) => idea.state == 'premiada').length} categories={false} rows={ideas.filter((idea) => idea.state == 'premiada').slice(0,3)} link="/mis-ideas/premiada" link2="/idea-ejecutar/" />
            }
        </div>
    }
}

const mapStateToProps = state => {
    const ideas = state.ideasinnovador.filter((idea) => idea.operator === false || !isBoolean(idea.operator))
    return {state, ideas}
}


export default connect(mapStateToProps, null)(MisIdeas)