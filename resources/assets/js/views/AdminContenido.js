import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {updateContentTextAsync} from '../redux/asyncActions'
import FontAwsome from 'react-fontawesome'

class AdminContenido extends Component {
  state = { idLoading: false }

  async handleUpdateContentText(contentTextId){
    this.setState({idLoading: true})
    const text = document.getElementById(`content-text-${contentTextId}`).value
    const data = { text }
    await this.props.updateContentTextAsync(contentTextId, data)
    this.setState({idLoading: false})
  }

  render() {
    return (
      <div>
        <div className="row mt-2">
          <a href="#" className="idea-progress-return mb-1">
            <FontAwsome name="chevron-left" /> Regresar
          </a>
        </div>
        <table className="table table-hover mt-5">
          <thead>
              <tr className="row">
                <th className="pl-0" className="col-sm-4" scope="col">Descripción</th>
                <th className="pl-0" className="col-sm-6" scope="col">Contenido</th>
                <th className="pl-0" className="col-sm-2" scope="col"></th>
              </tr>
          </thead>
          <tbody>
              {
                this.props.contenttext.map((contentText, i) => 
                  <tr key={contentText.slug} className="row">
                    <td className="col-sm-4">
                      {contentText.description}
                    </td>
                    <td className="col-sm-6">
                      <textarea
                        id={`content-text-${contentText.id}`}
                        defaultValue={contentText.text}
                        className="form-control"
                        style={{width: '100%', resize: 'none'}}
                        rows="5"
                      />
                    </td>
                    <td className="col-sm-2">
                      <button
                        className={`red button col-sm-4`}
                        onClick={() => this.handleUpdateContentText(contentText.id)}
                        disabled={this.state.idLoading}
                      >
                        { this.state.idLoading ?
                          'Procesando..' : 'Actualizar'
                        }
                      </button>
                    </td>
                  </tr>
                )
              }
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    updateContentTextAsync 
    }, dispatch)
  
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminContenido)