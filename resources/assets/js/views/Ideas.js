import React, { Component } from 'react';
import {connect} from 'react-redux';
import IdeasBlock from '../components/IdeasBlock'
import { bindActionCreators } from 'redux'
import { isEmpty } from 'lodash'

class Ideas extends Component {

    render() {
        const { ideasinnovador, ideassupervisor, auth } = this.props

        return (
            <div className="ideas-container">
                <div className="ideas-image mt-5 mb-5 text-center">
                    <img className="d-inline-block" src={assets + "/" + this.props.match.params.type  + ".png"} />
                </div>
                <div>
                    {  !isEmpty(auth.administrator) && auth.administrator[0].state ?
                        <IdeasBlock isOpen={true} categories={true} rows={ideassupervisor.filter((idea) => idea.state == this.props.match.params.type)} link="/mis-ideas/guardado" /> :
                        <IdeasBlock categories={true} isOpen={true} rows={ideasinnovador.filter((idea) => idea.state == this.props.match.params.type)} link="/mis-ideas/guardado" /> 
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { ideasinnovador } = state
    const { ideassupervisor } = state
    const { auth } = state
    return { ideasinnovador, ideassupervisor, auth }
}

export default connect(mapStateToProps, null)(Ideas)