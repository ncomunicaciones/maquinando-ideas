import React from 'react';
import { HashRouter as Router, Route,Switch, Link, Redirect, NavLink } from 'react-router-dom';
import {connect} from 'react-redux';
import { isEmpty } from 'lodash'
import { editNotificationAsync } from '../redux/asyncActions'
import {bindActionCreators} from 'redux'

class Notifications extends React.Component{

    state = {
        filter: null,
        load: false,
        param: !isEmpty(this.props.match.params)? this.props.match.params.filter : null
    }

    componentDidMount(){
        const getParams = this.state.param
        this.validate(getParams)
        this.setState({load : true})
    }

    validate(value){
        const getParams = value
        const options = ['todos', 'notificacion', 'chat']
        
        if(!isEmpty(getParams)){
            const validate = options.find((opt) => opt == getParams)
            if(isEmpty(validate)){
                this.setState({filter: 'todos'})
            } else{
                this.setState({filter: validate}, () => console.log(this.state.filter))
            }
        }else{
            this.setState({filter: 'todos'})
        }
    }

    componentDidUpdate(){
        if(!isEmpty(this.props.match.params.filter)){
            if(this.props.match.params.filter != this.state.param){
                this.setState({param: this.props.match.params.filter}, () => this.validate(this.state.param))
            }
        }
    }

    getError = (e, user) => {
        if(user.gender == 'm') e.target.src = assets + '/avatar.png'
        if(user.gender == 'f') e.target.src = assets + '/avatar-f.png'
    }

    callIdea = (e) => {
        this.props.editNotificationAsync(e)
    }

    changeFilter(e, filter){
        this.setState({filter}, () => console.log('ddd'))
    }

    getOpt = []

    render(){
        return(
            <div className="mis-ideas-container mt-5">
                
                <div className="notifications-container-page clearfix">
                    <div className="notifications-title mt-3">Notificaciones</div>
                    <div className="notifications-options-page">
                        <div ref={(e) => this.getOpt[0] = e}  onClick={(e) => this.changeFilter(e, 'todos')} className={this.state.filter != 'notificacion' && this.state.filter != 'chat'? "notifications-option-page active" : "notifications-option-page"}>Todos</div>
                        <div ref={(e) => this.getOpt[1] = e}  onClick={(e) => this.changeFilter(e, 'notificacion')} className={this.state.filter == 'notificacion'? "notifications-option-page active" : "notifications-option-page"}>Notificaciones</div>
                        <div ref={(e) => this.getOpt[2] = e}  onClick={(e) => this.changeFilter(e, 'chat')} className={this.state.filter == 'chat'? "notifications-option-page active" : "notifications-option-page"}>Mensajes</div>
                    </div>
                </div>
                {isEmpty(this.props.notifications) &&
                    <div className="mis-ideas-container-no-register">
                    No tienes ninguna notificación.
                    </div>
                }
                <div className="notifications-page-container">
                { this.props.notifications.filter(
                                        (notification) => this.state.filter == 'chat'? notification.type == 'chat' : this.state.filter == 'notificacion'?  notification.type != 'chat' : notification 
                                    ).map((notification, i) => 
                                        <Link key={i} to={"/"+notification.getLink} onClick={() => this.callIdea(notification.id)}>
                                            {notification.type != 'chat' &&
                                            <div className="notifications-box" >
                                                <div className="d-flex h-100  notifications-box-type-container align-items-center float-left">
                                                    <div className={!notification.read? "notifications-box-type green" : "notifications-box-type"} ></div>
                                                </div>
                                                <div className=" h-100  notifications-box-text-container float-left">
                                                    <span>{notification.title}</span>
                                                    <div className="notification-props">
                                                    {!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state?
                                                        <div className="notification-props-tag">
                                                            {!isEmpty(this.props.ideassupervisor) && !isEmpty(this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id)) && this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id).title}
                                                        </div>
                                                                :
                                                        <div className="notification-props-tag">
                                                            {!isEmpty(this.props.ideasinnovador) && !isEmpty(this.props.ideasinnovador.find((idea) => idea.id == notification.idea_id)) && this.props.ideasinnovador.find((idea) => idea.id == notification.idea_id).title}
                                                        </div> 
                                                    }
                                                    <div className={!notification.read? "notification-see-read" + " notification-props-tag notification-props-tag-see" : " " + " notification-props-tag notification-props-tag-see"}>
                                                        {notification.read? "Leído" : "No Leído"}
                                                    </div> 
                                                        <div className="notification-props-time">
                                                            {notification.time}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            }
                                            {notification.type == 'chat' &&
                                            <div className="notifications-box" >
                                                <div className="d-flex h-100  notifications-box-type-container align-items-center float-left">
                                                    <div className={!notification.read? "notifications-box-type green" : "notifications-box-type"} ></div>
                                                </div>
                                                <div className=" h-100  notifications-box-text-container float-left notifications-box-text-container-message">
                                                    <div className="image-box-noti">
                                                        <div>
                                                            <img className="image-box-noti-img" src={assets+'/avatars/'+ this.props.allUsers.find((user) => user.id == notification.provider).avatar} onError={(e) => this.getError(e, this.props.allUsers.find((user) => user.id == notification.provider))} />
                                                        </div>
                                                    </div>
                                                    <div className="image-box-desc">
                                                        <span>{notification.title}</span>
                                                        <div className="notification-props notification-props-message">
                                                            <div className="notification-props-time">
                                                                {notification.time}
                                                            </div>
                                                            {!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state?
                                                                    <div className="notification-props-tag">
                                                                        {!isEmpty(this.props.ideassupervisor) && 
                                                                            this.props.ideassupervisor.find((idea) => idea.id ==   notification.idea_id).user_id == notification.provider? 'Innovador' : !isEmpty(this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id)) && this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id).admin_id == notification.provider? 'Facilitador' : 'Ejecutor'}
                                                                    </div>
                                                                            :
                                                                    <div className="notification-props-tag">
                                                                        {!isEmpty(this.props.ideasinnovador) && 
                                                                            this.props.ideasinnovador.find((idea) => idea.id ==   notification.idea_id).user_id == notification.provider? 'Innovador' : this.props.ideasinnovador.find((idea) => idea.id ==   notification.idea_id).administratorcreator.id == notification.provider? 'Facilitador' : 'Ejecutor'}
                                                                    </div>
                                                            }
                                                            <div className="notification-props-tag notification-props-tag-last">
                                                                {notification.userProvider[0]}
                                                            </div>
                                                            <div className={!notification.read? "notification-see-read" + " notification-props-tag notification-props-tag-see" : " " + " notification-props-tag notification-props-tag-see"}>
                                                                {notification.read? "Leído" : "No Leído"}
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            }
                                        </Link>
                                    )}
                </div>
            </div>
        )
    }
}
    

const mapStateToProps = state => {
    const { auth } = state
    const { notifications } = state
    const { ideasinnovador } = state
    const { ideassupervisor } = state
    const allUsers = state.auth.allUsers
    return { auth,notifications,ideasinnovador,ideassupervisor, allUsers }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        editNotificationAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications)