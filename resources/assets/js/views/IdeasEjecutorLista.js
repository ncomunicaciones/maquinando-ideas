import React from 'react'
import { connect } from 'react-redux'
import IdeasBlock from '../components/IdeasBlock'

const IdeasEjecutorLista = ({ ideasinnovador, match }) =>
    <div className="ideas-container">
        <div className="ideas-image mt-5 mb-5 text-center">
            <img className="d-inline-block" src={assets + "/" + 'premiada'  + ".png"} />
        </div>
        <div>
            <IdeasBlock 
              categories={true} 
              rows={ideasinnovador.filter(idea => idea.operator == true)}
              isOpen={true}
              size={ideasinnovador.filter(idea => idea.operator == true).length}
              link="/mis-ideas/guardado"
            />
        </div>
    </div>


const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps, null)(IdeasEjecutorLista)