import React, {Component} from 'react'
import { connect } from 'react-redux'
import StatisticsTable from '../components/StatisticsTable'
import fuzzysearch from 'fuzzysearch'
import moment from 'moment'
import { isEmpty, isFinite, isEqual } from 'lodash'
import {excelIdeasAsync} from '../redux/asyncActions'
import { bindActionCreators } from 'redux';

class AdminStatistics extends Component{
    state = {
        statisticsFiltered: this.props.ideas,
        state: '',
        category: '',
        getStartMonth: '',
        getEndMonth: '',
        endMonth: [],
        load: false,
        filteredMonths: []
    }
    async componentDidMount(){
        moment.lang('es', {
                months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
                monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
                weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
                weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
                weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
            }
        );
        
        this.setState({load: true})
        this.filterAllChanges()
        setTimeout(
            () => {
                if(!isEqual(this.state.statisticsFiltered.sort(), this.props.ideas.sort())){
                    this.setState({statisticsFiltered: this.props.ideas})   
                }
            }, 2000
        )
    }
    changeFilterDate(e){
        const getVal = e.currentTarget.value
        this.setState({getStartMonth: getVal})
        const remove =  moment.months()
        remove.splice(0,getVal)
        this.setState({endMonth: !isEmpty(getVal) ? remove : [] })
        this.setState({filteredMonths: !isEmpty(getVal) ? remove : []},() => {this.filterAllChanges();})
    }

    changeFilterDate2(e){
        const getVal = e.currentTarget.value
        this.setState({getEndMonth: getVal})
        const allMonths = [0,1,2,3,4,5,6,7,8,9,10,11]
        allMonths.splice(0, this.state.getStartMonth)
        const removed = allMonths.splice((parseInt(getVal) + 1), (allMonths.length - (parseInt(getVal) + 1)))
        const filter = allMonths.filter(
            (num) => isEmpty(removed.find((rem) => rem == num))
        )
        const newFilter = filter.map(
            (filter) => moment().month(filter).format("MMMM")
        )
        this.setState({filteredMonths: newFilter}, () => {this.filterAllChanges()})
    }

    changeFilterState(e){
        const filter = e.currentTarget.getAttribute('type');
        const value = e.currentTarget.value
        this.setState({ state: filter == 'estado'? value : this.state.state, category: filter == 'categoria'? parseInt(value) : this.state.category },
        () => {
            this.filterAllChanges();
        }
        )
    }

    exportExcel(getNew){
        const ckm = {'ke': getNew.sort()}
        if(!isEmpty(getNew)){
            axios.post(axios_url+'/excelcookies', ckm)
                .then((succes) => console.log(succes))
        }
        
    }

    filterAllChanges(){
        const validateState = !isEmpty(this.state.state)
        const validateCategory = isFinite(this.state.category)
        const validateDates = !isEmpty(this.state.filteredMonths)

        const filteredUsers = this.props.ideas.filter(idea => {
            if(validateState && !validateCategory){
                return( fuzzysearch(this.state.state, idea.state) )
            }else if(validateCategory && !validateState){
                return( fuzzysearch(this.state.category, parseInt(idea.category_id)) )
            }else if(validateCategory && validateState){
                return( fuzzysearch(this.state.category, parseInt(idea.category_id)) && fuzzysearch(this.state.state, idea.state) )
            }
            return this.props.ideas
        })
        
        const newFiltered = filteredUsers.filter(
            (filter) => {
                return !isEmpty(this.state.filteredMonths.find((month) => moment(filter.created_at, 'YYYY/MM/DD').format('MMMM') == month ))
            }
        )

        this.setState({statisticsFiltered: validateDates? newFiltered : filteredUsers}, () => {
            const getNew = this.state.statisticsFiltered.map(
                (idea) => idea.id
            )
            this.exportExcel(getNew)
        })
    }

    render(){
        return this.state.load && (
            <div className="mt-5">
                <div className="title">{new Date().getFullYear()}</div>
                <div className="title">
                Estadísticas de ideas
                <a className="estadistica-title float-right" href={axios_url+"/exportexcel"} target="_blank" >Resumen</a>
                </div>
                <div className="row">
                    <div className="filtro-estadisticas">
                        <div className="float-left">
                            <div className="option">
                                <div className="titulo">
                                    Estado:
                                </div>
                                <select type="estado" onChange={(e) => this.changeFilterState(e)}>
                                    <option select value="">Elejir estado</option>
                                    <option value="guardado">Ideas guardadas</option>
                                    <option value="evaluacion">Ideas en evaluación</option>
                                    <option value="aprobado">Ideas aprobadas</option>
                                    <option value="desaprobado">Ideas desaprobadas</option>
                                    <option value="ejecucion">Ideas ejecutadas</option>
                                    <option value="premiada">Ideas premiadas</option>
                                </select>
                            </div>
                            <div className="option">
                                <div className="titulo">
                                    Categorías:
                                </div>
                                <select type="categoria" onChange={(e) => this.changeFilterState(e)}>
                                    <option select value="">Elegir categoría</option>
                                    {this.props.categories.map(
                                        (category, i) => <option key={i} value={category.id}>{ category.name }</option>
                                    )}
                                </select>
                            </div>
                        </div>
                        
                        <div className="float-right">
                            <div className="option">
                                <div className="titulo">
                                    Desde:
                                </div>
                                <select type="start" onChange={(e) => this.changeFilterDate(e)}>
                                    <option select value="">Elegir mes</option>
                                    
                                    {moment.months().map(
                                        (month, i) => <option key={i} value={i}>{ month }</option>
                                    )}
                                </select>
                            </div>
                            <div className="option no-option">
                                <div className="titulo">
                                    Hasta:
                                </div>
                                <select type="end" onChange={(e) => this.changeFilterDate2(e)}>
                                    <option select value="">Elegir mes</option>
                                    {this.state.endMonth.map(
                                        (month, i) => <option key={i} value={i}>{ month }</option>
                                    )}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="row p-0 m-0">
                    <StatisticsTable data={this.state.statisticsFiltered} categories={this.props.categories} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { ideas } = state
    const { categories } = state
    return { ideas, categories }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        excelIdeasAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminStatistics)