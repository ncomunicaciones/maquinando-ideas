import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'


class PrivateRoute extends Component {
    state = { isAllow: null }

    async componentDidMount() {
       try {
          const { data: isAllow } = await axios.get(axios_url+'/login/check')
          this.setState({ isAllow })
       } catch(err) {
          this.setState({ isAllow: 'error' })
       }
    }

    render() {
        const { component: Component } = this.props
        const { isAllow } = this.state
        
        if (isAllow === 'success') {
            return <Component />
            
        } else if (isAllow === 'error') {
            return <Redirect to="/login" />
        } else {
            return <div />
        }
    }

}

export default PrivateRoute