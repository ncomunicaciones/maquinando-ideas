import React from 'react';
import { HashRouter as Router, Route,Switch } from 'react-router-dom';
import AdminHome from '../views/AdminHome';
import Perfil from '../views/Perfil'
import Ideas from '../views/Ideas'
import Idea from '../views/Idea'
import Error404 from '../components/Error404'
import CambioTiempoIdea from '../views/CambioTiempoIdea'
import AdminCategorias from '../views/AdminCategorias'
import Notifications from '../views/Notifications'
import AdminContenido from '../views/AdminContenido'
import Requests from '../views/Requests'
import Users from '../views/Users'
import AdminStatistics from '../views/AdminStatistics';

const Administrador = () =>
    <Router>
        <Switch>
            <Route exact path="/" component={() => <AdminHome />} />
            <Route exact path="/idea/:state/:id" component={Idea} />
            <Route exact path="/perfil" component={Perfil} />
            <Route exact path="/notificaciones" component={Notifications} />
            <Route exact path="/notificaciones/:filter" component={Notifications} />
            <Route exact path="/solicitudes" component={Requests} />
            <Route exact path="/usuarios" component={Users} />
            <Route exact path="/estadisticas" component={AdminStatistics} />
            <Route exact path="/categorias/administrar" component={AdminCategorias} />
            <Route exact path="/categoria/administrar/crear" component={AdminCategorias} />
            <Route exact path="/categoria/administrar/:id" component={AdminCategorias} />
            <Route exact path="/categoria/administrar/:id" component={AdminCategorias} />
            <Route exact path="/usuarios/crear" component={Users} />
            <Route exact path="/usuarios/:type/:id" component={Users} />
            <Route exact path="/cambiar-tiempo/:id" component={CambioTiempoIdea} />
            <Route exact path="/contenido/administrar" component={AdminContenido} />
            <Route exact path="/ideas/:type" component={Ideas}  />
            <Route component={Error404} />
        </Switch>
    </Router>

export default Administrador