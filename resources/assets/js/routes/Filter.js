import React, {Component} from 'react';
import Navbar from '../containers/Navbar';
import Footer from '../containers/Footer';
import MenuMobile from '../containers/MenuMobile.js'
import { bindActionCreators } from 'redux'
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux'
import { getAuthUserAsync, getUserIdeasAsync, getCallContentTextsAsync, getCategoriesAsync, getAdminIdeasAsync, getAllUsersAsync, getRequestsAsync, getNotificationsAsync, excelIdeasAsync, changeStateAdminAsync, clearAdminIdeasAsync, clearInnovadorIdeasAsync, clearNotificationAsync, addAllChatsAsync, addAllChatsSupAsync, editResetIdeaAsync, editChatroomsAsync, addNotificationAsync, addInnovadorIdeaAdminAsync, editResetIdeaAdminAsync } from '../redux/asyncActions'
import Innovador from './Innovador'
import Administrador from './Administrador'
import LoadingApp from '../components/LoadingApp'
import {isEmpty, isNull} from 'lodash'
import api from '../redux/api'

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

class Home extends Component{
    state = {
        loadingPage: true,
        percentage: 0
    }
    // variables para intervals
    intNotifications = null
    intIdeas = null
    intSupervisorIdeas = null

    componentDidMount = async () => {
        await this.callApi()
        setTimeout( () => {this.setState({ loadingPage: false })}, 1800 )
    }

    async callApi(){
        await this.props.getAuthUserAsync()  // Obtener Usuario
        this.setState({percentage: 10})
        await this.props.getCallContentTextsAsync() // Obtener textos
        this.setState({percentage: 30})
        await this.props.getCategoriesAsync() // Obtener categorias
        this.setState({percentage: 40})
        await this.props.getNotificationsAsync() // Obtener notificaciones
        !isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state? this.validateSupervisor() : 
        this.validateInnovador()
        this.setState({percentage: 100})
    }

    async validateInnovador(){
        await this.props.getUserIdeasAsync() // Obtener Ideas Innovador
        await this.props.addAllChatsAsync() // Obtener chats
        this.intIdeas = setInterval(() => { this.setIntervalInnovador() }, 15000)
    }
    
    async validateSupervisor(){
        await this.props.getAdminIdeasAsync() // Obtener ideas de supervisor
        await this.props.getAllUsersAsync() // Obtener todos los usuarios
        await this.props.getRequestsAsync() // Obtener Peticiones
        await this.props.excelIdeasAsync() // Obtener todas las ideas
        await this.props.addAllChatsSupAsync()
        this.intIdeas = setInterval(() => { this.setIntervalSupervisor() }, 15000)
    }

    async setIntervalInnovador(){
        api.intervals.getNews().then((res) => {
            if(this.props.notifications.length != res.length){
                const saveNewData = res.map(
                    (val) => val.id
                )
                const newVals = this.props.notifications.map(
                    (val) => val.id
                )
                const getDifference = saveNewData.diff(newVals);
                const send = {};
                send.notificaciones = getDifference
                api.intervals.changeValues(send).then((res) => {
                    if(!isEmpty(res['chats'])){
                        res['chats'].map(
                            (chat) => chat.map((chat2) => this.props.editChatroomsAsync(chat2))
                        )
                    }
                    if(!isEmpty(res['ideas'])){
                        res['ideas'].map(
                            (idea) => {
                                if(!isEmpty(this.props.ideas.find((idea1) => idea1.id == idea.id))){
                                    this.props.editResetIdeaAsync(idea)
                                }else{
                                    this.props.addAllChatsAsync()
                                    this.props.editResetIdeaAsync(idea)
                                }
                            }
                        )
                    }
                })
                this.props.getAuthUserAsync()
                getDifference.map(
                    (data) => this.props.addNotificationAsync(res.find((data2) => data2.id == data))
                )
            }
        })
    }

    setIntervalSupervisor(){
        api.intervals.getNewsSup().then((res) => {
            if(this.props.notifications.length != res.length){
                const saveNewData = res.map(
                    (val) => val.id
                )
                const newVals = this.props.notifications.map(
                    (val) => val.id
                )
                const getDifference = saveNewData.diff(newVals);
                const send = {};
                send.notificaciones = getDifference
                api.intervals.changeValues(send).then((res) => {
                    if(!isEmpty(res['chats'])){
                        res['chats'].map(
                            (chat) => chat.map((chat2) => this.props.editChatroomsAsync(chat2))
                        )
                    }
                    if(!isEmpty(res['ideas'])){
                        res['ideas'].map(
                            (idea) => {
                                if(!isEmpty(this.props.ideas.find((idea1) => idea1.id == idea.id))){
                                    this.props.editResetIdeaAdminAsync(idea)
                                }else{
                                    this.props.addAllChatsSupAsync()
                                    this.props.addInnovadorIdeaAdminAsync(idea)
                                }
                            }
                        )
                    }
                    
                })
                this.props.getAuthUserAsync()
                getDifference.map(
                    (data) => this.props.addNotificationAsync(res.find((data2) => data2.id == data))
                )
            }else{
                console.log('nochanges')
            }
        })
    }

    clearSiteIntervals = () => {
        clearInterval(this.intIdeas)
        clearInterval(this.intSupervisorIdeas)
        clearInterval(this.intNotifications)
    }
    
    async clearStore(){
        await this.props.clearAdminIdeasAsync()
        await this.props.clearInnovadorIdeasAsync()
        await this.props.clearNotificationAsync()
    }

    async changeState(){
        if(this.props.auth.administrator[0].state == 1){
            await this.props.changeStateAdminAsync(0)
        }else{
            await this.props.changeStateAdminAsync(1)
        }
        this.setState({ loadingPage: true, percentage:0 })
        await this.clearSiteIntervals();
        await this.clearStore();
        this.props.history.push('/')
        await this.callApi();
        setTimeout( () => {this.setState({ loadingPage: false })}, 1800 )
    }
    
    async exit(){
        const resp = await axios.post(axios_url+'/user/logout')
        clearInterval(this.intIdeas)
        clearInterval(this.intSupervisorIdeas)
        localStorage.removeItem('userState')
    }
    
    
    render() {
        return (
            <div>
                <LoadingApp onTransitionEnd={this.transitionEnd} mounted={this.state.loadingPage} percentage={this.state.percentage} />
                { !this.state.loadingPage ?
                    <div>
                        <MenuMobile user={this.props.auth} />
                        <div className="close-mobile"></div>
                        <Navbar
                            exit={() => this.exit()}
                            user={this.props.auth}
                            changeStateFun={() => this.changeState()}
                            type={!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state ? 'administrator' : 'user'}
                            clearSiteIntervals={this.clearSiteIntervals}
                        />

                        <div id="body" className="container">
                            {!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state ? <Administrador /> : <Innovador /> }
                            <Footer />
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { categories, auth, ideas, userstate, notifications } = state;
    return {
      categories, auth, ideas, userstate, notifications
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return (
      bindActionCreators({
          getCallContentTextsAsync,
          getAuthUserAsync,
          getUserIdeasAsync,
          getCategoriesAsync,
          getRequestsAsync,
          getAdminIdeasAsync,
          getAllUsersAsync,
          getNotificationsAsync,
          excelIdeasAsync,
          changeStateAdminAsync,
          clearAdminIdeasAsync,
          clearInnovadorIdeasAsync,
          clearNotificationAsync,
          addAllChatsAsync,
          addAllChatsSupAsync,
          editResetIdeaAsync,
          editChatroomsAsync,
          addNotificationAsync,
          addInnovadorIdeaAdminAsync,
          editResetIdeaAdminAsync
        }, dispatch)
    )
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home))