import React from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import SelectCategory from '../views/SelectCategory';
import Perfil from '../views/Perfil';
import Ideas from '../views/Ideas';
import MisIdeas from '../views/MisIdeas';
import Idea from '../views/Idea'
import Error404 from '../components/Error404'
import Notifications from '../views/Notifications';
import IdeasEjecutor from './../views/IdeasEjecutor';
import IdeasEjecutorLista from '../views/IdeasEjecutorLista';

const Innovador = () =>
    <Router>
        <Switch>
            <Route exact path="/idea/:state/:id" component={Idea} />
            <Route exact path="/" component={SelectCategory} />
            <Route exact path="/perfil" component={Perfil} />
            <Route exact path="/notificaciones" component={Notifications} />
            <Route exact path="/notificaciones/:filter" component={Notifications} />
            <Route exact path="/mis-ideas" component={MisIdeas} />
            <Route exact path="/mis-ideas-ejecutor" component={IdeasEjecutor} />
            <Route exact path="/mis-ideas-ejecutor/:type" component={IdeasEjecutorLista} />
            <Route exact path="/mis-ideas/:type" component={Ideas} />
            <Route component={Error404} />
        </Switch>
    </Router>

export default Innovador;