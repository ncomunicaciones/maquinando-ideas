import { getAllUsers, createUser, editUser } from './ducks/users';
import { getUser, editAuth } from './ducks/auth'
import { addAllNotification, editNotification, clearNotification, addNotification } from './ducks/notifications';
import { addAllInnovadorIdeas, addInnovadorIdea, editInnovadorIdea, clearInnovadorIdeas } from './ducks/ideasinnovador'
import { editAdminIdea, clearAdminIdeas, addAdminIdea } from './ducks/ideassupervisor'
import { addAllCategories, addCategory, editCategory } from './ducks/categories'
import { getTextsContent, updateTextContent } from './ducks/contenttext'
import { addAllRequests, editRequests } from './ducks/requests'
import { addAllAdminIdeas } from './ducks/ideassupervisor'
import { addAllIdeas } from './ducks/ideas'
import {addAllChatrooms, editChatrooms} from './ducks/chatsrooms'
import api from './api'


/* 
    Innovador
*/

// Get User Auth
const getAuthUserAsync = () => {
    return api.user.getAuthUser().then((res) => getUser(res))
}
// Edit Auth
const editAuthAsync = (data) => {
    return api.user.editAuthUser(data).then((res) => editAuth(res))
}
// Change state
const changeStateAdminAsync = (data) => {
    return api.user.changeStateAdmin(data).then((res) => editAuth(res))
}

// Get Notifications
const getNotificationsAsync = () => {
    return api.notificaciones.getNotifications().then((res) => addAllNotification(res))
}
// Edit Notification
const editNotificationAsync = (e) => {
    return api.notificaciones.editNotification(e).then((res) => editNotification(res))
}

/* Ideas CRUD */
//Get all ideas
const getUserIdeasAsync = () => {
    return api.ideas.getUserIdeas().then((res) => addAllInnovadorIdeas(res))
}
//Add Idea
const addInnovadorIdeaAsync = (data) => {
    return api.idea.addInnovadorIdea(data).then((res) => addInnovadorIdea(res))
}

//Edit Idea
const editInnovadorIdeaAsync = (data, id) => {
    return api.idea.editInnovadorIdea(data, id).then((res) => editInnovadorIdea(res))
}
//Edit Idea
const editResetIdeaAsync = (data) => {
    return editInnovadorIdea(data)
}
//Evaluate Idea
const evaluateInnovadorIdeaAsync = (id) => {
    return api.idea.evaluateInnovadorIdea(id).then((res) => editInnovadorIdea(res))
}
//Aprove Idea
const passEvaluateInnovadorIdeaAsync = (data, id) => {
    return api.idea.passEvaluateInnovadorIdea(data, id).then((res) => editAdminIdea(res))
}
//Idea Time
const setExecuteTimeAsync = (data, id) => {
    return api.idea.setExecuteTime(data, id).then((res) => editAdminIdea(res))
}
//Execute Idea
const passInnovadorIdeaToExecuteAsync = (data, id) => {
    return api.idea.passInnovadorIdeaToExecute(data, id).then((res) => editAdminIdea(res))
}
//Finish Idea
const finishIdeaExecuteAsync = (id) => {
    return api.idea.finishIdeaExecute(id).then((res) => editAdminIdea(res))
}
//Set State time
const setStateTimeAsync = (id, path, data) => {
    return api.idea.setStateTime(id, path, data).then((res) => editAdminIdea(res))
}


// Save executors
const saveOperatorsAsync = (data, id) => {
    return api.operator.saveOperators(data, id).then((res) => editAdminIdea(res))
}

// Get Text
const getCallContentTextsAsync = () => {
    return api.contentText.getContentTexts().then((res) => getTextsContent(res))
}
// Get Categories
const getCategoriesAsync = () => {
    return api.categorias.getCategories().then((res) => addAllCategories(res))
}

/* 
    Supervisor
*/

// Get Admin Ideas
const getAdminIdeasAsync = () => {
    return api.ideas.getAdminIdeas().then((res) => addAllAdminIdeas(res))
}
// Get Users
const getAllUsersAsync = () => {
    return api.user.getAllUsers().then((res) => getAllUsers(res))
}
// Get Requests
const getRequestsAsync = () => {
    return api.requests.getRequests().then((res) => addAllRequests(res))
}
// Update Request
const updateRequestsAsync = (res) => {
    return api.requests.updateRequests(res).then((res) => editRequests(res))
}
// Get All ideas
const excelIdeasAsync = () => {
    return api.ideas.excelIdeas().then((res) => addAllIdeas(res))
}


/* 
    CATEGORIAS
*/
const addCategoryAsync = (data) => {
    return api.categorias.addCategory(data).then((res) => addCategory(res))
}
const editCategoryAsync = (data) => {
    return api.categorias.editCategory(data).then((res) => editCategory(res))
}
const disableCategoryAsync = (data) => {
    return api.categorias.disableCategory(data).then((res) => editCategory(res))
}


/* 
    Usuario
*/
const addUserAsync = (data) => {
    return api.users.addUser(data).then((res) => createUser(res))
}
const editUserAsync = (data) => {
    return api.users.editUser(data).then((res) => editUser(res))
}
const disableUserAsync = (data) => {
    return api.users.stateUser(data).then((res) => editUser(res))
}

/* 
    Textos
*/
const updateContentTextAsync = (data, id) => {
    return api.contentText.updateContentText(data, id).then((res) => updateTextContent(res))
}


// Clear Ideas
const clearAdminIdeasAsync = () => {
    return clearAdminIdeas()
}
const clearInnovadorIdeasAsync = () => {
    return clearInnovadorIdeas()
}
const clearNotificationAsync = () => {
    return clearNotification()
}


// Chats
const addAllChatsAsync = () => {
    return api.chats.addAllChats().then((res) => addAllChatrooms(res))
}
const addAllChatsSupAsync = () => {
    return api.chats.addAllChatsSup().then((res) => addAllChatrooms(res))
}
const sendChatAsync = (id, res, type) => {
    return api.chats.sendChat(id, res, type).then((res) => editChatrooms(res))
}
const editChatroomsAsync = (data) => {
    return editChatrooms(data)
}

const addNotificationAsync = (data) => {
    return addNotification(data)
}

//Add Idea
const addInnovadorIdeaAdminAsync = (data) => {
    return addAdminIdea(data)
}
//Edit Idea
const editResetIdeaAdminAsync = (data) => {
    return editAdminIdea(data)
}




export { addInnovadorIdeaAdminAsync, editResetIdeaAdminAsync, getAuthUserAsync, addNotificationAsync, sendChatAsync, editChatroomsAsync, addAllChatsSupAsync, addAllChatsAsync, getUserIdeasAsync, getCallContentTextsAsync, getCategoriesAsync, getAdminIdeasAsync, getAllUsersAsync, getRequestsAsync, getNotificationsAsync, excelIdeasAsync, addInnovadorIdeaAsync, editInnovadorIdeaAsync, editResetIdeaAsync, evaluateInnovadorIdeaAsync, passEvaluateInnovadorIdeaAsync, passInnovadorIdeaToExecuteAsync, saveOperatorsAsync, setExecuteTimeAsync, finishIdeaExecuteAsync, addCategoryAsync, editCategoryAsync, disableCategoryAsync, updateContentTextAsync, addUserAsync, editUserAsync, editAuthAsync, editNotificationAsync, changeStateAdminAsync, clearAdminIdeasAsync, clearInnovadorIdeasAsync, clearNotificationAsync, setStateTimeAsync, disableUserAsync, updateRequestsAsync }
