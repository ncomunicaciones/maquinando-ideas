const api = {
    user: {
        async getAuthUser() {
            let { data } = await axios.get(axios_url+'/auth-user')
            return data
        },
        async editAuthUser(res){
            let { data } = await axios.post(axios_url+'/user/update', res)
            return data
        },
        async getAllUsers() {
            let { data } = await axios.get(axios_url+'/allusers')
            return data
        },
        async changeStateAdmin(id) {
            let { data } = await axios.get(axios_url+'/change-role/'+id)
            return data
        }
    },
    requests: {
        async getRequests(){
            let { data } = await axios.get(axios_url+'/requests')
            return data
        },
        async updateRequests(res){
            let { data } = await axios.post(axios_url+'/request/update', res)
            return data
        }
    },
    ideas: {
        async getUserIdeas(){
            let { data } = await axios.get(axios_url+'/auth-ideas')
            return data
        },
        async getAdminIdeas(){
            let { data } = await axios.get(axios_url+'/supervisor-ideas')
            return data
        },
        async excelIdeas(){
            let {data} = await axios.get(axios_url+'/excelideas')
            return data
        },

    },
    idea:{
        async addInnovadorIdea(res){
            let {data} = await axios.post(axios_url+'/ideas/store', res)
            return data
        },
        async editInnovadorIdea(res, id){
            let {data} = await axios.post(axios_url+'/ideas/'+ id +'/update', res)
            return data
        },
        async evaluateInnovadorIdea(id){
            let {data} = await axios.get(axios_url+'/ideas/'+ id +'/evaluate')
            return data
        },
        async passEvaluateInnovadorIdea(res, id){
            let {data} = await axios.post(axios_url+'/ideas/'+ id +'/evaluateedit', res)
            return data
        },
        async setExecuteTime(res, id) {
            let {data} = await axios.post(axios_url + '/ejecutar_idea/'+ id +'/edit', res)
            return data
        },
        async passInnovadorIdeaToExecute(res, id){
            let {data} = await axios.post(axios_url+'/ideas/execute_idea/'+ id , res)
            return data
        },
        async finishIdeaExecute(id) {
            let {data} = await axios.post(axios_url +'/ideas/finish_execute_idea/'+ id)
            return data
        },
        async setStateTime(id, path, res) {
            let {data} = await axios.post(axios_url + '/'  + path +  '/' +  id + '/edit', res)
            return data
        }
    },
    operator: {
        async saveOperators(res, id) {
            let {data} = await axios.post(axios_url+ '/ideas/save_operators/' + id, res)
            return data
        }
    },
    categorias: {
        async getCategories() {
            let { data } = await axios.get(axios_url+'/categorias')
            return data
        },
        async addCategory(res) {
            let { data } = await axios.post(axios_url+'/createcategory', res)
            return data
        },
        async editCategory(res) {
            let { data } = await axios.post(axios_url+'/editcategory', res)
            return data
        },
        async disableCategory(res) {
            let {data} = await axios.post(axios_url+'/changecategory', res)
            return data
        }
    },
    notificaciones: {
        async getNotifications() {
            let { data } = await axios.get(axios_url+'/notificaciones')
            return data
        },
        async editNotification(id){
            let { data } = await axios.get(axios_url+'/notificaciones/leer/'+ id)
            return data
        }
    },
    contentText: {
        async getContentTexts() {
          const { data: contentTexts } = await axios.get(`${axios_url}/content_text/retrieve`)
          return contentTexts
        },
        async updateContentText(id, res) {
          const { data } = await axios.post(axios_url +'/content_text/update/'+ id, res)
          return data
        }
    },
    users: {
        async addUser(res){
            const { data } =  await axios.post(axios_url +'/user/create', res)
            return data
        },
        async editUser(res){
            const { data } = await axios.post(axios_url + '/edituser', res)
            return data
        },
        async stateUser(res){
            const { data } = await axios.post(axios_url+'/changeuser', res)
            return data
        }
    },
    chats: {
        async addAllChats(){
            const {data} = await axios.get(axios_url + '/getchatsall')
            return data
        },
        async addAllChatsSup(){
            const {data} = await axios.get(axios_url + '/getchatsallsup')
            return data
        },
        async sendChat(id, res, type){
            const {data} = await axios.post(axios_url+'/sendchat/'+id+'/'+type, res)
            return data
        }
    },
    intervals: {
        async getNews() {
          const { data } = await axios.get(`${axios_url}/seechanges`)
          return data
        },
        async getNewsSup() {
            const { data } = await axios.get(`${axios_url}/seechangessup`)
            return data
          },
        async changeValues(res) {
            const {data} = await axios.post(axios_url+'/changenoti', res)
            return data
        }
    }
}

export default api
