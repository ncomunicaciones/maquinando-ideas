import { createDuck } from 'redux-duck'

const duck = createDuck('ideassupervisor')

const ADD_ALL_IDEAS_SUPERVISOR = duck.defineType('ADD_ALL_IDEAS_SUPERVISOR');
const CLEAR_IDEAS_SUPERVISOR = duck.defineType('CLEAR_IDEAS_SUPERVISOR');
const ADD_IDEA_SUPERVISOR = duck.defineType('ADD_IDEA_SUPERVISOR');
const EDIT_IDEA_SUPERVISOR = duck.defineType('EDIT_IDEA_SUPERVISOR');

export const addAllAdminIdeas = duck.createAction(ADD_ALL_IDEAS_SUPERVISOR)
export const clearAdminIdeas = duck.createAction(CLEAR_IDEAS_SUPERVISOR)
export const addAdminIdea = duck.createAction(ADD_IDEA_SUPERVISOR)
export const editAdminIdea = duck.createAction(EDIT_IDEA_SUPERVISOR)

const initialState = []

export default duck.createReducer({
    [ADD_ALL_IDEAS_SUPERVISOR]: (state, {payload}) => {
       return payload
    },
    [ADD_IDEA_SUPERVISOR]: (state, {payload}) => {
        const search = state.findIndex(idea => idea.id === payload.id)
        return state.length == 0? [payload, ...state] : search === -1? [payload, ...state] : state
    },
    [EDIT_IDEA_SUPERVISOR]: (state, {payload}) => {
        const newIdeas = state.filter((idea) => idea.id != payload.id)
        return [ payload, ...newIdeas ]
    },
    [CLEAR_IDEAS_SUPERVISOR]: (state, {payload}) => {
        return []
    }
}, initialState)
