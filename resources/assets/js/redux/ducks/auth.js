import { createDuck } from 'redux-duck'

const duck = createDuck('auth')

const GET_USER = duck.defineType('GET_USER')
const EDIT_AUTH = duck.defineType('GET_AUTH')

export const getUser = duck.createAction(GET_USER)
export const editAuth = duck.createAction(EDIT_AUTH)

let initialState = {}

export default duck.createReducer({
    [GET_USER] : (state, {payload}) => {
        return payload
    },
    [EDIT_AUTH] : (state, {payload}) => {
        return Object.assign({}, state, payload)
    }
}, initialState)