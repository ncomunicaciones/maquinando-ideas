import { createDuck } from 'redux-duck'

//Creamos un duck
const duck = createDuck('categories')

//Definimos las constantes
const ADD_ALLCATEGORIES = duck.defineType('ADD_ALLCATEGORIES')
const ADD_CATEGORY = duck.defineType('ADD_CATEGORY')
const EDIT_CATEGORY = duck.defineType('EDIT_CATEGORY')
const CLEAR_CATEGORIES = duck.defineType('CLEAR_CATEGORIES')

export const addAllCategories = duck.createAction(ADD_ALLCATEGORIES)
export const addCategory = duck.createAction(ADD_CATEGORY)
export const clearCategories = duck.createAction(CLEAR_CATEGORIES)
export const editCategory = duck.createAction(EDIT_CATEGORY)

const initialState = []

export default duck.createReducer({
    [ADD_ALLCATEGORIES]: (state, {payload}) => {
        return payload
    },
    [ADD_CATEGORY]: (state, {payload}) => {
        return [...state, payload]
    },
    [EDIT_CATEGORY]: (state, {payload}) => {
        return state.map(category => {
            return category.id === payload.id ?
            Object.assign({}, category, payload) : 
            category
        })
    },
    [CLEAR_CATEGORIES]: (state, {payload}) => {
        return []
    }
}, initialState)