import { createDuck } from 'redux-duck'

let duck = createDuck('ideas')

const ADD_ALLIDEAS = duck.defineType('ADD_ALLIDEAS')

export const addAllIdeas = duck.createAction(ADD_ALLIDEAS)

let initialState = []

export default duck.createReducer({
    [ADD_ALLIDEAS] : (state, {payload}) => {
        return payload
    }
}, initialState)