import { createDuck } from 'redux-duck'

const duck = createDuck('ideasinnovador')

const ADD_ALL_IDEAS_INNOVADOR = duck.defineType('ADD_ALL_IDEAS_INNOVADOR');
const CLEAR_IDEAS_INNOVADOR = duck.defineType('CLEAR_IDEAS_INNOVADOR');
const ADD_IDEA_INNOVADOR = duck.defineType('ADD_IDEA_INNOVADOR');
const EDIT_IDEA_INNOVADOR = duck.defineType('EDIT_IDEA_INNOVADOR');

export const addAllInnovadorIdeas = duck.createAction(ADD_ALL_IDEAS_INNOVADOR)
export const clearInnovadorIdeas = duck.createAction(CLEAR_IDEAS_INNOVADOR)
export const addInnovadorIdea = duck.createAction(ADD_IDEA_INNOVADOR)
export const editInnovadorIdea = duck.createAction(EDIT_IDEA_INNOVADOR)

const initialState = []

export default duck.createReducer({
    [ADD_ALL_IDEAS_INNOVADOR]: (state, {payload}) => {
       return payload
    },
    [ADD_IDEA_INNOVADOR]: (state, {payload}) => {
        const search = state.findIndex(idea => idea.id === payload.id)
        return state.length == 0? [payload, ...state] : search === -1? [payload, ...state] : state
    },
    [EDIT_IDEA_INNOVADOR]: (state, {payload}) => {
        const newIdeas = state.filter((idea) => idea.id != payload.id)
        return [ payload, ...newIdeas ]
    },
    [CLEAR_IDEAS_INNOVADOR]: (state, {payload}) => {
        return []
    }
}, initialState)
