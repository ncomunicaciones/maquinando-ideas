import { createDuck } from 'redux-duck'

const duck = createDuck('requests')

const ADD_ALLREQUESTS = duck.defineType('ADD_ALLREQUESTS')
const EDIT_REQUESTS = duck.defineType('EDIT_REQUESTS')

export const addAllRequests = duck.createAction(ADD_ALLREQUESTS)
export const editRequests = duck.createAction(EDIT_REQUESTS)

let initialState = []

export default duck.createReducer({
    [ADD_ALLREQUESTS] : (state, {payload}) => {
        return payload
    },
    [EDIT_REQUESTS] : (state, {payload}) => {
        return state.map(request => {
            return request.id === payload.id?
            Object.assign({}, request, payload) :
            request
        })
    }
}, initialState)