import {combineReducers} from 'redux'
import categories from './categories'
import contenttext from './contenttext'
import notifications from './notifications'
import auth from './auth'
import ideasinnovador from './ideasinnovador'
import ideassupervisor from './ideassupervisor'
import requests from './requests'
import ideas from './ideas'
import users from './users'
import chatsrooms from './chatsrooms'

const reducers = combineReducers({
    categories,
    contenttext,
    notifications,
    ideasinnovador,
    ideassupervisor,
    requests,
    auth,
    ideas,
    users,
    chatsrooms
})

export default reducers