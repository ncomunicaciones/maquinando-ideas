import { createDuck } from 'redux-duck'

const duck = createDuck('contenttext')

const GET_TEXTS_CONTENT = duck.defineType('GET_TEXTS_CONTENT')
const UPDATE_TEXT_CONTENT = duck.defineType('UPDATE_TEXT_CONTENT')

export const getTextsContent = duck.createAction(GET_TEXTS_CONTENT)
export const updateTextContent = duck.createAction(UPDATE_TEXT_CONTENT)

let initialState = []

export default duck.createReducer({
    [GET_TEXTS_CONTENT]: (state, {payload}) => {
        return payload
    },
    [UPDATE_TEXT_CONTENT]: (state, {payload}) => {
        return state.map(text => {
            return text.id === payload.id ?
            Object.assign({}, text, payload) : 
            text
        })
    }
}, initialState)