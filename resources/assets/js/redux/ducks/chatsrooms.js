import { createDuck } from 'redux-duck'

const duck = createDuck('chatsrooms')

const ADD_ALL_CHATSROOMS = duck.defineType('ADD_ALL_CHATSROOMS');
const CLEAR_ALL_CHATSROOMS = duck.defineType('CLEAR_ALL_CHATSROOMS');
const ADD_CHATSROOMS = duck.defineType('ADD_CHATSROOMS');
const EDIT_CHATSROOMS = duck.defineType('EDIT_CHATSROOMS');

export const addAllChatrooms = duck.createAction(ADD_ALL_CHATSROOMS)
export const clearAllChatrooms = duck.createAction(CLEAR_ALL_CHATSROOMS)
export const addChatrooms = duck.createAction(ADD_CHATSROOMS)
export const editChatrooms = duck.createAction(EDIT_CHATSROOMS)

const initialState = []

export default duck.createReducer({
    [ADD_ALL_CHATSROOMS]: (state, {payload}) => {
       return payload
    },
    [ADD_CHATSROOMS]: (state, {payload}) => {
        const search = state.findIndex(idea => idea.id === payload.id)
        return state.length == 0? [payload, ...state] : search === -1? [payload, ...state] : state
    },
    [EDIT_CHATSROOMS]: (state, {payload}) => {
        return state.map(chatroom => {
            return chatroom.id === payload.id?
            Object.assign({}, chatroom, payload) :
            chatroom
        })
    },
    [CLEAR_ALL_CHATSROOMS]: (state, {payload}) => {
        return []
    }
}, initialState)
