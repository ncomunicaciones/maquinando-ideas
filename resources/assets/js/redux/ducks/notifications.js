import { createDuck } from 'redux-duck'

const duck = createDuck('notifications')

const ADD_NOTIFICATION = duck.defineType('ADD_NOTIFICATION')
const ADD_ALLNOTIFICATIONS = duck.defineType('ADD_ALLNOTIFICATION')
const EDIT_NOTIFICATION = duck.defineType('EDIT_NOTIFICATION')
const CLEAR_NOTIFICATIONS = duck.defineType('CLEAR_NOTIFICATIONS')

export const addNotification = duck.createAction(ADD_NOTIFICATION)
export const addAllNotification = duck.createAction(ADD_ALLNOTIFICATIONS)
export const editNotification = duck.createAction(EDIT_NOTIFICATION)
export const clearNotification = duck.createAction(CLEAR_NOTIFICATIONS)

let initialState = []

export default duck.createReducer({
    [ADD_ALLNOTIFICATIONS]: (state, {payload}) => {
        return payload
    },
    [ADD_NOTIFICATION]: (state, {payload}) => {
        return [payload, ...state]
    },
    [EDIT_NOTIFICATION]: (state, {payload}) => {
        return state.map(noti => {
            return noti.id === payload.id?
            Object.assign({}, noti, payload) :
            noti
        })
    },
    [CLEAR_NOTIFICATIONS]: (state, {payload}) => {
        return []
    },

}, initialState)
