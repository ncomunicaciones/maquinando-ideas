import { createDuck } from 'redux-duck'

const duck = createDuck('users')

const GET_ALL_USERS = duck.defineType('GET_ALL_USERS')
const CREATE_USER = duck.defineType('CREATE_USER')
const EDIT_USER = duck.defineType('EDIT_USER')

export const getAllUsers = duck.createAction(GET_ALL_USERS)
export const createUser = duck.createAction(CREATE_USER)
export const editUser = duck.createAction(EDIT_USER)

let initialState = []

export default duck.createReducer({
    [GET_ALL_USERS]: (state, {payload}) => {
        return payload
    },
    [CREATE_USER]: (state, {payload}) => {
        return [...state, payload]
    },
    [EDIT_USER]: (state, {payload}) => {
        return state.map(user => {
            return user.id === payload.id?
            Object.assign({}, user, payload) :
            user
        })
    }
}, initialState)

