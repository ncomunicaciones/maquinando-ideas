import { createStore, applyMiddleware } from 'redux'
import reducers from './ducks'
import thunk from 'redux-thunk';
import promise from 'redux-promise'
import { composeWithDevTools } from 'redux-devtools-extension';
import {loadState, saveState} from './localStorage'

const persistedState = loadState();

const store = createStore(
    reducers,
    persistedState,
    composeWithDevTools(applyMiddleware(thunk, promise))
    )
    
store.subscribe(() => {
    saveState(store.getState());
})

export default store