import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import {Link} from 'react-router-dom';
import {formatDate} from '../helpers/utils.js';
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'

class IdeasIcons extends Component{
    isUser = () => isEmpty(this.props.user.administrator) || this.props.userstate.data

    getIdeaLink = ideaState => {
        if (this.isUser()) {
            switch(ideaState) {
                case 'evaluacion':
                    return `/idea-evaluar/${this.props.id}`
                case 'ejecucion':
                    return `/idea-ejecutar/${this.props.id}`
                case 'premiada':
                    return `/idea-premio/${this.props.id}`
                default:
                    return `/idea/${this.props.id}`
            }
        } else {
            switch(ideaState) {
                case 'evaluacion':
                    return `/idea-evaluar/${this.props.id}`
                case 'ejecucion':
                    return `/idea-ejecutar/${this.props.id}`
                case 'premiada':
                    return `/idea/premiacion/${this.props.id}`
                default:
                    return `/idea/${this.props.id}`
            }
        }
    }

    render(){
        var icons = this.props.icons;
        var state;
        let is_executed_idea = false

        if (typeof this.props.data.idea_execute !== 'undefined' && this.props.data.idea_execute.length) {
            const startDate = new Date(this.props.data.idea_execute[0].start_day)
            const currentDate = new Date()
            const timeDiff = Math.abs(currentDate.getTime() - startDate.getTime())
            const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1
            is_executed_idea = this.props.data.idea_execute[0].execute_time - diffDays

            if(this.props.data.idea_execute[0].execute_time > 66){state = 'green'}
            else if(this.props.data.idea_execute[0].execute_time > 33){state= 'yellow'}
            else if(this.props.data.idea_execute[0].execute_time > 0){state = 'red'}
        } else {
            if(this.props.data.state_time > 66){state = 'green'}
            else if(this.props.data.state_time > 33){state= 'yellow'}
            else if(this.props.data.state_time > 0){state = 'red'}
        }

        const evaluado = this.props.data.idea_evaluate_create? " " +"Evaluado: " + formatDate(this.props.data.idea_evaluate_create.date) : null
        const aprobado = this.props.data.idea_aprobed_create? " " + "Aprobado: " + formatDate(this.props.data.idea_aprobed_create.date)  : ' '
        const ejecutado = this.props.data.idea_execute_create? "  " + "Ejecutado: " +    formatDate(this.props.data.idea_execute_create.date) : ' '
        const desaprobado = this.props.data.idea_disapproved_create? " " + "Desaprobado: " + formatDate(this.props.data.idea_disapproved_create.date) : ' '
        const abortado = this.props.data.idea_abortado_create?  " " + "Evaluado: " + this.props.data.idea_evaluate_create.date : ' '
        const premiada = this.props.data.finish?  " " + "Premiada: " + formatDate(this.props.data.finish) : ' '
        const answer = ("Creado: " + formatDate(this.props.data.created_at) + evaluado + aprobado + ejecutado + desaprobado + abortado + premiada).trim()
        const link = this.props.data.state == 'guardado'?
                    '/idea/guardado/' : this.props.data.state == 'evaluacion' || this.props.data.state == 'aprobado' || this.props.data.state == 'desaprobado'?
                    '/idea/evaluacion/' : this.props.data.state == 'ejecucion'?
                    '/idea/ejecucion/': this.props.data.state == 'premiada'?
                    '/idea/premiada/' : null
        return(
            <div>
                <Link to={link+this.props.id}>
                    <div className="table-ideas-icon table-ideas-icon-link">
                        <FontAwesome name="search" />
                        <div className="table-ideas-icon-tooltip">Detalle de la idea</div>
                    </div>
                </Link>
                <div data-html="true" className="table-ideas-icon">
                    <FontAwesome name="clock-o" />
                    <div className="table-ideas-icon-tooltip">{this.props.data.date_start}</div>
                </div>
                {evaluado &&
                    <div
                        className="table-ideas-icon">
                        <FontAwesome name="calendar" />
                        <div className="table-ideas-icon-tooltip">
                        {answer}
                        </div>
                    </div>
                }
                {this.props.data.date_difference && this.props.data.state != 'premiada' && this.props.data.state != 'ejecucion' &&
                        <div
                        data-html="true"
                        data-toggle="tooltip"
                        className={"table-ideas-circle-state  table-ideas-icon " + state}>
                            <div className="table-ideas-icon-tooltip">
                                {"La evaluación terminará en "+this.props.data.date_difference+" dias."}
                            </div>
                        </div>
                }
                {is_executed_idea && this.props.data.state == 'ejecucion' &&
                        <div
                        data-html="true"
                        data-toggle="tooltip"
                        className={"table-ideas-circle-state  table-ideas-icon " + state}>
                            <div className="table-ideas-icon-tooltip">
                                {"La ejecución terminará en "+is_executed_idea+" dias."}
                            </div>
                        </div>
                }
            </div>
        );
    }

}

const mapStateToProps = state => {
    const { user, userstate } = state;
    return {
      user, userstate
    }
}


export default connect(mapStateToProps, null)(IdeasIcons)
