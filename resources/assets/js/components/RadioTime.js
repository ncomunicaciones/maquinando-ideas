import React from 'react'

function RadioTime({ time, setTimeOption, isSelectedClass }) {
  return (
    <div
        className="form-check form-check-inline"
    >
        <input
            className="form-check-input hidden square"
            type="radio"
            name="timeOption"
            value={time}
            id={`time-${time}`}
            onChange={setTimeOption}
        />
        <label
            className={isSelectedClass}
            htmlFor={`time-${time}`}
        >
            {time} días
        </label>
    </div>
  )
}

export default RadioTime