import React from 'react';

const BarIdea = ({alltime, time}) => 
    <div className="bar-ideas">
        <div className="bar-ideas-transcurrido" style={{ width: alltime >= time ? '100%' : parseInt(time*100/alltime) + '%' }}>
        </div>
    </div>

export default BarIdea;