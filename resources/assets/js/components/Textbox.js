import React, { Component } from 'react'

export default class Textbox extends Component {

    validInputProps = () => {
        let inputProps = {}

        if (this.props.value) {
            inputProps.value = this.props.value
        }
        inputProps.defaultValue = this.props.defaultValue
        inputProps.ref = this.props.refInput
        inputProps.type = this.props.type
        inputProps.name = this.props.name
        inputProps.onChange = this.props.handleChange
        inputProps.onFocus = this.props.onFocus
        inputProps.disabled = this.props.disabled
        inputProps.pattern = this.props.pattern
        if (this.props.readOnly) {
            inputProps.readOnly = 'readonly'
        }

        return inputProps
    }

    render() {

        const invalidClass = this.props.errors ? 'is-invalid' : ''
        const hidde = this.props.hiddeme? 'd-none': ''
        const valueset = this.props.incluseVal? '' : ''
        const macomp = this.props.classNameOP
        return (
            <div className={`materialize-component ${invalidClass} ${macomp} ${hidde}` }>
                <div className="group">
                    { !this.props.incluseVal?
                        <input autoComplete="off" formNoValidate
                        {...this.validInputProps()}
                        required
                        />
                        :
                        <input autoComplete="off" formNoValidate
                        {...this.validInputProps()} value="none"/>
                     }
                    <span className="highlight"></span>
                    <span className="bar"></span>
                    <span></span>
                    <label>{ this.props.title }</label>
                    {
                        this.props.errors && this.props.errors.map((error, i) => <span key={`error-${i}`} className="helper-text">{error}</span>)
                    }
                </div>
            </div>
        )  
    }
}