import React, {Component} from 'react'
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome'
import {Link} from 'react-router-dom'
import fuzzysearch from 'fuzzysearch'
import { bindActionCreators } from 'redux';
import { disableUserAsync } from '../redux/asyncActions'
import { isEmpty, isEqual } from 'lodash'

class UserList extends Component {
    state = {
        pagination : 10,
        search: '',
        usersFiltered: this.props.users,
        allUsers: this.props.users
    }

    componentDidUpdate(){
        if(!isEqual(this.state.allUsers.sort(), this.props.users.sort())){
            this.setState({allUsers: this.props.users}, () => this.filterchange())
        }
    }

    paginationFunction = () => {
        if(this.state.pagination < this.state.usersFiltered.length){
            this.setState({pagination: this.state.pagination + 10})
        }
    }
    filterchange(e){
        var newUsers = this.props.users.map(
            (user) => {
                user.perfil = user.admin? 'Facilitador' : 'innovador'
                return user
            }
        );
        if(!isEmpty(e)){
            var filter = e.currentTarget.value
        }else{
            var filter = ''
        }
        const filteredUsers = this.props.users.filter(user => {
            return (
              fuzzysearch(filter, user.name.toLowerCase()) ||
              fuzzysearch(filter, user.perfil)
            )
        })
        this.setState({usersFiltered: filteredUsers})
    }

    async changeValue(e, id){
        const value = e.currentTarget.checked
        e.checked = false
        var consult = {
            id: id,
            value: value
        }
        await this.props.disableUserAsync(consult)
        this.filterchange('')
    }

    render(){
        return(
        <div className="col-md-9 usuarios-content">
            <div className="subtitle mt-3">
                Lista de usuarios
                <div className="searcher-user">
                    <input placeholder="Buscar" type="text" onChange={(e) => this.filterchange(e)} />
                </div>
            </div>
            <div className="clearfix w-100">
                <div className="mt-3">
                    <table className="table table-hover ">
                        <thead>
                            <tr>
                            <th className="pl-0" scope="col" width="100%">Nombre y apellido</th>
                            <th className="pl-0" scope="col"><span style={{'paddingRight': '25px'}}>Perfil</span></th>
                            <th className="pl-0" scope="col"><span style={{'paddingRight': '25px'}}>Detalle</span></th>
                            <th className="pl-0" scope="col"><span style={{'paddingRight': '25px'}}>Estado</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.usersFiltered.slice(0, this.state.pagination).map(
                                (user, i) => 
                                    <tr key={i}>
                                        <td scope="row">{user.name}</td>
                                        <td>
                                            <input disabled="true" checked={user.admin} type="checkbox" className="input-checked" />
                                        </td>
                                        <td>
                                            <Link to={"/usuarios/detalle/"+user.id}><FontAwesome style={{'color': '#333'}} name="search" /></Link>
                                        </td>
                                        <td>
                                            <input checked={user.state} onChange={(e) => this.changeValue(e, user.id)} type="checkbox" className="input-checked" />
                                        </td>
                                    </tr>
                            )}
                        </tbody>
                    </table>
                    {
                        this.state.pagination < this.state.usersFiltered.length?
                            <div className="user-list-load" >
                                <div onClick={() => this.paginationFunction()}>Cargar más</div>
                            </div>
                        : null
                    }
                    
                </div>
            </div>
        </div>
        )
    }
}
    

const mapStateToProps = state => {
    const { users } = state
    return {users}
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        disableUserAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)