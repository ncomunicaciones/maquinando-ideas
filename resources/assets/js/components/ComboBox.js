import React from 'react'

function ComboBox(props) {
  const invalidClass = props.errors ? 'is-invalid' : ''

  return (
    <div className={`container-combo ${invalidClass}`}>
      <select
        name={props.name}
        id={props.id}
        className="combobox"
        ref={props.refInput}
      >
        <option value="">{props.title}</option>
        {
          props.options.map((option, i) => 
            <option selected={props.select == option.value? true: false} key={`option-${option.value}-${i}`}  value={option.value}>
              { option.name }
            </option>
          )
        }
      </select>
      {
        props.errors && props.errors.map((error, i) => <span key={`error-${i}`} className="helper-text">{error}</span>)
      }
    </div>
  )
}

export default ComboBox