import React, {Component} from 'react';
import Textbox from './Textbox';
import FontAwesome from 'react-fontawesome';
import { isEmpty, isEqual, isFinite } from 'lodash'

export default class Problem extends Component{
    state = {
        solutions: [0,1,2],
        solutionsData: isFinite(this.props.ideaExist)? [0,1,2] : [0]
    }

    componentDidMount(){
        this.resetNumberOfSolutions()
    }

    resetNumberOfSolutions(){
        if(!isEmpty(this.state.solutionsData)){
            let newArr = []
            this.state.solutionsData.map(
                (solutions, i) => newArr.push(i)
            )
            this.setState({solutions: newArr})
        }
    }
    
    addSolution(){
        let arr = this.state.solutions;
        let val = this.state.solutions.length + 1;
        arr.indexOf(val) == -1? '' : val = Math.max.apply(Math, this.state.solutions) + 1;
        this.state.solutions.length >= 3? '' : arr.push(val);
        this.setState({
            solutions : arr
        })
    }

    removeSolution(id){
        let arr = this.state.solutions;
        let index = arr.indexOf(id);
        this.state.solutions.length > 1 ? arr.splice(index, 1) : '';
        this.setState({
            solutions : arr
        });
    }

    render(){
         return !isEmpty(this.state.solutions) && (
            <div className="problem-container mb-4 pb-3">
                <div className={ this.props.numberProblems >= 3 ? 'problem-container-header inactive': 'problem-container-header '}>
                    Situación a mejorar <span onClick={this.props.event}><FontAwesome name="plus" /> Agregar</span>
                </div>
                <div className="clearfix">
                    <div onClick={this.props.deleteProblem} className={ this.props.numberProblems <= 1? 'float-left idea-form-minus inactive': 'float-left idea-form-minus'}>
                        <FontAwesome  name="minus-circle" />
                    </div>
                    <div className="col-part">
                        <Textbox
                            title="Oportunidad de mejora"
                            name={"problem["+this.props.set+"]"}
                            refInput={node => this.props.problems[this.props.set] = node}
                            errors={this.props.errors.problems ? this.props.errors.problems[this.props.set] : []}
                        />
                    </div>
                </div>
                <div className="solution-container">
                    <div className={ this.state.solutions.length >= 3 ? 'problem-container-header inactive': 'problem-container-header'}>
                        Escribe tus ideas <span onClick={() => this.addSolution()} ><FontAwesome name="plus" /> Agregar</span>
                    </div>
                    {
                        !isEmpty(this.state.solutions) && this.state.solutions.map(solution =>
                            <div
                                key={solution}
                                className="clearfix"
                            >
                                <div
                                    className={ this.state.solutions.length <= 1? 'float-left idea-form-minus inactive': 'float-left idea-form-minus'}
                                    onClick={() => this.removeSolution(solution)}
                                >
                                    <FontAwesome name="minus-circle" />
                                </div>
                                <div className="col-part">
                                    <Textbox
                                        title="Propuesta"
                                        name={"solution["+this.props.set+"]["+ solution +"]"}
                                        refInput={node => {
                                            this.props.solutions[this.props.set] = Array.isArray(this.props.solutions[this.props.set]) ?
                                                this.props.solutions[this.props.set] :
                                                []

                                            return this.props.solutions[this.props.set][solution] = node
                                        }}
                                        errors={this.props.errors.solutions ?
                                            this.props.errors.solutions[this.props.set] ?
                                                this.props.errors.solutions[this.props.set][solution]
                                                : []
                                            : []
                                        }
                                    />
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }
}