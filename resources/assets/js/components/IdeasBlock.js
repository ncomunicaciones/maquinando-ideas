import React from 'react'
import TableIdeas from './TableIdeas'
import { Link } from 'react-router-dom'
import FontAwesome from 'react-fontawesome'
import {isEmpty, isFinite} from 'lodash'

const IdeasBlock = ({title, categories, rows, link, icons, link2, data = null, ideastate, size, isOpen}) => {
    const toggleItems = (e) => {
        $(e.currentTarget).parents('.subtitle').next('div').stop(true,true).slideToggle()
        $(e.currentTarget).toggleClass('active')
    }
    return (
        <div className="set-type-idea">
            {!isEmpty(title) && 
                <div className="subtitle subtitle-mis-ideas clearfix">
                    { rows !== null && 
                        <div className="float-left">{title} {isFinite(size)? '('+size+')' : null}</div>
                    }
                    <div className={isOpen? "active subtitle-icon" : 'subtitle-icon'} onClick={(e) => toggleItems(e)}><FontAwesome name="chevron-up" />
                    </div>
                </div>
            }
            <div style={{display : isOpen? 'block' : 'none', width: '100%'}}>
                <div className="clearfix w-100" >
                    <TableIdeas link={link2} icons={icons} type="guardado" categories={categories} ideastate={ideastate? 1:0} rows={rows} data={data} />
                </div>
                {
                    rows !== null && !isEmpty(title) && <div className="center-me"><Link className="clearfix" to={link}><div className="ver-todos">Ver todos</div></Link></div>
                }
            </div>
            {!isEmpty(title) && 
                <div className="mis-ideas-br"> </div>
            }
        </div>
    )
}

export default IdeasBlock
