import React from 'react'
import LoaderHOC from '../HOC/LoaderHOC'
import {isEmpty} from 'lodash'
import FontAwesome from 'react-fontawesome'

const CategoryDetail = ({category, ideaName, idea, innovador, auth}) => {
    const getError = (e) => {
        e.target.src = assets + "/error-category.png"
    }
    const toggle = (e) => {
        $(e.target).parents('.toggle-category').toggleClass('active')
        $(e.target).parents('.category-detail-container').find('.CategoryDetail').slideToggle();
    }

    return(
        <div className="category-detail-container">
        <div className="toggle-category clearfix">
            <div className="float-left toggle-category-content">Resumen de idea</div>
            <div className="float-right toggle-category-content" onClick={(e) => toggle(e)}><FontAwesome name="chevron-down    " /></div>
        </div>
        <div className="CategoryDetail">
            <div className="CategoryDetail-circle">
                <div className="CategoryDetail-circle2 d-flex align-items-center justify-content-center">
                    <img src={assets + "/avatars/" + category.image} onError={(e) => getError(e)} />
                </div>
            </div>
            <div className="CategoryDetail-description">
                <div className="CategoryDetail-description-title">
                    {category.name}
                </div>
            </div>
            {ideaName?
                <div className="mt-3">
                    <div className="CategoryDetail-idea-name1">idea innovadora</div>
                    <div className="CategoryDetail-idea-name2">{ideaName}</div>
                </div>
                    : 
                    ''
            }
            { !isEmpty(idea) && idea.state == "ejecucion" || idea.state == "aprobado" || idea.state == "desaprobado" || idea.state == "premiada" || idea.state == "abortada" || idea.state == "evaluacion"?
                <div className="mt-4 category-detail">
                    <div className="category-detail-title">Innovador</div>
                    <ul>
                        <li style={{color: auth.name == innovador ? '#b71a1b' : null}}>{innovador}</li>
                    </ul>
                </div> : null
            }
            { !isEmpty(idea) && idea.state == "ejecucion" || idea.state == "premiada" || idea.state == "abortada"? 
                <div className="mt-4 category-detail">
                    <div className="category-detail-title">Colaboradores</div>
                    <div className="category-detail-subtitle">Jefe Operador</div>
                    <ul>
                        <li style={{color: auth.name == idea.getBossOperators.name ? '#b71a1b' : null}}>{idea.getBossOperators.name}</li>
                    </ul>
                    {!isEmpty(idea.getOperators) && 
                        <div className="category-detail-subtitle">Ejecutores</div>
                    }
                    <ul>
                        {idea.getOperators.map(
                            (operator, i) => <li key={i} style={{color: auth.name == operator.name ? '#b71a1b' : null}}>{operator.name}</li>
                        )}
                    </ul>
                </div> : null
            }
            { !isEmpty(idea) && idea.state == "ejecucion" || idea.state == "aprobado" || idea.state == "desaprobado" || idea.state == "premiada" || idea.state == "abortada" || idea.state == "evaluacion" && !isEmpty(idea.administratorcreator)?
                <div className="mt-4 category-detail">
                    <div className="category-detail-title">Facilitador</div>
                    <ul>
                        <li style={{color: auth.name == idea.administratorcreator.name ? '#b71a1b' : null}}>{idea.administratorcreator.name}</li>
                    </ul>
                </div> : null
            }
        </div>
    </div>
    )
}
    

export default LoaderHOC('category')(CategoryDetail)