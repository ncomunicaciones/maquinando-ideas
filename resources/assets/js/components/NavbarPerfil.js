import React from 'react';
import { HashRouter as Router, Route,Switch, Link, Redirect, NavLink } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { isEmpty } from 'lodash'


class NavbarPerfil extends React.Component{
    state = {
        gender : this.props.user.gender,
        validate : true,
        loading: false
    }
    componentDidUpdate(){
        if(this.props.user.gender != this.state.gender){
            this.setState({validate: false, gender: this.props.user.gender}, () => this.setState({validate: true}))
        }
    }
    render(){
        return(
            <div className="navbar-user-container h-100 float-right">
        <div className="float-left h-100 navbar-user-hover">
            <div className="float-left">
                <Link to="/perfil">
                    <div className="d-flex align-items-center h-100 float-left ml-2 mr-2 navbar-user-name">
                        <div><span>{this.props.type == 'administrator'? 'Facilitador(a)' : 'innovador' }</span><br/>{this.props.user.name.split(" ")[0]}</div>
                    </div>
                    <div className="d-flex align-items-center h-100 float-left ml-1 mr-2">
                        <div style={ {background: `image( ${assets+'/avatars/'+this.props.user.avatar}, ${assets+'/avatar.png'} )`}}  className="navbar-user-icon">
                            {this.state.validate &&
                                <div>
                                    {this.state.gender == 'm'?
                                        <img src={assets+'/avatars/'+this.props.user.avatar} data='M' onError={(e) => e.target.src = assets + '/avatar.png'} /> 
                                        : 
                                        <img src={assets+'/avatars/'+this.props.user.avatar} data='F' onError={(e) => e.target.src = assets + '/avatar-f.png'} />
                                    }
                                </div>
                            }
                        </div>
                    </div>
                    <div className="d-flex align-items-center h-100 float-left dropdown-toggle">
                        <FontAwesome name='chevron-down' className='navbar-arrow'/>
                    </div>
                </Link>
            </div>
        </div>
        <div className="perfil-dropdown">
            <div className="dropdown-setter"></div>
            <ul>
                { this.props.type == 'user' &&
                    <li className="navbar-options-container-hidden">
                        <Link to="/mis-ideas">
                            Mis ideas
                        </Link>
                    </li>
                }
                { this.props.type == 'user' &&
                    <li className="navbar-options-container-hidden">
                        <Link to="/mis-ideas-ejecutor">
                            Ejecutor
                        </Link>
                    </li>
                }
                { (!isEmpty(this.props.user.administrator)) ?
                    !this.state.loading? 
                        <li onClick={() => {this.setState({loading: true}); this.props.changeStateUser()}}>
                            <a>{this.props.userstate ? 'Perfil innovador' : 'Perfil Facilitador'}</a>
                        </li>
                    :
                        <li><a>Cargando...</a></li>
                    :
                    null
                }
                { (this.props.type == 'administrator' && this.props.userstate) &&
                    <li>
                        <Link to="/contenido/administrar">
                            Administrar
                        </Link>
                    </li>
                }
                { (this.props.type == 'administrator' && this.props.userstate) &&
                    <li>
                        <Link to="/usuarios">
                            Usuarios
                        </Link>
                    </li>
                }
                { (this.props.type == 'administrator' && this.props.userstate) &&
                    <li>
                        <Link to="/categorias/administrar">
                            Categorías
                        </Link>
                    </li>
                }
                { (this.props.type == 'administrator' && this.props.userstate) &&
                    <li>
                        <Link to="/estadisticas">
                            Estadísticas
                        </Link>
                    </li>
                }
                <li>
                    <Link to="/perfil">
                        Editar perfil
                    </Link>
                </li>
                <li onClick={() => this.props.logout()}><a>Salir</a></li>
            </ul>
        </div>
        </div>
        )
    }
}
        
    
export default NavbarPerfil