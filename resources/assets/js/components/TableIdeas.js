import React from 'react';
import IdeasIcons from '../components/IdeasIcons';
import LoaderHOC from '../HOC/LoaderHOC';

const TableIdeas = ({rows, categories, type, link, icons, data, ideastate}) => {
        return (
            <div>
                <table className="table table-hover ">
                    <thead>
                        <tr>
                        <th className="pl-0" scope="col" width="100%">Lista de ideas</th>
                        {categories && <th className="pl-0" scope="col" style={{'paddingRight': '75px'}} >Categoría</th>}
                        {ideastate? <th className="pl-0" scope="col" style={{'paddingRight': '75px'}} >Estado</th>: null}
                        <th className="pl-0" scope="col"><span style={{'paddingRight': '45px'}}>Opciones</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        {typeof(rows) !== 'undefined' && rows.map(
                            row =>
                            <tr key={`${type}-${row.id}`}>
                                <td scope="row">{row.title}</td>
                                {categories && <td>{row.category_name}</td>}
                                {ideastate? <td>{row.state == 'ejecucion'? 'ejecución' : row.state}</td>: null}
                                <td><IdeasIcons icons={icons} id={row.id} type={type} data={row} link={link} /></td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
}

export default LoaderHOC('rows')(TableIdeas)