import React from 'react'

class Alert extends React.Component{
    state = {
        counter: 5,
        show: true,
        style :{
            opacity: 0,
            visibility: 'hidden',
            transition: 'all 1s ease',
            position: 'relative',
            top: '50px'
          }
    }
    componentDidMount(){
        this.setCounter();
    }
    
    counter = 0

    setCounter = () => {
        this.counter = setInterval( () => {this.setState({counter: this.state.counter - 1}, () => {
            if(this.state.counter <= 0){
                this.props.deletethis();
                clearTimeout(this.counter);
                this.counter = 0;
            }
        })}, 1000 );
    }

    componentWillReceiveProps = (newProps) => { //check for the mounted props
        if(!newProps.mounted) return this.unMountStyle()
        this.setState({
          show: true
        })
        this.resetCounter();
        setTimeout(this.mountStyle, 10)
    }

    clearCounter = () => {
        this.props.deletethis();
        clearTimeout(this.counter);
        this.counter = 0;
    }

    resetCounter = () => {
        clearTimeout(this.counter);
        this.counter = 0;
        this.setState({counter: 5}, () => {
            this.setCounter()
        })
    }

    unMountStyle = () =>  { //css for unmount animation
        this.setState({
          style: {
            opacity: 0,
            visibility: 'hidden',
            transition: 'all .5s ease',
            top: '50px'
          }
        })
      }

      mountStyle = () => { // css for mount animation
        this.setState({
          style: {
            opacity: 1,
            visibility: 'visible',
            transition: 'all 1s ease',
            top: '0px'
          }
        })
      }

      transitionEnd = () => {
        if(!this.props.mounted){ //remove the node on transition end when the mounted prop is false
          this.setState({
            show: false
          })
        }
      }

    render(){
        return(
            <div style={this.state.style} onTransitionEnd={this.transitionEnd} className={"hide-alert alert-"+ this.props.type +" alert alert-dismissible fade show mt-4"} role="alert">
                { this.props.message }
                <button type="button" className="close" onClick={() => this.clearCounter()}>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        )
    }
}


//Types: Success, Danger

export default Alert