import React from 'react'

const StatisticsTable = ( {data, categories} ) => {
    return(
        <table className="table-statistics table table-hover ">
            <thead>
                <tr>
                    <th className="pl-0" scope="col" width="100%">Lista de ideas</th>
                    <th className="pl-0" scope="col" style={{'paddingRight': '75px'}} >Última actualización</th>
                    <th className="pl-0" scope="col" style={{'paddingRight': '75px'}} >Categoría</th>
                    <th className="pl-0" scope="col"><span style={{'paddingRight': '45px'}}>Estado</span></th>
                </tr>
            </thead>
            <tbody>
                {data.map(
                    (row, i) =>
                    <tr key={i}>
                        <td scope="row">{ row.title }</td>
                        <td scope="row">{ row.updated_at }</td>
                        <td>{ categories.find((category) => category.id == row.category_id).name }</td>
                        <td>{ row.state }</td>
                    </tr>
                )}
            </tbody>
        </table>
    )
}

export default StatisticsTable