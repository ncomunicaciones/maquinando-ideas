import React from 'react';
import {connect} from 'react-redux';
import { HashRouter as Router, Route,Switch, Link, Redirect, NavLink } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import { bindActionCreators } from 'redux';
import { editNotificationAsync } from '../redux/asyncActions'
import { isEmpty } from 'lodash'

function getNotifsLength(notifications) {

    let allNotifications = notifications.filter((notification) => notification.read == false)
    return allNotifications
}

class NavbarNotifications extends React.Component{
    state = {
        filter: 'todos'
    }
    callIdea = (e) => {
        this.props.editNotificationAsync(e)
    }

    getError = (e, user) => {
        if(user.gender == 'm') e.target.src = assets + '/avatar.png'
        if(user.gender == 'f') e.target.src = assets + '/avatar-f.png'
    }
    changeState(e, message){
        this.getOpt.map(
            (div) => div.classList.remove('active')
        )
        e.target.classList.add('active')
        this.setState({filter: message})
    }

    getOpt = []

    render(){
        return(
            <div className="navbar-notification-container float-right h-100 ml-2 mr-2">
        <div className="d-flex align-items-center h-100 float-left">
            <div>
            <div className="dropdown clearfix notifications-open">
                <Link to="/notificaciones/todos" className="notifications-container dropdown-toggle d-flex align-items-center">
                        <FontAwesome name='bell' className={`navbar-notifications incoming-animatee`}/>
                            {getNotifsLength(this.props.notifications).length > 0?
                                <div className="notification-alert">{getNotifsLength(this.props.notifications).length }</div> :
                                null
                            }
                </Link>
                    <div className="notifications-dropdown">
                    <div className="dropdown-setter"></div>
                    <div className="notifications-dropdown-mx">
                        <div className="dropdown-tabs">
                            <div onClick={(e) => this.changeState(e,'todos')} ref={(e) => this.getOpt[0] = e} className="dropdown-tab dropdown-tab-first active">
                                Todos ({!isEmpty(this.props.notifications) && this.props.notifications.filter((notification) => notification.read == false).length})
                            </div>
                            <div onClick={(e) => this.changeState(e,'notificacion')} ref={(e) => this.getOpt[1] = e} className="dropdown-tab dropdown-tab-first">
                                Notificaciones ({!isEmpty(this.props.notifications) && this.props.notifications.filter((notification) => notification.type != 'chat' && notification.read == false).length})
                            </div>
                            <div onClick={(e) => this.changeState(e,'chat')} ref={(e) => this.getOpt[2] = e} className="dropdown-tab dropdown-tab-second">
                                Mensajes ({!isEmpty(this.props.notifications) && this.props.notifications.filter((notification) => notification.type == 'chat' && notification.read == false).length})
                            </div>
                        </div>
                        <div className="notifications-opts clearfix">
                            <div className="notifications-opt">
                                <div className="dropdown-body">
                                    {isEmpty(this.props.notifications.filter((noti) => noti.read == false & noti.type != 'chat')) && this.state.filter == 'notificacion' &&
                                       <div className="notificacion-message-inde">No tiene nuevas notificaciones</div>  }
                                    {isEmpty(this.props.notifications.filter((noti) => noti.read == false & noti.type == 'chat')) && this.state.filter == 'chat' &&
                                       <div className="notificacion-message-inde">No tiene nuevos mensajes</div>  }
                                    {isEmpty(this.props.notifications.filter((noti) => noti.read == false)) && this.state.filter == 'todos' &&
                                       <div className="notificacion-message-inde">No tiene nuevas actualizaciones</div>  }
                                    { this.props.notifications.filter(
                                        (notification) => this.state.filter == 'chat'? notification.type == 'chat' : this.state.filter == 'notificacion'?  notification.type != 'chat' : notification 
                                    ).filter((noti) => noti.read != true).map((notification, i) => 
                                        <Link key={i} to={"/"+notification.getLink} onClick={() => this.callIdea(notification.id)}>
                                            {notification.type != 'chat' &&
                                            <div className="notifications-box" >
                                                <div className="d-flex h-100  notifications-box-type-container align-items-center float-left">
                                                    <div className={!notification.read? "notifications-box-type green" : "notifications-box-type"} ></div>
                                                </div>
                                                <div className=" h-100  notifications-box-text-container float-left">
                                                    <span>{notification.title}</span>
                                                    <div className="notification-props">
                                                    {!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state?
                                                        <div className="notification-props-tag">
                                                            {!isEmpty(this.props.ideassupervisor) && !isEmpty(this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id)) && this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id).title}
                                                        </div>
                                                                :
                                                        <div className="notification-props-tag">
                                                            {!isEmpty(this.props.ideasinnovador) && !isEmpty(this.props.ideasinnovador.find((idea) => idea.id == notification.idea_id)) && this.props.ideasinnovador.find((idea) => idea.id == notification.idea_id).title}
                                                        </div> 
                                                    }
                                                    <div className={!notification.read? "notification-see-read" + " notification-props-tag notification-props-tag-see" : " " + " notification-props-tag notification-props-tag-see"}>
                                                        {notification.read? "Leído" : "No Leído"}
                                                    </div> 
                                                        <div className="notification-props-time">
                                                            {notification.time}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            }
                                            {notification.type == 'chat' &&
                                            <div className="notifications-box" >
                                                <div className="d-flex h-100  notifications-box-type-container align-items-center float-left">
                                                    <div className={!notification.read? "notifications-box-type green" : "notifications-box-type"} ></div>
                                                </div>
                                                <div className=" h-100  notifications-box-text-container float-left notifications-box-text-container-message">
                                                    <div className="image-box-noti">
                                                        <div>
                                                            <img className="image-box-noti-img" src={assets+'/avatars/'+ this.props.allUsers.find((user) => user.id == notification.provider).avatar} onError={(e) => this.getError(e, this.props.allUsers.find((user) => user.id == notification.provider))} />
                                                        </div>
                                                    </div>
                                                    <div className="image-box-desc">
                                                        <span>{notification.title}</span>
                                                        <div className="notification-props notification-props-message">
                                                            <div className="notification-props-time">
                                                                {notification.time}
                                                            </div>
                                                            {!isEmpty(this.props.auth.administrator) && this.props.auth.administrator[0].state?
                                                                    <div className="notification-props-tag">
                                                                        {!isEmpty(this.props.ideassupervisor) && 
                                                                            this.props.ideassupervisor.find((idea) => idea.id ==   notification.idea_id).user_id == notification.provider? 'Innovador' : !isEmpty(this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id)) && this.props.ideassupervisor.find((idea) => idea.id == notification.idea_id).admin_id == notification.provider? 'Facilitador' : 'Ejecutor'}
                                                                    </div>
                                                                            :
                                                                    <div className="notification-props-tag">
                                                                        {!isEmpty(this.props.ideasinnovador) && 
                                                                            this.props.ideasinnovador.find((idea) => idea.id ==   notification.idea_id).user_id == notification.provider? 'Innovador' : this.props.ideasinnovador.find((idea) => idea.id ==   notification.idea_id).administratorcreator.id == notification.provider? 'Facilitador' : 'Ejecutor'}
                                                                    </div>
                                                            }
                                                            <div className="notification-props-tag notification-props-tag-last">
                                                                {notification.userProvider[0]}
                                                            </div>
                                                            <div className={!notification.read? "notification-see-read" + " notification-props-tag notification-props-tag-see" : " " + " notification-props-tag notification-props-tag-see"}>
                                                                {notification.read? "Leído" : "No Leído"}
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            }
                                        </Link>
                                    ).slice(0,5)}
                                    {this.state.filter == 'todos' && <Link className="see-all-noti" to="/notificaciones/todos">Ver todos</Link>}
                                    {this.state.filter == 'notificacion' && <Link className="see-all-noti" to="/notificaciones/notificacion">Ver todas las notificaciones</Link>}
                                    {this.state.filter == 'chat' && <Link className="see-all-noti" to="/notificaciones/chat">Ver todos los chats</Link>}
                                </div>
                            </div>
                        </div>
                        {/*getNotifsLength(notifications, user.administrator, userstate.data) > 0 ? 
                            <div className="dropdown-footer text-center"><Link to="/notificaciones">Mostrar Todas las notificaciones</Link></div> :
                            <div className="dropdown-footer text-center"><a>No tiene registros</a></div>
                        */}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { ideasinnovador } = state
    const { ideassupervisor } = state
    const { notifications } = state
    const { auth } = state
    const allUsers = state.auth.allUsers
    return {ideasinnovador, notifications, ideassupervisor, auth, allUsers}
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        editNotificationAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(NavbarNotifications)