import React,{Component} from 'react'
import {connect} from 'react-redux'
import { HashRouter as Router, Link, Redirect, NavLink, withRouter } from 'react-router-dom'
import FontAwesome from 'react-fontawesome'

class NavbarMobile extends Component {

    componentDidMount() {
        const elements = Array.from(document.querySelectorAll('.close-menu-button, .close-mobile'))
        
        elements.map(
            (elem) => elem.onclick = () => {
                document.getElementsByClassName('menu-mobile')[0].classList.toggle('active')
                document.getElementsByClassName('close-mobile')[0].classList.toggle('active')
            }
        )
    }

    render() {
        return(
            <div className="nav-bar-responsive active">
                <div className="row h-100">
                    <div className="col-sm-8 col-xs-8 col-8 h-100">
                        <div className="d-flex align-items-center h-100">
                            <Link to="/"> <img src={assets + "/logo.png"} /> </Link>
                        </div>
                    </div>
                    <div className="col-sm-4 col-xs-4 col-4 h-100 w-100">
                        <div className="d-flex align-items-center h-100 w-100 flex-row-reverse close-menu-button">
                            <FontAwesome onClick={this.openMenu} className="float-right pr-2 text-dark" size="2x" name="bars" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(connect(null, null)(NavbarMobile))