import React from 'react';
import Category from './Category'
import {connect} from 'react-redux'

const CategoryList = ({categories}) => 
            <div className="row">
                {categories.filter((category) => category.state == true).map(
                    
                        (categoria, i) => <div key={i} className="col-md-6 pr-0 mb-4"><Category data={categoria} /></div>
                
                )}
            </div>
    

const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps, null)(CategoryList) 