import React, { Component } from 'react'
import Textbox from './../components/Textbox'
import Button from './../components/Button'
import {isEmpty} from 'lodash'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { addCategoryAsync, editCategoryAsync } from '../redux/asyncActions'
import Alert from '../components/Alert';


class AdminCreateCategory extends Component {

    state = {
        categoryId: this.props.match.params.id,
        type: null,
        category: null,
        message: false,
        messageText: null,
        send: false,
    }

    componentDidMount(){
        this.setState({type: this.props.match.params.id? 'editar' : 'crear'}, () => {
            if(this.state.type == 'crear'){
                this.setState({category: null})
            }else{
                this.setState({category: this.props.categories.find(category => category.id == this.props.match.params.id)})
            }
        })
    }

    changeMessage = async () => {
        this.setState({message: false})
    }

    componentDidUpdate(){
        if(!isEmpty(this.state.category)){
            if(this.props.match.params.id != this.state.category.id){
                this.setState({type:'crear', category:null})
            }
        }
    }

    editCategory = async (event) => {
        event.preventDefault()
        if (true) {
            this.setState({send: true})
            event.preventDefault()

            if (true) {
                this.setState({ errors: {} })
                try {
                    const fd = new FormData();
                    fd.append('id', this.state.category.id);
                    fd.append('name', this.inputs.name.value);
                    fd.append('description', this.inputs.description.value);
                    fd.append('image', this.inputs.image.files[0])
                    this.props.editCategoryAsync(fd)
                    this.setState({send: false, message: true, messageText: 'La categoría se editó exitosamente'})
                } catch (err) {
                    console.error(err)
                }
            }
        }
    }
    createCategory = async (event) => {
        event.preventDefault()
        if (true) {
            this.setState({ errors: {} })
            try {
                this.setState({send: true})
                const fd = new FormData();
                fd.append('name', this.inputs.name.value);
                fd.append('description', this.inputs.description.value);
                fd.append('image', this.inputs.image.files[0])

                this.props.addCategoryAsync(fd)
                document.getElementById('myform').reset();
                this.setState({send: false, message: true, messageText: 'La categoría se creó exitosamente'})
            } catch (err) {
                console.error(err)
            }
        }
    }

    setImage(){

    }

    inputs = {}
    render(){
        return(
            <div className="col-md-9 usuarios-content">
                <div className="subtitle mt-3"> {this.state.type == 'crear'? 'Crear categoría' : 'Editar categoría' }</div>
                <div className="mt-2 d-flex flex-row-reverse">
                    <div className="col-md-12">
                        {isEmpty(this.state.category)?
                        <form id="myform" onSubmit={this.createCategory} noValidate autoComplete="no">
                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="names"
                                        title="Nombre"
                                        refInput={node => this.inputs.name = node}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="description"
                                        title="Descripción"
                                        refInput={node => this.inputs.description = node}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 mt-5">
                                    <input
                                        onChange={() => this.setImage()}
                                        id="avatar"
                                        type="file"
                                        accept="image/x-png,image/gif,image/jpeg"
                                        className="input-avatar"
                                        ref={node => this.inputs.image = node}
                                    />
                                    <div className="image-helper">El ícono de la categoría debe tener las siguientes dimensiones: 78px x 108px y debe pesar menos de 10.00 kb</div>
                                </div>
                            </div>
                            <div className="clearfix mt-3">
                                <div className="mr-2 float-right">
                                    <Button
                                        event={() => window.history.go(-1)}
                                        type="button"
                                        state="true"
                                        color="black"
                                        name="Cancelar"
                                    />
                                    
                                </div>
                                <div className="mr-2 float-right">
                                    <Button
                                        type="submit"
                                        color="red"
                                        name="Crear"
                                        disabled={this.state.send}
                                    />
                                </div>
                            </div>
                        </form>
                        :
                        <form id="myform" onSubmit={this.editCategory} noValidate autoComplete="no">
                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="names"
                                        title="Nombre"
                                        defaultValue={this.state.category.name}
                                        refInput={node => this.inputs.name = node}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <Textbox
                                        name="description"
                                        defaultValue={this.state.category.description}
                                        title="Descripción"
                                        refInput={node => this.inputs.description = node}
                                    />
                                    
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 mt-5">
                                    <input
                                        onChange={() => this.setImage()}
                                        id="avatar"
                                        type="file"
                                        accept="image/x-png,image/gif,image/jpeg"
                                        className="input-avatar"
                                        ref={node => this.inputs.image = node}
                                    />
                                    <div className="image-helper">El ícono de la categoría debe tener las siguientes dimensiones: 78px x 108px y debe pesar menos de 10.00 kb</div>
                                </div>
                            </div>
                            <div className="clearfix mt-3">
                                <div className="mr-2 float-right">
                                    <Button
                                        event={() => window.history.go(-1)}
                                        type="button"
                                        state="true"
                                        color="black"
                                        name="Cancelar"
                                    />
                                    
                                </div>
                                <div className="mr-2 float-right">
                                    <Button
                                        type="submit"
                                        color="red"
                                        name="Editar"
                                        disabled={this.state.send}
                                    />
                                </div>
                            </div>
                        </form>
                        }
                        <Alert deletethis={() => this.changeMessage()} onTransitionEnd={this.transitionEnd} mounted={this.state.message} type="success" message={this.state.messageText} />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {categories} = state
    return {categories}
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        addCategoryAsync, 
        editCategoryAsync
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AdminCreateCategory)