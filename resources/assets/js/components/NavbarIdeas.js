import React from 'react';
import { HashRouter as Router, Route,Switch, Link, Redirect, NavLink } from 'react-router-dom';

const NavbarIdeas = ({ideas, type}) => 
    <div className="d-flex align-items-center navbar-options-container-opt float-left h-100 pr-2 pl-2 position-relative">
    <Router>
        <NavLink className="navbar-options-container-opt-link" exact to={type == 'administrator'? '/' : '/mis-ideas'}>
            <div><strong>{type == 'administrator'? 'Ideas' : 'Mis ideas'}</strong> <span>({ideas.length})</span></div>
        </NavLink>
    </Router>
    {ideas.length > 0 &&
    <div className="navbar-dropdown">
    <div className="dropdown-setter"></div>
        <ul>
            {ideas.filter( (idea) => idea.state == 'guardado' ).length > 0 &&
                <li>
                    <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/guardado' : '/mis-ideas/guardado'}>
                        <div className="navbar-dropdown-size">
                            <div className="navbar-dropdown-icon">
                                {ideas.filter( (idea) => idea.state == 'guardado' ).length}
                            </div>
                        </div>
                        <div className="navbar-dropdown-text">Ideas guardadas</div>
                    </Link>
                </li>
            }
            {ideas.filter( (idea) => idea.state == 'evaluacion' ).length > 0 &&
                <li>
                    <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/evaluacion' : '/mis-ideas/evaluacion'}>
                        <div className="navbar-dropdown-size">
                            <div className="navbar-dropdown-icon">
                                {ideas.filter( (idea) => idea.state == 'evaluacion' ).length}
                            </div>
                        </div>
                        <div className="navbar-dropdown-text">Ideas en evaluación</div>
                    </Link>                                     
                </li>
            }
            {ideas.filter( (idea) => idea.state == 'aprobado' ).length > 0 &&
                <li className="clearfix">
                    <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/aprobado' : '/mis-ideas/aprobado'}>
                        <div className="navbar-dropdown-size">
                            <div className="navbar-dropdown-icon">
                                {ideas.filter( (idea) => idea.state == 'aprobado' ).length}
                            </div>
                        </div>
                        <div className="navbar-dropdown-text">Ideas aprobadas</div>  
                    </Link>                                        
                </li>
            }
            {ideas.filter( (idea) => idea.state == 'ejecucion' ).length > 0 &&
                <li className="clearfix">
                    <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/ejecucion' : '/mis-ideas/ejecucion'}>
                        <div className="navbar-dropdown-size">
                            <div className="navbar-dropdown-icon">
                                {ideas.filter( (idea) => idea.state == 'ejecucion' ).length}
                            </div>
                        </div>
                        <div className="navbar-dropdown-text">Ideas aprobadas y en ejecución</div>       
                    </Link>                                   
                </li>
            }
            {ideas.filter( (idea) => idea.state == 'premiada' ).length > 0 &&
                <li className="clearfix">
                    <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/premiada' : '/mis-ideas/premiada'}>
                        <div className="navbar-dropdown-size">
                            <div className="navbar-dropdown-icon">
                                {ideas.filter( (idea) => idea.state == 'premiada' ).length}
                            </div>
                        </div>
                        <div className="navbar-dropdown-text">Ideas premiadas</div> 
                    </Link>        
                </li>
            }
            {ideas.filter( (idea) => idea.state == 'desaprobado' ).length > 0 &&
                <li className="clearfix">
                    <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/desaprobado' : '/mis-ideas/desaprobado'}>
                        <div className="navbar-dropdown-size">
                            <div className="navbar-dropdown-icon">
                                {ideas.filter( (idea) => idea.state == 'desaprobado' ).length}
                            </div>
                        </div>
                        <div className="navbar-dropdown-text">Ideas desaprobadas</div> 
                    </Link>
                </li>
            }
            {ideas.filter( (idea) => idea.state == 'abortado' ).length > 0 &&
            <li className="clearfix">
                <Link className="navbar-dropdown-link clearfix" to={type == 'administrator'? '/ideas/abortado' : '/mis-ideas/abortado'}>
                    <div className="navbar-dropdown-size">
                        <div className="navbar-dropdown-icon">
                            {ideas.filter( (idea) => idea.state == 'abortado' ).length}
                        </div>
                    </div>
                    <div className="navbar-dropdown-text">Ideas abortadas</div> 
                </Link>
            </li>
            }
        </ul>
    </div>
    }
    <Router>
        <Route exact path="/idea/:id" component={() =>
            <div className="bubble-ideas">Se agregó una nueva idea</div>
        } />
    </Router>
    
    </div>
    
export default NavbarIdeas