import React from 'react'
import FontAwesome from 'react-fontawesome'

const NavbarSocial = () => 
    <div className="col-md-3 h-100">
        <div className="h-100 float-left">
            <div className="d-flex align-items-center h-100 float-left ml-2 mr-2">
                <div className="navbar-redes">
                    <a href="mailto:contact.center@unimaq.com.pe" target="_top">
                        <div className="navbar-redes-box">
                            <FontAwesome name="envelope" />
                        </div>
                    </a>
                    <div className="navbar-redes-box">
                        <FontAwesome name="phone" /> &nbsp; (01) 202-1300
                    </div>
                    <a href="https://www.facebook.com/UnimaqPeru/" target="_blank">
                        <div className="navbar-redes-box">
                            <FontAwesome name="facebook" />
                        </div>
                    </a>
                    <a href="https://www.linkedin.com/company/unimaq-peru/" target="_blank">
                        <div className="navbar-redes-box">
                            <FontAwesome name="linkedin" />
                        </div>
                    </a>
                    <a href="https://www.youtube.com/channel/UCuouDGNBuX3nAio4vicrmdQ" target="_blank">
                        <div className="navbar-redes-box">
                            <FontAwesome name="youtube-play" />
                        </div>
                    </a>
                    <a href="https://www.instagram.com/unimaq.peru/" target="_blank">
                        <div className="navbar-redes-box">
                            <FontAwesome name="instagram" />
                        </div>
                    </a>
                    <a href="tel:973850194" target="_blank">
                        <div className="navbar-redes-box">
                            <FontAwesome name="whatsapp" />
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>


export default NavbarSocial