import React from 'react'

class LoadingApp extends React.Component{
    state = {
        show: true,
        style :{
            fontSize: 60,
            opacity: 0,
            transition: 'all 1s ease',
          }
    }
    componentWillReceiveProps = (newProps) => { //check for the mounted props
        if(!newProps.mounted)
          return this.unMountStyle()
        this.setState({ 
          show: true
        })
        setTimeout(this.mountStyle, 10)
    }

    unMountStyle = () =>  { //css for unmount animation
        this.setState({
          style: {
            fontSize: 60,
            opacity: 0,
            visibility: 'hidden',
            transition: 'all .5s ease',
          }
        })
      }
      
      mountStyle = () => { // css for mount animation
        this.setState({
          style: {
            fontSize: 60,
            opacity: 1,
            visibility: 'visible',
            transition: 'all 1s ease',
          }
        })
      }

      componentDidMount(){
        setTimeout(this.mountStyle, 10) //call the into animiation
      }
      transitionEnd = () => {
        if(!this.props.mounted){ //remove the node on transition end when the mounted prop is false
          this.setState({
            show: false
          })
        }
      }
      
    render(){
        return this.state.show && (
            <div style={this.state.style} onTransitionEnd={this.transitionEnd} className="loading-app">
                <div className="logo">
                  <img className="w-100" src={assets + "/logo-load.png"} />
                    <div className="loading-bar">
                      <div style={{width: this.props.percentage+'%'}} className="loading-bar-progress"></div>
                    </div>
                </div>
            </div>
        )
    }
} 
    
export default LoadingApp