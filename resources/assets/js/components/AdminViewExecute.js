import React from 'react'
import EvaluateProgress from './../containers/EvaluateProgress'
import IdeaState from './../containers/IdeaState'
import ChatBox from './../containers/ChatBox'

function AdminViewExecute(props) {
  return (
    <div>
      <EvaluateProgress
        day={props.daySent}
        isAdmin={true}
        ideaId={props.ideaId}
        stateText='ejecución'
        startDate={props.startDateExec}
        timeState={props.timeExec}
      />

      <IdeaState
        type="ejecucion"
        typeUser="admin"
      />
      
      <ChatBox
        admin={true}
        messages={props.messages}
        sendMessage={props.sendMessage}
        refInputChat={props.refInputChat}
        loadMessage={props.loadMessage}
      />
    </div>
  )
}

export default AdminViewExecute