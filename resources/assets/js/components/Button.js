import React,{Component} from 'react';

export default class Button extends Component{
    linkTo(){
        if(this.props.link){
            window.location.href=this.props.link;
        }
        let val = (this.props.state == 'true');
        val? this.props.event() : '';
    }
    render(){
        return(
            <div>
                <button
                    id={this.props.id}
                    className={this.props.color+' button'}
                    value={this.props.val}
                    onClick={this.linkTo.bind(this)}
                    type={this.props.type}
                    disabled={this.props.disabled}
                >
                    {
                        this.props.disabled? <div><i className="fa fa-circle-o-notch spinner-animation" aria-hidden="true"></i></div> : this.props.name
                    }
                </button>
            </div>
        );
    }
}