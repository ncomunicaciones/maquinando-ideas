import React from 'react'
import moment from 'moment'
import {isEmpty} from 'lodash'

const Chat = ({ messages, users, idea, type }) => (
    messages.map((chat,i) => {
        const user = users.find((user) => user.id == chat.user_id);
        const getError = (e) =>  {
            if(user.gender == 'm') e.target.src = assets + '/avatar.png'
            if(user.gender == 'f') e.target.src = assets + '/avatar-f.png'
        }

        const imoperator = !isEmpty(idea.getOperators) ? idea.getOperators.find((operator) => operator.id == user.id) : null
        const imbossoperator = !isEmpty(idea.getBossOperators) ? idea.getBossOperators.id == user.id ? true : false : null
        moment.lang("es")
        return <div key={i} className="supervisor chat-card clearfix mb-3">
            <div>
                <div className="chat-card-message">
                    { chat.message }
                </div>
                <div className="chat-card-date">
                    <div className="chat-card-date-container clearfix">
                        <div className="chat-card-date-container-image">
                            <div className="chat-card-date-container-image-circle">
                                <img className="find-me" src={assets+'/avatars/'+ user.avatar} onError={(e) => getError(e)} />
                            </div>
                        </div>
                        <div className="chat-card-date-container-data">
                            <div className="chat-card-date-container-data-username chat-card-date-container-data-username-title">
                                {user.name}
                                {type == 'evaluate' ? idea.user_id == user.id ? ' (Innovador)' : ' (Facilitador)' : null}
                                {type == 'execute' ? idea.user_id == user.id ? ' (Innovador)' : null : null}
                                {type == 'execute' ? imbossoperator ? ' (Jefe Operador)' : null : null}
                                {type == 'execute' ? !isEmpty(imoperator) ? ' (Ejecutor)' : null : null}
                                {type == 'execute' ? !imbossoperator && isEmpty(imoperator) && idea.user_id != user.id ? ' (Facilitador)' : null : null}
                            </div>
                            <div className="chat-card-date-container-data-username">

                            </div>
                            <div className="chat-card-date-container-data-date">
                                {moment(chat.created_at, "YYYYMMDD hh:mm:ss").fromNow()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        }
    )
)

export default Chat
