import React, {Component} from 'react'
import { connect } from 'react-redux'
import FontAwesome from 'react-fontawesome'
import {Link} from 'react-router-dom'
import fuzzysearch from 'fuzzysearch'
import { bindActionCreators } from 'redux';
import { disableCategoryAsync } from '../redux/asyncActions'

class CategoriesList extends Component {
    state = {
        pagination : 10,
        search: '',
        usersFiltered: this.props.users
    }
    paginationFunction = () => {
        if(this.state.pagination < this.state.usersFiltered.length){
            this.setState({pagination: this.state.pagination + 10})
        }
    }
    filterchange(e){
        const filter = e.currentTarget.value
        const filteredUsers = this.props.users.filter(user => {
            return (
              fuzzysearch(filter, user.name.toLowerCase())
            )
        })
        this.setState({usersFiltered: filteredUsers})
    }

    async changeValue(e, id){
        const value = e.currentTarget.checked
        e.checked = false
        var consult = {
            id: id,
            value: value
        }
        await this.props.disableCategoryAsync(consult)
    }

    render(){
        return(
        <div className="col-md-9 usuarios-content">
            <div className="subtitle mt-3">
                Lista de categorías
                <div className="searcher-user">
                    <input placeholder="Buscar" type="text" onChange={(e) => this.filterchange(e)} />
                </div>
            </div>
            <div className="clearfix w-100">
                <div className="mt-3">
                    <table className="table table-hover ">
                        <thead>
                            <tr>
                            <th className="pl-0" scope="col" width="100%">Nombre</th>
                            <th className="pl-0" scope="col"><span style={{'paddingRight': '45px'}}>Detalle</span></th>
                            <th className="pl-0" scope="col"><span style={{'paddingRight': '45px'}}>Estado</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.users.slice(0, this.state.pagination).map(
                                (user, i) => 
                                    <tr key={i}>
                                        <td scope="row">{user.name}</td>
                                        <td>
                                            <Link to={"/categoria/administrar/"+user.id}><FontAwesome style={{'color': '#333'}} name="search" /></Link>
                                        </td>
                                        <td>
                                            <input checked={user.state} onChange={(e) => this.changeValue(e, user.id)} type="checkbox" className="input-checked" />
                                        </td>
                                    </tr>
                            )}
                        </tbody>
                    </table>
                    {
                        this.state.pagination < this.state.usersFiltered.length?
                            <div className="user-list-load" >
                                <div onClick={() => this.paginationFunction()}>Cargar más</div>
                            </div>
                        : null
                    }
                    
                </div>
            </div>
        </div>
        )
    }
}
    

const mapStateToProps = state => {
    const users = state.categories
    return {users}
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        disableCategoryAsync
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesList)