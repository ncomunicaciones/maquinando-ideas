import React from 'react';
import { HashRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux'

const NavbarRequests = ({requests}) => 
    <div className="d-flex align-items-center navbar-options-container-opt float-left h-100 pr-2 pl-2 position-relative">
        <Router>
            <NavLink className="navbar-options-container-opt-link" exact to='/solicitudes/'>
                <div><strong>Solicitudes</strong><span> ({requests.length})</span></div>
            </NavLink>
        </Router>
    </div>

const mapStateToProps = state => {
    const requests = state.requests.filter((request) => request.state == 0)
    return {requests}
}
export default connect(mapStateToProps, null)(NavbarRequests)