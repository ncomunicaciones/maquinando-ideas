import React from 'react';
import FontAwesome from 'react-fontawesome';
import {Link} from 'react-router-dom'
import LoaderHOC from '../HOC/LoaderHOC';

const Category = ({data}) => {
    const getError = (e) => {
        e.target.src = assets + "/error-category.png"
    }
    return(
        <Link to={'/idea/crear/'+data.slug}>
            <div className="category-container">
                <div className="category-image d-flex align-items-center justify-content-center">
                    <img src={assets + "/avatars/" + data.image} onError={(e) => getError(e)} />
                </div>
                <div className="category-description d-flex align-items-center">
                    <div>
                        <div className="category-description-title">
                            {data.name}
                        </div>
                        <div className="category-description-content paragraph">
                            {data.description}
                        </div>
                    </div>
                </div>
                <div className="category-plus d-flex align-items-center justify-content-center">
                    <FontAwesome name="plus" className="category-plus-icon" />
                </div>
            </div>
        </Link>
    )
}

export default LoaderHOC('data')(Category)