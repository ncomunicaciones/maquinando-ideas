require('./bootstrap');
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Login from './views/Login';
import Filter from './routes/Filter';
import {  HashRouter as Router, Route,Switch} from 'react-router-dom';
import PrivateRoute from './routes/PrivateRoute'
import { Provider } from 'react-redux'
import store from './redux/store'

class App extends Component {
    render() {
        return (
            <div className="h-100 w-100">
                <Router>
                    <Switch>
                        <Route exact path="/login" component={Login} />
                        <Route exact render={(props) => <PrivateRoute component={Filter} {...props} />} />
                    </Switch>
                </Router>
            </div>
        );
    }
}  

const root = document.getElementById('app')
root ? ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, root) : false


