export function formatDate(date) {
    if (!date) return ''

    const time = new Date(date)
    const year = time.getFullYear()
    let month = time.getMonth() + 1
    let day = time.getDate()

    if (day < 10) {
        day = `0${day}`
    }
    if (month < 10) {
        month = `0${month}`
    }

    return `${day}/${month}/${year}`
}