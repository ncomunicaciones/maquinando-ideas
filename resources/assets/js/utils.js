export function formatDate(date) {
    if (!date) return ''

    const time = new Date(date)
    const year = time.getFullYear()
    let month = time.getMonth() + 1
    let day = time.getDate()

    if (day < 10) {
        day = `0${day}`
    }
    if (month < 10) {
        month = `0${month}`
    }

    return `${day}/${month}/${year}`
}

export function getTimeElapsedFromDate(date) {
    const startDate = new Date(date)
    const currentDate = new Date()
    const timeDiff = Math.abs(currentDate.getTime() - startDate.getTime())
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1
    
    return diffDays > 1 ? `${diffDays} días` : `${diffDays} día`
}

export function getTimeElapsedFromDate2(date) {
    const startDate = new Date(date)
    const currentDate = new Date()
    const timeDiff = Math.abs(currentDate.getTime() - startDate.getTime())
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1
    return diffDays > 1 ? `${diffDays}` : `${diffDays}`
}

export function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
}

export function validatePhone(phone) {
    //var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
    //return re.test(String(phone))
    return !isNaN(phone)
}

export function setContentText(contentTexts, slug) {
    if (contentTexts) {
        const searchedText = contentTexts.filter(contentText => {
            return contentText.slug === slug
        })
        if (searchedText.length) {
            return searchedText[0].text
        } else {
            return '...'
        }
    } else {
        return '...'
    }
}
