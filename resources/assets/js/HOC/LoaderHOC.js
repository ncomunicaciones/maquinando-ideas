import React from 'react'
import { isEmpty } from 'lodash'

const LoaderHOC = type => WrapperComponent => {
  return class extends React.Component {
    render() {
      return(
        <div>
            {this.props[type] !== null? 
              <div>
                {!isEmpty(this.props[type])  && 
                  <WrapperComponent {...this.props} />
                }
                {isEmpty(this.props[type])  && 
                  <div className="mis-ideas-container-no-register">No tienes registros.</div>
                }
              </div>
              :          
              <div className={"mis-ideas-container-carga mis-ideas-container-carga-"+type}><img src={assets + "/load.png"} /></div>
            }
          
        </div>
      )
    }
  }
}

export default LoaderHOC