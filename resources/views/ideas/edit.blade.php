<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>
<body>
    <div className="container">
        <form action="{{ url('ideas/1/update') }}" method="POST">
            @csrf
            <label for="">Titulo</label>
            <input type="text" name="title" value="{{ $idea->title }}"><br>
            <br>

            @foreach ($problems as $index => $problem)
              <label for="">Problema {{ $index + 1 }}</label>
              <input type="text" name="problem[{{ $index }}]" value="{{ $problem->problem }}"><br>
              
              @foreach (unserialize($problem->solutions) as $isol => $solution)
                <label for="">solución {{ $isol + 1 }}</label>
                <input type="text" name="solution[{{ $index }}][]" value="{{ $solution }}"><br>
              @endforeach
              <br>
            @endforeach
            
            <label for="">Resumen</label> 
            <input type="text" name="summary" value="{{ $idea->summary }}"><br>
            <label for="">Conclusiones</label> 
            <input type="text" name="conclusions" value="{{ $idea->conclusions }}"> <br>
            <button type="submit" className="btn btn-primary">Editar</button>
        </form>
    </div>
</body>
</html>