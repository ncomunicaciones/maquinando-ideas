<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>
<body>
    <div className="container">
        <form action="{{ url('ideas/store') }}" method="POST">
            @csrf
            <label for="">Título</label>
            <input type="text" name="title"><br>
            <br>
            <label for="">Problema 1</label>
            <input type="text" name="problem[]"><br>
            <label for="">solución 1</label>
            <input type="text" name="solution[0][]"><br>
            <label for="">solución 2</label>
            <input type="text" name="solution[0][]"><br>
            <br>
            <label for="">Problema 2</label>
            <input type="text" name="problem[]"><br>
            <label for="">solución 1</label>
            <input type="text" name="solution[1][]"><br>
            <label for="">solución 2</label>
            <input type="text" name="solution[1][]"><br>
            <br>
            <label for="">Resumen</label> 
            <input type="text" name="summary"><br>
            <label for="">Conclusiones</label> 
            <input type="text" name="conclusions"> <br>
            <button type="submit" className="btn btn-primary">Enviar</button>
        </form>
    </div>
</body>
</html>