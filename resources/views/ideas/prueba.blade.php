<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>
<body>
    <div className="container">
        <div>{{ $idea->title }}</div>
        <div>{{ $idea->state }}</div>
        <div>{{ $evaluate->evaluate_time }}</div>
        <div>{{ $evaluate->chat_room_id }}</div>

        <div className="card" >
            <ul className="list-group list-group-flush">
                @foreach($messages as $message)
                <li className="list-group-item">{{ $message->message }}</li>
                @endforeach
            </ul>
        </div>
        <form action="{{ url('ideas/'. $idea->id .'/message') }}" method="POST">
            @csrf
            <div className="form-group mt-4">
                <label for="exampleFormControlTextarea1">Escribir comentario</label>
                <textarea name="message" className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                <button className="btn btn-primary">Enviar</button>
            </div>
        </form>
    </div>

</body>
</html>