<!DOCTYPE html>
<html style="background-color: #EEE;">
<head>
	<meta charset="utf-8">
	<title>Maquinando Ideas - Cambio de contraseña</title>
</head>
<body style="width: 600px; padding: 24px; margin: 0 auto; background-color: #FFF;">
	<table style="border-collapse: collapse; table-layout: fixed; width: 600px; margin: 0 auto; color: #545454; font-family: sans-serif; font-size: 14px;">
		<tbody>
			<tr>
				<td>
					<table style="border-collapse: collapse; table-layout: fixed; width: 100%;">
						<tbody>
							<tr>
								<td>
									<img style="vertical-align: top;" src="{{ asset('img/logo.png') }}" alt="MAQUINANDO IDEAS">
								</td>
							</tr>
							<tr><td style="height: 24px;"></td></tr>
							<tr>
								<td style="font-size: 16px;">
									Hola <strong>{{ $user->name }}</strong>.
								</td>
							</tr>
							</tr>
							<tr><td style="height: 6px;"></td></tr>
						</tbody>
					</table>
					<table style="border-collapse: collapse; table-layout: fixed; width: 100%;">
						<tbody>
							<tr>
								<td style="width: 389px; padding: 10px; border: 1px solid #EEE;">Se le a cambiado la contraseña a tu cuenta exitosamente</td>
							</tr>
							<tr><td style="height: 24px;" colspan="2"></td></tr>
						</tbody>
					</table>
					<table style="border-collapse: collapse; table-layout: fixed; width: 100%;">
						<tbody>
							<tr>
								<td>
									Gracias por su tiempo.
								</td>
							</tr>
							<tr><td style="height: 24px;"></td></tr>
							<tr>
								<td style="font-size: 10px; text-align: center;">
									Av. Javier Prado Oeste N° 2344, San Isidro. Lima - Per&uacute;<br>
									Teléfono: +(511) 460-9090
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
