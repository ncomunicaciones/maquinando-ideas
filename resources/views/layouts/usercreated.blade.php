<!DOCTYPE html>
<html style="background-color: #EEE;">
<head>
	<meta charset="utf-8">
	<title>Maquinando Ideas - Creación de Usuario</title>
</head>
<body style="width: 600px; padding: 24px; margin: 0 auto; background-color: #FFF;">
	<table style="border-collapse: collapse; table-layout: fixed; width: 600px; margin: 0 auto; color: #545454; font-family: sans-serif; font-size: 14px;">
		<tbody>
			<tr>
				<td>
					<table style="border-collapse: collapse; table-layout: fixed; width: 100%;">
						<tbody>
							<tr>
								<td>
									<img style="vertical-align: top;" src="{{ asset('img/logo.png') }}" alt="MAQUINANDO IDEAS">
								</td>
							</tr>
							<tr><td style="height: 24px;"></td></tr>
							<tr>
								<td style="font-size: 16px;">
									Hola <strong>{{ $user->name }}</strong>.
								</td>
							</tr>
							</tr>
							<tr><td style="height: 6px;"></td></tr>
						</tbody>
					</table>
					<table style="border-collapse: collapse; table-layout: fixed; width: 100%;">
						<tbody>
							<?php if (isset($state)) : ?>
								<tr>
									<td style="width: 389px; padding: 10px; border: 1px solid #EEE;">Los credenciales asignados por el administrador son:  <br/>
										usuario: <?php echo $user->username ?> <br />
										password: <?php echo $password ?></td>
								</tr>
							<?php elseif(isset($state1)) : ?>
								<tr>
									<td style="width: 389px; padding: 10px; border: 1px solid #EEE;">La idea <?php echo $password ?> ha concluido con éxito. Agradecemos el tiempo brindado.</td>
								</tr>
							<?php elseif(isset($account)) : ?>
								<tr>
									<td style="width: 389px; padding: 10px; border: 1px solid #EEE;">Su solicitud para ingresar a la plataforma de maquinando ideas será evaluada por nuestros supervisores</td>
								</tr>
							<?php else: ?>
								<tr>
									<td style="width: 389px; padding: 10px; border: 1px solid #EEE;">Tus credenciales para poder ingresar a la plataforma Maquinando Ideas son: <br/>
									usuario: <?php echo $user->username ?> <br />
									password: <?php echo $password ?>
								</tr>
							<?php endif ?>
							<tr><td style="height: 24px;" colspan="2"></td></tr>
						</tbody>
					</table>
					<table style="border-collapse: collapse; table-layout: fixed; width: 100%;">
						<tbody>
							<tr>
								<td>
									Gracias por su tiempo.
								</td>
							</tr>
							<tr><td style="height: 24px;"></td></tr>
							<tr>
								<td style="font-size: 10px; text-align: center;">
									Av. Javier Prado Oeste N° 2344, San Isidro. Lima - Per&uacute;<br>
									Teléfono: +(511) 460-9090
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
