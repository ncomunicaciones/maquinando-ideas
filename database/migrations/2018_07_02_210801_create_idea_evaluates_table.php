<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdeaEvaluatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idea_evaluates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idea_id')->unique();
            $table->string('end_day');
            $table->integer('evaluate_time');
            $table->text('sustain')->nullable();
            $table->integer('chat_room_id');
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idea_evaluates');
    }
}
