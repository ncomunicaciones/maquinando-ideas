<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdeaExecutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idea_executes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idea_id')->unique();
            $table->integer('operator_boss_user_id')->nullable();
            $table->timestamp('start_day')->nullable();
            $table->timestamp('end_day')->nullable();
            $table->string('state');
            $table->integer('chat_room_id');
            $table->integer('execute_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idea_executes');
    }
}
