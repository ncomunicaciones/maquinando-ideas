<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    $name = str_random(10);
    return [
        [
            'name' => 'metodologia',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'metodologia.png'
        ],
        [
            'name' => 'mayor eficiencia',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'eficiencia.png'
        ],
        [
            'name' => 'ideas innovadoras',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'innovadoras.png'
        ],
        [
            'name' => 'optimizacion de procesos',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'optimizacion.png'
        ],
        [
            'name' => 'trabajo seguro',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'trabajo.png'
        ],
        [
            'name' => 'servicio al cliente',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'servicio.png'
        ],
        [
            'name' => 'otra categoria',
            'slug' => str_slug($name),
            'description' => $faker->text(),
            'image' => 'otros.png'
        ]
    ];
});
