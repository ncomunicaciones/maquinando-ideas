<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 30)->create();

        DB::table('categories')->delete();

        $categories = [
            [
                'name' => 'metodologia',
                'slug' => str_slug('metodologia'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'metodologia.png',
                'state' => true
            ],
            [
                'name' => 'mayor eficiencia',
                'slug' => str_slug('mayor eficiencia'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'eficiencia.png',
                'state' => true
            ],
            [
                'name' => 'ideas innovadoras',
                'slug' => str_slug('ideas innovadoras'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'innovadoras.png',
                'state' => true
            ],
            [
                'name' => 'optimizacion de procesos',
                'slug' => str_slug('optimizacion de procesos'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'optimizacion.png',
                'state' => true
            ],
            [
                'name' => 'trabajo seguro',
                'slug' => str_slug('trabajo seguro'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'trabajo.png',
                'state' => true
            ],
            [
                'name' => 'servicio al cliente',
                'slug' => str_slug('servicio al cliente'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'servicio.png',
                'state' => true
            ],
            [
                'name' => 'otra categoria',
                'slug' => str_slug('otra categoria'),
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
                'image' => 'otros.png',
                'state' => true
            ]
        ];
        foreach($categories as $category){
            App\Category::create($category);
        }
    }
}
