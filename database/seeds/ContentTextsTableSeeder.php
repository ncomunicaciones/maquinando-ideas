<?php

use Illuminate\Database\Seeder;

class ContentTextsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content_texts = [
            [
                'description' => 'Texto en el inicio del innovador',
                'slug' => 'text-user-home',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Texto en el inicio del supervisor',
                'slug' => 'text-admin-home',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Título de presentación de las categorías',
                'slug' => 'title-home-categories',
                'text' => 'PUEDES ELEGIR UNA DE LAS CATEGORÍAS Y AGREGAR SU IDEA',
            ],
            [
                'description' => 'Texto de título de aprobación de idea en el innovador',
                'slug' => 'title-user-idea-approved',
                'text' => 'TU IDEA HA SIDO APROBADA',
            ],
            [
                'description' => 'Texto de aprobación de idea en el innovador',
                'slug' => 'text-user-idea-approved',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Texto de título de aprobación de idea en el supervisor',
                'slug' => 'title-admin-idea-approved',
                'text' => 'LA IDEA HA SIDO APROBADA',
            ],
            [
                'description' => 'Texto de aprobación de idea en el supervisor',
                'slug' => 'text-admin-idea-approved',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Texto de título de ejecución de idea en el innovador',
                'slug' => 'title-user-idea-execute',
                'text' => 'TU IDEA HA SIDO PUESTA EN EJECUCIÓN',
            ],
            [
                'description' => 'Texto de ejecución de idea en el innovador',
                'slug' => 'text-user-idea-execute',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Texto de título de ejecución de idea en el supervisor',
                'slug' => 'title-admin-idea-execute',
                'text' => 'LA IDEA HA SIDO PUESTA EN EJECUCIÓN',
            ],
            [
                'description' => 'Título de desaprobación de idea en el supervisor',
                'slug' => 'title-admin-idea-rejected',
                'text' => 'LA IDEA HA SIDO DESAPROBADA',
            ],
            [
                'description' => 'Texto de desaprobación de idea en el supervisor',
                'slug' => 'text-admin-idea-rejected',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Título de desaprobación de idea en el innovador',
                'slug' => 'title-user-idea-rejected',
                'text' => 'TU IDEA HA SIDO DESAPROBADA',
            ],
            [
                'description' => 'Texto de desaprobación de idea en el innovador',
                'slug' => 'text-user-idea-rejected',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Texto de ejecución de idea en el supervisor',
                'slug' => 'text-admin-idea-execute',
                'text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
            ],
            [
                'description' => 'Título de idea premiada al colaborador',
                'slug' => 'title-award-collaborator',
                'text' => 'PREMIO AL COLABORADOR',
            ],
            [
                'description' => 'Texto de idea premiada al colaborador',
                'slug' => 'text-award-collaborator',
                'text' => 'AGRADECEMOS AL LIDER INNOVADOR {INNOVADOR} Y LOS MIEMBROS DEL EQUIPO QUE HAN LOGRADO PONER EN MARCHA "{IDEA}"',
            ],
        ];
        
        foreach ($content_texts as $content_text) {
            App\ContentText::create($content_text);
        }
    }
}
