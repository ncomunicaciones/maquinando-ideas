-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 11, 2018 at 07:44 PM
-- Server version: 5.7.21-log
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maquinando_ideas`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `administradorById` (IN `userid` INT(11))  select * from administrators where user_id = userid$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getIdeaById` (IN `userid` INT)  select ideas.id, ideas.user_id, ideas.admin_id, ideas.title, ideas.summary, ideas.conclusions, ideas.state, ideas.category_id, ideas.created_at, ideas.updated_at from ideas where ideas.user_id = userid$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getIdeasExecutesById` (IN `userid` INT)  select ideas.id, ideas.user_id, ideas.admin_id, ideas.title, ideas.summary, ideas.conclusions, ideas.state, ideas.category_id, ideas.created_at, ideas.updated_at
from ideas
inner join idea_executes on (ideas.id = idea_executes.idea_id)
where idea_executes.operator_boss_user_id = userid$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getIdeasOperatorsById` (IN `userid` INT)  select ideas.id, ideas.user_id, ideas.admin_id, ideas.title, ideas.summary, ideas.conclusions, ideas.state, ideas.category_id, ideas.created_at, ideas.updated_at
from ideas
inner join idea_executes on (ideas.id = idea_executes.idea_id)
inner join idea_operators on (idea_executes.id = idea_operators.idea_execute_id)
where idea_operators.operator_user_id = userid$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ideasGetAll` (IN `idadmin` INT)  select ideas.id, ideas.user_id, ideas.admin_id, ideas.title, ideas.summary, ideas.conclusions, ideas.state, ideas.category_id, ideas.created_at, ideas.updated_at,
problems.problem, problems.solutions, problems.created_at as problem_created_at, problems.updated_at as problem_updated_at
from ideas
inner join problems on (problems.idea_id = ideas.id)
inner join users on (users.id = ideas.user_id)
where ideas.admin_id = idadmin$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `user_id`, `state`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-09-14 01:38:38', '2018-10-12 00:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `image`, `state`, `created_at`, `updated_at`) VALUES
(1, 'ddddddd', 'ddddddd', 'ddddd', 'T8CdTnkyVeqHMdKtgWc6bbhNeceUVtxzeWul03bb.png', 1, '2018-09-04 22:40:27', '2018-10-04 02:54:36'),
(2, 'Optimización de recursos', 'optimizacion-de-recursos', 'Ahorro de recursos (gastos)', 'eficiencia.png', 1, '2018-09-04 22:40:27', '2018-09-18 00:23:25'),
(3, 'Clima laboral', 'clima-laboral', 'Mejora del bienestar de los colaboradores', 'hyvvRo0CR2yDXZR0QhqyHiJuIeCvp5m7ZvMjrpxq.png', 1, '2018-09-04 22:40:27', '2018-09-18 00:47:17'),
(4, 'Optimización de procesos', 'optimizacion-de-procesos', 'Mejora y simplificación  de procesos (método de trabajo, tiempos)', 'optimizacion.png', 1, '2018-09-04 22:40:27', '2018-10-04 02:54:32'),
(5, 'Trabajo seguro', 'trabajo-seguro', 'Fortalece la cultura de seguridad que permita minimizar la accidentabilidad.', 'trabajo.png', 1, '2018-09-04 22:40:27', '2018-10-04 03:28:53'),
(6, 'Servicio al cliente', 'servicio-al-cliente', 'Nuestros clientes son prioridad y la atención debe ser la mejor.', 'servicio.png', 1, '2018-09-04 22:40:27', '2018-10-04 03:28:54'),
(7, 'Otra categoría', 'otra-categoria', 'Si consideras que tu idea no se encuentra en otras categorías, regístrala aquí.', 'otros.png', 1, '2018-09-04 22:40:27', '2018-09-11 03:31:27'),
(8, 'aaaa', 'aaaa', 'ssss', 'P0mj27a6xygEuJTuiganuLJg7rkK1Wk1eSERvaf1.jpeg', 0, '2018-10-02 02:15:50', '2018-10-10 20:26:48'),
(9, 'w', 'w', 'dddwwww', '28uiiLUxe65RYaiQkpWdVwYDlvPJoetcQrYNsTRZ.jpeg', 0, '2018-10-04 01:13:35', '2018-10-04 03:39:24'),
(10, 'aaaasdf', 'aaaasdf', 'asdfasdf', 'Bo4V6VQT4o9Lqb3lqzU26vHseFWqaxt9vHhyzDD9.jpeg', 0, '2018-10-04 03:36:18', '2018-10-04 03:39:22'),
(11, 'aaaa', 'aaaa', 'sss', 'BixJxB1dw6G0QaPS5Bdb8Q1aNP7MY6RzPsaUus7U.jpeg', 0, '2018-10-04 03:36:55', '2018-10-04 03:39:21'),
(12, 'adfasd', 'adfasd', 'asdfasd', 'XQQ2C2eq9eDLtgjUOGNW9du0wil7q9yID5So6kE0.jpeg', 0, '2018-10-04 03:38:05', '2018-10-04 03:39:21'),
(13, '112', '112', 'qq', 'inurjIk7ZsPktjKhUF6PHX8AhDXAnqZKm0svSzVp.jpeg', 0, '2018-10-04 03:38:26', '2018-10-04 03:39:20'),
(14, 'asdfad', 'asdfad', 'dfdf', 'image', 0, '2018-10-04 03:38:42', '2018-10-04 03:39:20');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `chat_room_id`, `user_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'En un plazo no mayor a 30 se evalurá tu idea y te diremos cuales son los siguientes pasos', '2018-10-04 01:43:10', '2018-10-04 01:43:10'),
(2, 2, 2, 'En un plazo no mayor a 30 se evalurá tu idea y te diremos cuales son los siguientes pasos', '2018-10-04 01:43:19', '2018-10-04 01:43:19'),
(3, 3, 2, 'En un plazo no mayor a 30 se evalurá tu idea y te diremos cuales son los siguientes pasos', '2018-10-04 01:43:27', '2018-10-04 01:43:27'),
(4, 6, 2, 'En un plazo no mayor a 30 se evalurá tu idea y te diremos cuales son los siguientes pasos', '2018-10-04 02:53:54', '2018-10-04 02:53:54'),
(5, 7, 2, 'En un plazo no mayor a 30 se evalurá tu idea y te diremos cuales son los siguientes pasos', '2018-10-10 22:41:52', '2018-10-10 22:41:52'),
(6, 6, 2, 'adsfasfasdf', '2018-10-11 20:28:23', '2018-10-11 20:28:23'),
(7, 1, 2, 'dafadsfsd', '2018-10-11 20:30:35', '2018-10-11 20:30:35'),
(8, 1, 2, 'dsfsdfasdf', '2018-10-11 20:30:38', '2018-10-11 20:30:38'),
(9, 1, 2, 'dafasdfsadf', '2018-10-11 20:30:53', '2018-10-11 20:30:53'),
(10, 1, 2, 'wwww', '2018-10-11 20:30:56', '2018-10-11 20:30:56'),
(11, 1, 2, 'hola', '2018-10-11 21:29:34', '2018-10-11 21:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `chat_rooms`
--

CREATE TABLE `chat_rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat_rooms`
--

INSERT INTO `chat_rooms` (`id`, `created_at`, `updated_at`) VALUES
(1, '2018-10-04 01:43:10', '2018-10-04 01:43:10'),
(2, '2018-10-04 01:43:19', '2018-10-04 01:43:19'),
(3, '2018-10-04 01:43:27', '2018-10-04 01:43:27'),
(4, '2018-10-04 01:54:11', '2018-10-04 01:54:11'),
(5, '2018-10-04 02:04:16', '2018-10-04 02:04:16'),
(6, '2018-10-04 02:53:53', '2018-10-04 02:53:53'),
(7, '2018-10-10 22:41:52', '2018-10-10 22:41:52'),
(8, '2018-10-10 22:47:02', '2018-10-10 22:47:02'),
(9, '2018-10-11 20:29:53', '2018-10-11 20:29:53');

-- --------------------------------------------------------

--
-- Table structure for table `content_texts`
--

CREATE TABLE `content_texts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_texts`
--

INSERT INTO `content_texts` (`id`, `description`, `slug`, `text`, `created_at`, `updated_at`) VALUES
(1, 'Texto en el inicio del innovador', 'text-user-home', 'HOLA2', '2018-09-04 22:40:27', '2018-10-03 02:12:34'),
(2, 'Texto en el inicio del supervisor', 'text-admin-home', 'HOLA SUPERVISOR (A)', '2018-09-04 22:40:27', '2018-10-02 02:54:21'),
(3, 'Titulo de presentación de las categorías', 'title-home-categories', 'AQUÍ PUEDES SELECCIONAR UNA DE LAS CATEGORÍAS Y AGREGAR TU IDEA222', '2018-09-04 22:40:27', '2018-10-02 02:57:09'),
(4, 'Texto de titulo de aprobacion de idea en el innovador', 'title-user-idea-approved', 'TU IDEA HA SIDO APROBADA', '2018-09-04 22:40:27', '2018-09-06 02:38:20'),
(5, 'Texto de aprobacion de idea en el innovador', 'text-user-idea-approved', 'GRACIAS POR COMPARTIR TU IDEA,\nAHORA INICIA LA ETAPA DE IMPLEMENTACIÓN', '2018-09-04 22:40:27', '2018-09-06 02:59:16'),
(6, 'Texto de titulo de aprobacion de idea en el supervisor', 'title-admin-idea-approved', 'LA IDEA HA SIDO APROBADA', '2018-09-04 22:40:27', '2018-09-04 22:40:27'),
(7, 'Texto de aprobacion de idea en el supervisor', 'text-admin-idea-approved', 'INICIA LA ETAPA DE IMPLEMENTACIÓN DE LA IDEA', '2018-09-04 22:40:27', '2018-09-11 03:22:38'),
(8, 'Texto de titulo de ejecución de idea en el innovador', 'title-user-idea-execute', 'TU IDEA HA SIDO PUESTA EN EJECUCIÓN', '2018-09-04 22:40:27', '2018-09-04 22:40:27'),
(9, 'Texto de ejecución de idea en el innovador', 'text-user-idea-execute', 'DEBES INICIAR LA EJECUCIÓN. \n\nCOORDINA CON TODO EL EQUIPO.', '2018-09-04 22:40:27', '2018-09-06 03:05:52'),
(10, 'Texto de titulo de ejecución de idea en el supervisor', 'title-admin-idea-execute', 'LA IDEA HA SIDO PUESTA EN EJECUCIÓN', '2018-09-04 22:40:27', '2018-09-04 22:40:27'),
(11, 'Titulo de desaprobacion de idea en el supervisor', 'title-admin-idea-rejected', 'LA IDEA HA SIDO DESAPROBADA', '2018-09-04 22:40:27', '2018-09-04 22:40:27'),
(12, 'Texto de desaprobacion de idea en el supervisor', 'text-admin-idea-rejected', 'LA IDEA HA SIDO DESAPROBADA', '2018-09-04 22:40:27', '2018-09-11 01:57:44'),
(13, 'Titulo de desaprobacion de idea en el innovador', 'title-user-idea-rejected', 'TU IDEA HA SIDO DESAPROBADA', '2018-09-04 22:40:27', '2018-09-04 22:40:27'),
(14, 'Texto de desaprobacion de idea en el innovador', 'text-user-idea-rejected', 'Por ahora tu idea no se ajusta a los objetivos del proceso.\nGracias por tu aporte , estamos ansiosos por recibir tu próxima idea.', '2018-09-04 22:40:27', '2018-09-06 02:34:43'),
(15, 'Texto de ejecución de idea en el supervisor', 'text-admin-idea-execute', 'INICIA LA ETAPA DE EJECUCIÓN DE LA IDEA', '2018-09-04 22:40:27', '2018-09-11 01:58:10'),
(16, 'Titulo de idea premiada al colaborador', 'title-award-collaborator', 'PREMIO A LA IDEA', '2018-09-04 22:40:27', '2018-09-06 02:36:05'),
(17, 'Texto de idea premiada al colaborador', 'text-award-collaborator', 'AGRADECEMOS AL LÍDER INNOVADOR Y A LOS MIEMBROS DEL EQUIPO QUE HAN LOGRADO PONER EN MARCHA LA IDEA INNOVADORA.', '2018-09-04 22:40:27', '2018-09-06 02:35:53');

-- --------------------------------------------------------

--
-- Table structure for table `ideas`
--

CREATE TABLE `ideas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `conclusions` text COLLATE utf8mb4_unicode_ci,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ideas`
--

INSERT INTO `ideas` (`id`, `user_id`, `admin_id`, `title`, `summary`, `conclusions`, `state`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 719, 1, '1', 'asdfasdf', 'asdfasdf', 'evaluacion', '2', '2018-10-04 01:43:09', '2018-10-04 01:43:10'),
(2, 719, 1, '2', 'asdfsadf', 'sdfdasf', 'aprobado', '2', '2018-10-04 01:43:18', '2018-10-04 02:04:16'),
(3, 719, 1, '3', 'asdfasdf', 'asdfasdf', 'premiada', '2', '2018-10-04 01:43:26', '2018-10-04 02:11:28'),
(4, 719, 1, 'asdfasdf', 'adsfasd', 'asdf', 'aprobado', '1', '2018-10-04 02:53:52', '2018-10-11 20:29:53'),
(5, 2, NULL, 'asdf', 'asdfad', 'asdfasdf', 'guardado', '1', '2018-10-10 20:29:53', '2018-10-10 20:29:53'),
(6, 719, 1, 'asdfasd', 'asdfasdf', 'fasdf', 'ejecucion', '1', '2018-10-10 22:41:51', '2018-10-10 22:47:36'),
(7, 2, NULL, 'adsdasd', 'adsasd', 'asdda', 'guardado', '1', '2018-10-11 23:19:18', '2018-10-11 23:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `idea_evaluates`
--

CREATE TABLE `idea_evaluates` (
  `id` int(10) UNSIGNED NOT NULL,
  `idea_id` int(11) NOT NULL,
  `end_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluate_time` int(11) NOT NULL,
  `sustain` text COLLATE utf8mb4_unicode_ci,
  `chat_room_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `idea_evaluates`
--

INSERT INTO `idea_evaluates` (`id`, `idea_id`, `end_day`, `evaluate_time`, `sustain`, `chat_room_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-01-31 20:43:10', 120, NULL, 1, '2018-10-04 01:43:10', '2018-10-04 02:35:56'),
(2, 2, '2018-11-02 20:43:19', 30, 'asdfasdf', 2, '2018-10-04 01:43:19', '2018-10-04 02:04:16'),
(3, 3, '2018-11-02 20:43:27', 30, 'aaa', 3, '2018-10-04 01:43:27', '2018-10-04 01:54:11'),
(4, 4, '2019-01-01 21:53:54', 90, 'dasfsadf', 6, '2018-10-04 02:53:54', '2018-10-11 20:29:53'),
(5, 6, '2018-11-09 17:41:52', 30, 'aaa', 7, '2018-10-10 22:41:52', '2018-10-10 22:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `idea_executes`
--

CREATE TABLE `idea_executes` (
  `id` int(10) UNSIGNED NOT NULL,
  `idea_id` int(11) NOT NULL,
  `operator_boss_user_id` int(11) DEFAULT NULL,
  `start_day` timestamp NULL DEFAULT NULL,
  `end_day` timestamp NULL DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chat_room_id` int(11) NOT NULL,
  `execute_time` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `idea_executes`
--

INSERT INTO `idea_executes` (`id`, `idea_id`, `operator_boss_user_id`, `start_day`, `end_day`, `state`, `chat_room_id`, `execute_time`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2018-10-03 13:54:29', '2018-10-03 14:11:28', 'active', 4, 150, '2018-10-04 01:54:11', '2018-10-04 02:11:28'),
(2, 2, NULL, NULL, NULL, 'inactive', 5, NULL, '2018-10-04 02:04:16', '2018-10-04 02:04:16'),
(3, 6, 742, '2018-10-10 10:47:35', NULL, 'active', 8, 90, '2018-10-10 22:47:02', '2018-10-10 22:47:35'),
(4, 4, NULL, NULL, NULL, 'inactive', 9, NULL, '2018-10-11 20:29:53', '2018-10-11 20:29:53');

-- --------------------------------------------------------

--
-- Table structure for table `idea_operators`
--

CREATE TABLE `idea_operators` (
  `id` int(10) UNSIGNED NOT NULL,
  `idea_execute_id` int(11) NOT NULL,
  `operator_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `idea_operators`
--

INSERT INTO `idea_operators` (`id`, `idea_execute_id`, `operator_user_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2018-10-10 22:47:35', '2018-10-10 22:47:35'),
(2, 3, 743, '2018-10-10 22:47:35', '2018-10-10 22:47:35'),
(3, 3, 745, '2018-10-10 22:47:36', '2018-10-10 22:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `read` tinyint(1) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `idea_id` int(11) DEFAULT NULL,
  `provider` int(11) NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isadmin` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `read`, `title`, `type`, `user_id`, `idea_id`, `provider`, `link`, `isadmin`, `created_at`, `updated_at`) VALUES
(1, 1, 'Se agregó una nueva idea para su evaluación', 'grey', 2, 1, 719, '1', 1, '2018-10-04 01:43:10', '2018-10-04 02:32:25'),
(2, 1, 'Se agregó una nueva idea para su evaluación', 'grey', 2, 2, 719, '2', 1, '2018-10-04 01:43:19', '2018-10-04 02:32:25'),
(3, 1, 'Se agregó una nueva idea para su evaluación', 'grey', 2, 3, 719, '3', 1, '2018-10-04 01:43:27', '2018-10-04 02:32:24'),
(4, 1, 'Se modifico el tiempo de evaluación a 180 dias de la idea \"2\"', 'green', 719, 2, 2, '2', 0, '2018-10-04 01:47:56', '2018-10-04 01:57:37'),
(5, 1, 'Se modifico el tiempo de evaluación a 210 dias de la idea \"2\"', 'green', 719, 2, 2, '2', 0, '2018-10-04 01:48:04', '2018-10-04 01:57:39'),
(6, 1, 'Se modifico el tiempo de evaluación a 120 dias de la idea \"2\"', 'green', 719, 2, 2, '2', 0, '2018-10-04 01:48:33', '2018-10-04 01:55:06'),
(7, 1, 'Se modifico el tiempo de evaluación a 180 dias de la idea \"2\"', 'green', 719, 2, 2, '2', 0, '2018-10-04 01:48:37', '2018-10-04 01:55:02'),
(8, 1, 'Se modifico el tiempo de evaluación a 30 dias de la idea \"2\"', 'green', 719, 2, 2, '2', 0, '2018-10-04 01:48:38', '2018-10-04 01:54:59'),
(9, 1, 'La idea \"3\" enviada fue aprobado', 'green', 719, 3, 2, '3', 0, '2018-10-04 01:54:11', '2018-10-04 01:55:25'),
(10, 1, 'La idea \"3\" enviada fue puesta en ejecución', 'green', 719, 3, 2, '3', 0, '2018-10-04 01:54:29', '2018-10-04 01:57:28'),
(11, 1, 'Se modifico el tiempo de ejecución a 150 dias de la idea \"3\"', 'green', 719, 3, 2, '3', 0, '2018-10-04 01:57:09', '2018-10-04 02:04:50'),
(12, 1, 'La idea \"2\" enviada fue aprobado', 'green', 719, 2, 2, '2', 0, '2018-10-04 02:04:16', '2018-10-04 02:04:49'),
(13, 1, 'La idea 3 fue premiada', 'green', 719, 3, 2, '3', 0, '2018-10-04 02:11:28', '2018-10-10 20:32:12'),
(14, 1, 'Se modifico el tiempo de evaluación a 150 dias de la idea \"1\"', 'green', 719, 1, 2, '1', 0, '2018-10-04 02:27:11', '2018-10-10 20:32:16'),
(15, 1, 'Se modifico el tiempo de evaluación a 120 dias de la idea \"1\"', 'green', 719, 1, 2, '1', 0, '2018-10-04 02:35:56', '2018-10-10 20:32:13'),
(16, 1, 'Se agregó una nueva idea para su evaluación', 'grey', 2, 4, 719, '4', 1, '2018-10-04 02:53:54', '2018-10-04 02:59:34'),
(17, 1, 'Se agregó una nueva idea para su evaluación', 'grey', 2, 6, 719, '6', 1, '2018-10-10 22:41:52', '2018-10-11 20:27:22'),
(18, 0, 'La idea \"asdfasd\" enviada fue aprobado', 'green', 719, 6, 2, '6', 0, '2018-10-10 22:47:02', '2018-10-10 22:47:02'),
(19, 0, 'La idea \"asdfasd\" enviada fue puesta en ejecución', 'green', 719, 6, 2, '6', 0, '2018-10-10 22:47:36', '2018-10-10 22:47:36'),
(20, 0, 'adsfasfasdf', 'chat', 2, 4, 2, 'evaluacion', 0, '2018-10-11 20:28:23', '2018-10-11 20:28:23'),
(21, 0, 'Se modifico el tiempo de evaluación a 90 dias de la idea \"asdfasdf\"', 'green', 719, 4, 2, '4', 0, '2018-10-11 20:28:43', '2018-10-11 20:28:43'),
(22, 0, 'La idea \"asdfasdf\" enviada fue aprobado', 'green', 719, 4, 2, '4', 0, '2018-10-11 20:29:53', '2018-10-11 20:29:53'),
(23, 0, 'dafadsfsd', 'chat', 2, 1, 2, 'evaluacion', 0, '2018-10-11 20:30:35', '2018-10-11 20:30:35'),
(24, 0, 'dsfsdfasdf', 'chat', 2, 1, 2, 'evaluacion', 0, '2018-10-11 20:30:38', '2018-10-11 20:30:38'),
(25, 0, 'dafasdfsadf', 'chat', 2, 1, 2, 'evaluacion', 0, '2018-10-11 20:30:53', '2018-10-11 20:30:53'),
(26, 0, 'wwww', 'chat', 2, 1, 2, 'evaluacion', 0, '2018-10-11 20:30:56', '2018-10-11 20:30:56'),
(27, 0, 'hola', 'chat', 2, 1, 2, 'evaluacion', 0, '2018-10-11 21:29:34', '2018-10-11 21:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE `problems` (
  `id` int(10) UNSIGNED NOT NULL,
  `idea_id` int(11) NOT NULL,
  `problem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `solutions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`id`, `idea_id`, `problem`, `solutions`, `created_at`, `updated_at`) VALUES
(1, 1, 'dfasdf', 'a:3:{i:0;s:8:\"asdfasdf\";i:1;N;i:2;N;}', '2018-10-04 01:43:09', '2018-10-04 01:43:09'),
(2, 2, 'asdfasd', 'a:3:{i:0;s:5:\"fasdf\";i:1;N;i:2;N;}', '2018-10-04 01:43:18', '2018-10-04 01:43:18'),
(3, 3, 'asdfasdf', 'a:3:{i:0;s:8:\"asdfasdf\";i:1;N;i:2;N;}', '2018-10-04 01:43:26', '2018-10-04 01:43:26'),
(4, 4, 'fasdf', 'a:3:{i:0;s:4:\"asdf\";i:1;N;i:2;N;}', '2018-10-04 02:53:52', '2018-10-04 02:53:52'),
(5, 5, 'fasdfasd', 'a:3:{i:0;s:5:\"fasdf\";i:1;N;i:2;N;}', '2018-10-10 20:29:53', '2018-10-10 20:29:53'),
(6, 6, 'asdfasdf', 'a:3:{i:0;s:7:\"sadfsad\";i:1;N;i:2;N;}', '2018-10-10 22:41:51', '2018-10-10 22:41:51'),
(7, 7, 'adasd', 'a:3:{i:0;s:6:\"asdasd\";i:1;N;i:2;N;}', '2018-10-11 23:19:18', '2018-10-11 23:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `user_id`, `type`, `data`, `state`, `created_at`, `updated_at`) VALUES
(1, 2, 'create', 'a:3:{i:0;s:11:\"matheu soto\";i:1;s:25:\"msoto@ncomunicaciones.com\";i:2;s:6:\"123456\";}', 0, '2018-10-10 21:26:27', '2018-10-10 21:26:27'),
(2, 2, 'update', 'a:1:{i:0;s:25:\"msoto@ncomunicaciones.com\";}', 0, '2018-10-10 21:40:56', '2018-10-10 21:40:56'),
(3, 2, 'update', 'a:1:{i:0;s:4:\"user\";}', 0, '2018-10-10 21:42:02', '2018-10-10 21:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password1` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(191) NOT NULL,
  `avatar` varchar(191) NOT NULL DEFAULT 'avatar.jpg',
  `state` tinyint(1) NOT NULL,
  `gender` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `email`, `password1`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `avatar`, `state`, `gender`) VALUES
(1, 'mario.berghusenn', 'Mario Marco Martin Berghusen Sayas 1123123', 'mario.beergghusen@unimaq.com.pee', '7580726', '$2y$10$A9SwqBK9NznTTFSgUifNxeFbHKMe3w4SOh3c16c9NHRz4ExAJMaAa', '9PES2WqiO4fGxJObjbHYJor9Albd6cCwuZrZqcQuh1ItUbI1LjKmH5SVozGO', '2018-10-03 22:26:13', '2018-10-04 03:26:13', '12345671', 'rNjJArexmTPfhs49kgJuIFi6YaYWM542biooHD2x.png', 1, 'm'),
(2, 'adrian.soto', 'Adrian Cesar Soto Salgado', 'adrian.soto@unimaq.com.pe', '8704727', '$2y$10$7JDxg.PIzKgsbttOjb67V.RnWaA8TaNdsOKYrAiVVYVa1wc1.xI6u', 'LQRUhM2kiHkClZpuE3861UH4hjNciIZO5e371xdy3xebD6vCnv5EeGpHCPfi', '2018-10-11 19:40:23', '2018-10-10 20:28:22', '444444', 'Qin5jTed2qpNX0Hz7fXyY33jOERqsyaG4LiPxum4.jpeg', 1, 'm'),
(719, 'matheu', 'matheu', 'adrian.soto@unimaq.com.pee', 'ss', '$2y$10$7TlDMs49.Wl2eYl.AGV.O.En.2cMfEa.RycAOJQme4b9rRnzJgqxC', 'iRGHPJVAcCHnpnrzgKDDv3xopStFVc6ilDwVU1Ew4A3vQqzMVGyoewlejOHG', '2018-10-10 17:46:44', '2018-10-03 02:45:30', '123555', 'vzAy4HLPkdnOW9MVQKtjHm9sF2Zt3u83rNh3hPbm.jpeg', 1, 'm'),
(742, 'aaaaa', 'aaaa', 'aaasa@aaaa.com', 'ss', '$2y$10$kDRrq7BM2.qrMqqV4eKCwumRBSc0sebpaakq2DGyh3rcnU6kqGY7u', NULL, '2018-10-04 03:14:42', '2018-10-04 03:14:42', '123', 'avatar.jpg', 1, 'm'),
(743, 'juan', 'Juan Peres', 'juan@juan.com', 'ss', '$2y$10$OIZEI7EsxmmskOJopzYmL.zzAujvetP1elAeJCf8qI0INb7RiOvUq', NULL, '2018-10-03 22:28:58', '2018-10-04 03:28:58', '123', 'avatar.jpg', 0, 'm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_rooms`
--
ALTER TABLE `chat_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_texts`
--
ALTER TABLE `content_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas`
--
ALTER TABLE `ideas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `idea_evaluates`
--
ALTER TABLE `idea_evaluates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idea_evaluates_idea_id_unique` (`idea_id`);

--
-- Indexes for table `idea_executes`
--
ALTER TABLE `idea_executes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idea_executes_idea_id_unique` (`idea_id`);

--
-- Indexes for table `idea_operators`
--
ALTER TABLE `idea_operators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `chat_rooms`
--
ALTER TABLE `chat_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `content_texts`
--
ALTER TABLE `content_texts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `ideas`
--
ALTER TABLE `ideas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `idea_evaluates`
--
ALTER TABLE `idea_evaluates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `idea_executes`
--
ALTER TABLE `idea_executes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `idea_operators`
--
ALTER TABLE `idea_operators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=746;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
