<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ideas', 'IdeasController@index'); // Obtener todas las categorias
Route::get('/ideas/{idea}/edit', 'IdeasController@edit');
Route::get('/ideas/{idea}/prueba', 'IdeasController@prueba'); // Prueba de chat

// Todas las rutas POST retornan 'success' o 'error'

Auth::routes(); // La ruta para el login es '/login', es POST.
Route::post('/validatelogin', 'UserController@validatelogin'); // login api
Route::post('/user/login', 'UserController@login'); // login api
Route::post('/user/logout', 'UserController@logout'); // login api
Route::get('/login/check', 'UserController@check_login'); // check login

Route::post('/ideas/store', 'IdeasController@store'); // Subir una idea
Route::post('/ideas/{idea}/update', 'IdeasController@update'); // Editar una idea

Route::get('/ideas/{filtro}', 'IdeasController@filter'); // Filtrar Ideas {guardado,aprobado,desaprobado,evaluacion}

Route::get('/supervisor-ideas', 'UserController@supervisorIdeas'); // Todas las ideas del supervisor
Route::get('/ejecutar_idea/{idea}/detail', 'IdeasController@ejecutarIdeaDetalle'); // Filtrar Ideas {guardado,aprobado,desaprobado,evaluacion,finalizado}
Route::post('/user/update', 'UserController@update_user'); // Actualizar data de usuario
Route::post('/user/create', 'UserController@create_user'); // Crear usuario


Route::get('/{user}/is-administrator', 'UserController@isAdministrator');
Route::get('/{admin}/is-administrator/{type}/all-ideas', 'UserController@allIdeasIsAdministrator');


Route::get('/ideas/evaluate-detail/{idea}', 'IdeasController@evaluateDetail');
Route::get('/ideas/execute-detail/{idea}', 'IdeasController@executeteDetail');
Route::get('/ideas/{idea}/view', 'IdeasController@show');
Route::get('/admin/view/{admin_id}', 'UserController@get_administrator');
Route::post('/ideas/{idea_evaluate}/evaluateedit', 'IdeasController@evaluateEdit'); // Solicitar editar una idea
Route::get('/ideas/{idea}/show', 'IdeasController@ideaDetail'); // Solicitar evaluar una idea
Route::get('/admin/ideas/{type}', 'UserController@AdminIdeas');


Route::post('/ideas/save_operators/{idea_execute}', 'IdeasController@save_operators');
Route::get('/ideas/get_idea_state/{idea}', 'IdeasController@get_idea_state_data');


Route::get('ideas/get_ideas_operator/{user_id}', 'IdeasController@get_ideas_operator');
Route::get('ideas/get_ideas_operator_boss/{user_id}', 'IdeasController@get_ideas_operator_boss');



Route::get('/categorias', 'IdeasController@categorias');
Route::get('/categorias/{categoria}', 'IdeasController@categoriasShow');
Route::get('ideas/categories/{caterory_id}', 'CategoriesController@get_category_by_id');

Route::get('/notificaciones', 'UserController@getNotifications');
Route::post('/notificaciones/store', 'UserController@addNotifications');
Route::get('/notificaciones/leer/{id}', 'UserController@readNotifications');

//Obtener Chats
Route::get('/getchat/{idea}/{state}', 'ChatController@getChat');
Route::post('/sendchat/{idea}/{state}', 'ChatController@sendChat');

Route::post('content_text/send', 'ContentTextController@store');
Route::get('content_text/retrieve', 'ContentTextController@show');
Route::post('content_text/update/{content_text_id}', 'ContentTextController@update');

Route::post('/recoverpassword', 'UserController@recoverPassword');
Route::post('/newuser', 'UserController@newUser');


Route::get('/allusers', 'UserController@getUsers');
Route::post('/edituser', 'UserController@edit_user');


Route::post('/changeuser', 'UserController@change_user');


//RUTAS VALIDAS

//Obtener usuario logueado
Route::get('/auth-user', 'UserController@auth');
Route::get('/change-role/{value}', 'UserController@changeRole'); // cambiar role

//Todas las ideas del usuario
Route::get('/auth-ideas', 'UserController@authIdeas'); 

//Todas las categorias 
Route::get('/categorias', 'CategoriesController@index');
Route::post('/editcategory', 'UserController@edit_category');
Route::post('/createcategory', 'UserController@create_category');
Route::post('/changecategory', 'UserController@change_category'); // desactivar idea

//Todas las peticiones
Route::get('/requests', 'UserController@requests');

//Todos los usuarios
Route::get('/all-users', 'UserController@allUsers');

//Excel Ideas
Route::get('/excelideas', 'IdeasController@excelIdeas');

//Generar Excel
Route::get('/exportexcel', 'IdeasController@exportExcel');

//Guardar Excel en cookies
Route::post('/excelcookies', 'IdeasController@excelCookies');

//Tiempo de ejecución
Route::post('/ejecutar_idea/{idea}/edit', 'IdeasController@ejecutarIdeaEditar');
//Tiempo de evaluación
Route::post('/evaluar_idea/{idea}/edit', 'IdeasController@evaluarIdeaEditar');

//Ejecutar Idea
Route::post('/ideas/execute_idea/{idea}', 'IdeasController@execute_idea');

//Finalizar Idea
Route::post('/ideas/finish_execute_idea/{idea}', 'IdeasController@finish_idea_execute'); //Finalizar ejecucion de una idea

//Actualizar solicitud
Route::post('/request/update', 'IdeasController@update_request');


Route::get('/ideas/{idea}/evaluate', 'IdeasController@evaluate'); // Solicitar evaluar una idea

Route::get('/getchatsall', 'IdeasController@getAllChatsRooms'); 
Route::get('/getchatsallsup', 'IdeasController@getAllChatsRoomsSupervisor'); 

Route::get('/seechanges', 'UserController@seeChanges');
Route::get('/seechangessup', 'UserController@seeChangesSup');
Route::post('/changenoti', 'IdeasController@checkNotifications');

