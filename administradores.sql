-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `administrators` (`id`, `user_id`, `state`, `created_at`, `updated_at`) VALUES
(1,	2,	1,	'2018-09-14 01:38:38',	'2018-09-14 01:38:38'),
(2,	29,	1,	'2018-09-14 01:38:38',	'2018-09-14 01:38:38'),
(3,	318,	1,	'2018-09-14 01:38:38',	'2018-09-14 01:38:38'),
(4,	342,	1,	'2018-09-14 01:38:38',	'2018-09-14 01:38:38'),
(5,	640,	1,	'2018-09-14 01:38:38',	'2018-09-14 01:38:38');

-- 2018-09-14 16:07:11
